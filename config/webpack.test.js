const helpers = require('./helpers');
const path = require('path');

const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');

const ENV = 'test';
const BASE_URL = '';

module.exports = {
    devtool: 'inline-source-map',
    resolve: {
        extensions: [ '.ts', '.js' ],
        modules: [ helpers.root('src'), 'node_modules' ]
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\/.ts$/,
                loader: 'tslint-loader',
                exclude: [helpers.root('node_modules')]
            },
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader',
                exclude: [
                    helpers.root('node_modules/rxjs'),
                    helpers.root('node_modules/@angular')
                ]
            },
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader',
                query: {
                    sourceMap: false,
                    inlineSourceMap: true,
                    compilerOptions: {
                        removeComments: true
                    }
                },
                exclude: [/\.e2e\.ts$/]
            },
            {
                test: /\.html$/,
                loader: 'raw-loader',
                exclude: [helpers.root('src/index.html')]
            },
            {
                test: /\.css$/,
                loaders: ['to-string-loader', 'css-loader']
            }
        ]
    },
    plugins: [
      new DefinePlugin({
          'ENV': JSON.stringify(ENV),
          'BASE_URL': JSON.stringify(BASE_URL)
      }),
      new ContextReplacementPlugin(
          /angular(\\|\/)core(\\|\/)@angular/,
          helpers.root('src')
      ),
      new LoaderOptionsPlugin({
          debug: true,
          options: {
              tslint: {
                  emitErrors: false,
                  failOnHint: false,
                  resourcePath: 'src'
              }
          }
      })
    ],
    performance: {
        hints: false
    },
    node: {
      global: true,
      process: false,
      crypto: 'empty',
      module: false,
      clearImmediate: false,
      setImmediate: false
    }
};
