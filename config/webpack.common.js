"use strict"

const webpack = require('webpack');
const helpers = require('./helpers');

const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const ForkCheckerPlugin = require('awesome-typescript-loader').ForkCheckerPlugin;
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin');
const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ngcWebpack = require('ngc-webpack');

module.exports = function(options) {
  let isProd = options.env === 'production';

  return {
    devtool: 'source-map',
    entry: {
      'polyfills': './src/polyfills.browser.ts',
      'main':      isProd ? './src/main.browser.aot.ts' :
                         './src/main.browser.ts'
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                loader: 'raw-loader',
                exclude: [helpers.root('src/index.html')]
            },
            {
                test: /\.ts$/,
                use: [
                  {
                    loader: '@angularclass/hmr-loader',
                    options: { pretty: !isProd, prod: isProd }
                  },
                  { // MAKE SURE TO CHAIN VANILLA JS CODE, I.E. TS COMPILATION OUTPUT.
                    loader: 'ng-router-loader',
                    options: {
                        loader: 'async-import',
                        genDir: 'compiled',
                        aot: isProd,
                    }
                  },
                  {
                    loader: 'awesome-typescript-loader',
                    options: {
                        configFileName: 'tsconfig-webpack.json',
                        useCache: !isProd,
                    }
                  },
                  { loader: 'angular2-template-loader' },
                ],
                exclude: [/\.(spec|e2e)\.ts$/],
            },
            {
                test: /\.css$/,
                loaders: ['to-string-loader', 'css-loader'],
                exclude: [helpers.root('src', 'styles')],
            },
            {
                test: /\.scss$/,
                loaders: ['to-string-loader', 'css-loader', 'sass-loader'],
                exclude: [helpers.root('src', 'styles')],
            },
            {   test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.(eot|woff2?|svg|ttf)([\?]?.*)$/,
                loader: 'file-loader'
            },
        ]
    },
    plugins: [
        new CheckerPlugin(),
        new CommonsChunkPlugin({
            name: 'polyfills',
            chunks: ['polyfills']
        }),
        // This enables tree shaking of the vendor modules
        new CommonsChunkPlugin({
            name: 'vendor',
            chunks: ['main'],
            minChunks: module => /node_modules\//.test(module.resource)
        }),
        // Specify the correct order the scripts will be injected in
        new CommonsChunkPlugin({
            name: ['polyfills', 'vendor'].reverse()
        }),
        new CommonsChunkPlugin({
            name: ['manifest'],
            minChunks: Infinity,
        }),
        new InlineManifestWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            chunksSortMode: 'dependency',
            inject: 'head',
        }),
        new ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)@angular/, helpers.root('src'), {}
        ),
        new ScriptExtHtmlWebpackPlugin({
            defaultAttribute: 'defer'
        }),
        new CopyWebpackPlugin([
            { from: './src/assets', to: 'assets' },
            { from: './src/meta'}
        ],
        { ignore: [ 'icon.png' ]}),
      new ngcWebpack.NgcWebpackPlugin({
        disabled: !isProd,
        tsConfig: helpers.root('tsconfig-webpack.json'),
      })
    ],
    resolve:{
        extensions: ['.js', '.ts'],
        modules: [helpers.root('src'), helpers.root('node_modules')]
    },
    node: {
        global: true,
        crypto: 'empty',
        process: true,
        module: false,
        clearImmediate: false,
        setImmediate: false
    }
  }
}
