const helpers = require('./helpers');
const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.common.js');

const DefinePlugin = require('webpack/lib/DefinePlugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const HashedModuleIdsPlugin = require('webpack/lib/HashedModuleIdsPlugin');
const OptimizeJsPlugin = require('optimize-js-plugin');
const BrotliPlugin = require('brotli-webpack-plugin')
const ModuleConcatenationPlugin = require('webpack/lib/optimize/ModuleConcatenationPlugin')

const ENV = 'production';
const BASE_URL = '';

const METADATA = {
    baseUrl: BASE_URL,
    ENV: ENV,
};

module.exports = function () {
    return webpackMerge(commonConfig({env: ENV}), {
        devtool: 'source-map',
        output: {
            path: helpers.root('dist/META-INF/resources/fragaria/webapp'),
            filename: '[name].[chunkhash].bundle.js',
            sourceMapFilename: '[file].map',
            chunkFilename: '[name].[chunkhash].chunk.js'
        },
        module: {
          rules: [
            {
              test: /\.css$/,
              loader: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: 'css-loader'
              }),
              include: [helpers.root('src', 'styles')]
            },
            {
              test: /\.scss$/,
              loader: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: 'css-loader!sass-loader'
              }),
              include: [helpers.root('src', 'styles')]
            },
          ]
        },
        plugins: [
            new ModuleConcatenationPlugin(),
            new HashedModuleIdsPlugin(),
            new ExtractTextPlugin('[name].[contenthash].css'),
            new OptimizeJsPlugin({
                sourceMap: false
            }),
            new WebpackMd5Hash(),
            new DefinePlugin({
                'ENV': JSON.stringify(METADATA.ENV),
                'BASE_URL': JSON.stringify(METADATA.baseUrl)
            }),
            new UglifyJsPlugin({
                beautify: false,
                output: {
                    comments: false
                },
                mangle: {
                    screw_ie8: true,
                },
                compress: {
                    screw_ie8: true,
                    warnings: false,
                    conditionals: true,
                    unused: true,
                    comparisons: true,
                    sequences: true,
                    dead_code: true,
                    evaluate: true,
                    if_return: true,
                    join_vars: true,
                    negate_iife: false
                },
                sourceMap: true
            }),
            new FaviconsWebpackPlugin({
                logo: './src/assets/icon.png',
                prefix: 'icons-[hash]/',
                persistentCache: true,
                inject: true,
            }),
            new LoaderOptionsPlugin({
                minimize: true,
                debug: false,
                options: {
                    htmlLoader: {
                        minimize: true,
                        removeAttributeQuotes: false,
                        caseSensitive: true,
                        customAttrSurround: [
                            [/#/, /(?:)/],
                            [/\*/, /(?:)/],
                            [/\[?\(?/, /(?:)/],
                        ],
                        customAttrAssign: [/\)?\]?=/],
                    },
                }
            }),
            new BundleAnalyzerPlugin({
                analyzerMode: 'static',
                openAnalyzer: false,
                reportFilename: '../../../../../report.html'
            }),
            new BrotliPlugin({
                asset: '[path].br',
                test: /\.(js|css|html|svg)$/,
                treshold: 10240,
                minRatio: 0.8,
            }),
        ],
        node: {
          global: true,
          crypto: 'empty',
          process: false,
          module: false,
          clearImmediate: false,
          setImmediate: false
        }
    });
}
