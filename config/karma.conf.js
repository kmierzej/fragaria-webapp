const testWebpackConfig = require('./webpack.test.js');

module.exports = function (config) {
    var _config = {
        basePath: '',
        frameworks: ['jasmine'],
        exclude: [],
        files: [ { pattern: './config/spec-bundle.js', watched: false}],
        preprocessors: { './config/spec-bundle.js': ['webpack']},
        webpack: testWebpackConfig,
        webpackMiddleware: { stats: 'errors-only' },
        reporters: [ 'mocha' ],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: [
            'Chromium'
        ],
        singleRun: true
    };
    config.set(_config);
}
