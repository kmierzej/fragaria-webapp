const helpers = require('./helpers');
const webpackMerge = require('webpack-merge');
const webpackMergeDll = webpackMerge.strategy({plugins: 'replace'});
const commonConfig = require('./webpack.common.js');

const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const DllBundlesPlugin = require('webpack-dll-bundles-plugin').DllBundlesPlugin;
const NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const ENV = 'dev';
const BASE_URL = 'http://localhost:8080';
const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3000;

const METADATA = {
    host: HOST,
    port: PORT,
    baseUrl: BASE_URL,
    ENV: ENV,
};

module.exports = function () {
    return webpackMerge(commonConfig({env: ENV}), {
        devtool: 'eval',
        module: {
          rules: [
            {
              test: /\.css$/,
              use: ['style-loader', 'css-loader'],
              include: [helpers.root('src', 'styles')]
            },
            {
              test: /\.scss$/,
              use: ['style-loader', 'css-loader', 'sass-loader'],
              include: [helpers.root('src', 'styles')]
            },
          ]
        },

        output: {
            path: helpers.root('dist'),
            filename: '[name].bundle.js',
            sourceMapFilename: '[file].map',
        },

        devServer: {
            contentBase: '../src',
            port: METADATA.port,
            host: METADATA.host,
            //index.html package will be served instead of 404s
            historyApiFallback: true,
            watchOptions: {
                aggregateTimeout: 300,
                poll: 1000,
                ignored: 'node_modules'
            },
            stats: {
                warnings: false
            },
        },
        module: {
            rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
                include: [helpers.root('src', 'styles')]
            },
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
                include: [helpers.root('src', 'styles')]
            },
            ]
        },
        plugins: [
            new FaviconsWebpackPlugin({
                logo: './src/assets/icon.png',
                prefix: 'icons-[hash]/',
                persistentCache: true,
                inject: true,
                icons: {
                    android: false,
                    appleIcon: false,
                    appleStartup: false,
                    coast: false,
                    favicons: true,
                    firefox: false,
                    opengraph: false,
                    twitter: false,
                    yandex: false,
                    windows: false
                }
            }),
            new NamedModulesPlugin(),
            new DefinePlugin({
                'ENV': JSON.stringify(METADATA.ENV),
                'BASE_URL': JSON.stringify(METADATA.baseUrl)
            }),
            new DllBundlesPlugin({
                bundles: {
                    polyfills: [
                        'core-js',
                        'css-loader',
                        'deep-freeze',
                        'ngrx-store-freeze',
                        'reselect',
                        'hammerjs',
                        { name: 'core-js', path: 'core-js/es6/array.js' },
                        { name: 'core-js', path: 'core-js/es6/function.js' },
                        { name: 'core-js', path: 'core-js/es6/number.js' },
                        { name: 'core-js', path: 'core-js/es6/math.js' },
                        { name: 'core-js', path: 'core-js/es6/map.js' },
                        { name: 'core-js', path: 'core-js/es6/object.js' },
                        { name: 'core-js', path: 'core-js/es6/parse-float.js' },
                        { name: 'core-js', path: 'core-js/es6/parse-int.js' },
                        { name: 'core-js', path: 'core-js/es6/reflect.js' },
                        { name: 'core-js', path: 'core-js/es7/reflect.js' },
                        { name: 'core-js', path: 'core-js/es6/regexp.js' },
                        { name: 'core-js', path: 'core-js/es6/set.js' },
                        { name: 'core-js', path: 'core-js/es6/string.js' },
                        { name: 'core-js', path: 'core-js/es6/symbol.js' },
                        { name: 'core-js', path: 'core-js/es6/typed.js' },
                        { name: 'core-js', path: 'core-js/es6/weak-map.js' },
                        { name: 'core-js', path: 'core-js/es6/weak-set.js' },
                        { name: 'zone.js', path: 'zone.js/dist/zone.js' },
                        { name: 'zone.js', path: 'zone.js/dist/long-stack-trace-zone.js' },
                        { name: 'webpack-dev-server', path: 'webpack-dev-server/client/index.js' },
                    ],
                    vendor: [
                        '@angular/platform-browser',
                        '@angular/platform-browser-dynamic',
                        '@angular/core',
                        '@angular/common',
                        '@angular/forms',
                        '@angular/http',
                        '@angular/material',
                        '@angular/cdk',
                        '@angular/router',
                        '@angular/animations',
                        '@ngrx/effects',
                        '@ngrx/store',
                        'rxjs',
                        'file-saver',
                        { name: '@angularclass/hmr', path: '@angularclass/hmr/dist/index.js' },
                        { name: '@angularclass/hmr', path: '@angularclass/hmr/dist/helpers.js' },
                        { name: '@ngrx/core', path: '@ngrx/core/compose.js' },
                    ]
                },
                dllDir: helpers.root('dll'),
                webpackConfig: webpackMergeDll(commonConfig, {
                devtool: 'cheap-module-source-map',
                plugins: []
                }),
            }),
            new AddAssetHtmlPlugin([
                { filepath: helpers.root(`dll/${DllBundlesPlugin.resolveFile('polyfills')}`) },
                { filepath: helpers.root(`dll/${DllBundlesPlugin.resolveFile('vendor')}`) }
            ]),
        ],
    })
}
