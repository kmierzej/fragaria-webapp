require('ts-node/register');
var helpers = require('./helpers');

exports.config = {
    baseUrl: 'http://localhost:3000/',

    specs: [
        helpers.root('e2e/**/*.e2e.ts'),
    ],

    framework: 'jasmine2',

    allScriptsTimeout: 110000,

    jasmineNodeOpts: {
        showTiming: true,
        showColors: true,
        isVerbose: false,
        includeStackTrace: false,
        defaultTimeoutInterval: 400000
    },
    directConnect: true,

    capabilities: {
      browserName: 'chrome',
      chromeOptions: {
        prefs: {
          credentials_enable_service: false,
          profile: {
            password_manager_enabled: false,
          }
        }
      }
    },

    onPrepare: function() {
        browser.ignoreSynchronization = true;
    },

    useAllAngular2AppRoots: true,

    SELENIUM_PROMISE_MANAGER: false,
}