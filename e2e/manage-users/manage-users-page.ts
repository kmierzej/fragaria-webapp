import { browser, by, element, ElementFinder } from 'protractor';
import { PagedHeaderPage } from './paged-header-page';

export class ManageUsersPage {
    public static control = new PagedHeaderPage(element(by.id('users-control')));

    public static initialSpinner(): ElementFinder { return element(by.id('users-init-spinner')); }

    public static filterByInput(): ElementFinder { return element(by.id('filter-by-input')); }
    public static filterByButton(): ElementFinder { return element(by.id('filter-by-button')); }

    public static loadingAll(): ElementFinder { return element(by.id('users-loading')); }

    public static createUsers(): ElementFinder { return element(by.id('create-user-button')); }

    public static logins(): any {
        return element.all(by.css('.user-login')).map((elm) => elm.getText());
    }
}
