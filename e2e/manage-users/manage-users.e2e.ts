import { browser } from 'protractor';
import { assertLoggedAdmin, until } from '../auth-util';
import { ManageUsersPage } from './manage-users-page';

describe('Manage Users Module', () => {
  beforeAll(async () => {
    await browser.get('/');
  });

  beforeEach(async () => {
    const url = await browser.getCurrentUrl();
    await assertLoggedAdmin('admin', 'admin');
    if (url.indexOf('from=admin&limit=3') < 0) {
      await browser.driver.get(browser.baseUrl + '#/admin/users?from=admin&limit=3');
    }
    // bug, does not werk: browser.setLocation('admin/users')
    await browser.waitForAngular();
    await browser.wait(until.presenceOf(ManageUsersPage.initialSpinner()), 500).catch( () => true);
    await browser.wait(until.invisibilityOf(ManageUsersPage.initialSpinner()), 5000).catch( () => true);
  });

  it('Webapp receives users from backend', async () => {
    expect(ManageUsersPage.logins()).toEqual(['admin', 'duke', 'john']);
  });

  it('Next and previous buttons work', async () => {
    const nextSpinner = ManageUsersPage.control.nextSpinner();
    const previousSpinner = ManageUsersPage.control.nextSpinner();

    expect(ManageUsersPage.logins()).toEqual(['admin', 'duke', 'john'], 'Test data precondition');

    await ManageUsersPage.control.nextButton().click();
    await browser.wait(until.presenceOf(nextSpinner), 500).catch( () => true);
    await browser.wait(until.invisibilityOf(nextSpinner), 5000).catch( () => true);
    expect(ManageUsersPage.logins()).toEqual(['lars', 'mary'], 'Next button failure');

    await ManageUsersPage.control.previousButton().click();
    await browser.wait(until.presenceOf(previousSpinner), 500).catch( () => true);
    await browser.wait(until.invisibilityOf(previousSpinner), 5000).catch( () => true);
    await browser.wait(until.invisibilityOf(ManageUsersPage.loadingAll()), 5000).catch( () => true);
    expect(ManageUsersPage.logins()).toEqual(['admin', 'duke', 'john'], 'Previous button failure');
  });

  it('Find popup works', async () => {
    await ManageUsersPage.control.findButton().click();
    await browser.wait(until.presenceOf(ManageUsersPage.filterByInput()));
    await ManageUsersPage.filterByInput().sendKeys('duke');
    await ManageUsersPage.filterByButton().click();
    await browser.wait(until.presenceOf(ManageUsersPage.loadingAll()), 500).catch( () => true);
    await browser.wait(until.invisibilityOf(ManageUsersPage.loadingAll()), 5000).catch( () => true);
    expect(ManageUsersPage.logins()).toEqual(['duke', 'john', 'lars']);
    expect(browser.getCurrentUrl()).toContain('duke');
  });
});
