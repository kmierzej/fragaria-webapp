import { browser, by, element, ElementFinder } from 'protractor';

export class PagedHeaderPage {
    constructor (private element: ElementFinder) {}

    public nextButton(): ElementFinder {
        return this.element.element(by.className('next-button'));
    }

    public previousButton(): ElementFinder {
        return this.element.element(by.className('previous-button'));
    }

    public nextSpinner(): ElementFinder {
        return  this.nextButton().element(by.className('small-spinner'));
    }

    public previousSpinner(): ElementFinder {
        return  this.previousButton().element(by.className('small-spinner'));
    }

    public findButton(): ElementFinder {
        return  this.element.element(by.className('filter-button'));
    }
}
