import { browser, by, element, ElementFinder } from 'protractor';

export class MenuPrompt {
    public static loginButton(): ElementFinder { return element(by.id('login-button')); }
    public static username(): ElementFinder { return element(by.id('menu-login')); }
    public static appTitle(): ElementFinder { return element(by.id('app-title')); }

    public static menuButton(): ElementFinder { return element(by.id('menu-button')); }
    public static logoutButton(): ElementFinder { return element(by.buttonText('Wyloguj się')); }
    public static adminSessionButton(): ElementFinder {
        return element(by.partialButtonText('Sesja Administratora'));
    }

    public static manageUsersButton(): ElementFinder {
        return element(by.buttonText('Zarządzaj użytkownikami'));
    }
}
