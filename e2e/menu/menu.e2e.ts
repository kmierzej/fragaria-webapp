import { browser } from 'protractor';
import { until, assertLoggedAdmin, assertLoggedAs, assertLoggedOutAdmin } from '../auth-util';

import { MenuPrompt } from './menu-prompt';

describe('Login Module', () => {
  beforeEach( async () => {
    await browser.get('/');
  });

  it('Admin logged as user cannot navigate to manage users page', async () => {
    const mainAppMenu = MenuPrompt.menuButton();
    const manageUsers = MenuPrompt.manageUsersButton();
    const appTitle = MenuPrompt.appTitle();

    await assertLoggedAs('admin', 'admin');
    await assertLoggedOutAdmin();
    await mainAppMenu.click();
    await browser.wait(until.presenceOf(manageUsers), 5000, 'No manage users button');
    expect( await manageUsers.isEnabled()).toBeFalsy();
    await browser.actions().click(appTitle).perform();
  });

  it('Admin is able to navigate to manage users page', async () => {
    const mainAppMenu = MenuPrompt.menuButton();
    const manageUsers = MenuPrompt.manageUsersButton();
    const appTitle = MenuPrompt.appTitle();

    await assertLoggedAdmin('admin', 'admin');
    await mainAppMenu.click();
    await browser.wait(until.presenceOf(manageUsers), 5000, 'No manage users button -2 ');
    await browser.wait(until.elementToBeClickable(manageUsers), 5000, 'Manage users not clickable');
    await manageUsers.click();
    await browser.waitForAngular();

    await browser.sleep(200);
    expect(await browser.getCurrentUrl()).toContain('admin/users');
  });
});
