import { browser } from 'protractor';

describe('App', () => {

  beforeAll(async () => {
    await browser.get('/');
  });

  it('should have a title', async () => {
    const subject = await browser.getTitle();
    const result  = 'Fragaria';
    expect(subject).toEqual(result);
  });
});
