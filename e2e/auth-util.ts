import { browser, by, element, ExpectedConditions, protractor } from 'protractor';

import { UserLoginPrompt, AdminLoginPrompt } from './login';
import { MenuPrompt } from './menu';
import { AdminWarningPrompt } from './admin-warning';

export const until = ExpectedConditions;

export async function assertLoggedAs(username: string, password: string) {
    const loginElement = await MenuPrompt.username();
    const isPresent = await loginElement.isPresent();
    if (!isPresent) {
      await login(username, password);
    } else {
      const text = await loginElement.getText();
      if (text !== username) {
        await assertLoggedOut();
        await login(username, password);
        expect(await loginElement.getText()).toEqual(username);
      }
    }
}

export async function assertLoggedAdmin(username: string, password: string) {
    const dropAdminButton = AdminWarningPrompt.dropPrivilege();
    await browser.wait(until.presenceOf(dropAdminButton), 500).catch(() => ({}));
    if (!(await dropAdminButton.isPresent())) {
      await loginAdmin(username, password);
    }
}

export async function assertLoggedOut() {
    const mainAppMenu = MenuPrompt.menuButton();
    const loginButton = MenuPrompt.loginButton();
    const logoutButton = MenuPrompt.logoutButton();

    if ( await mainAppMenu.isPresent()) {
      await browser.actions().click(mainAppMenu).perform();
      await browser.wait(until.presenceOf(logoutButton));
      await logoutButton.click();
      await browser.wait(until.presenceOf(loginButton), 5000);
    }
}

export async function assertLoggedOutAdmin() {
    const logoutButton = AdminWarningPrompt.dropPrivilege();
    await browser.wait(until.presenceOf(logoutButton), 500).catch(() => ({}));
    if ( await logoutButton.isPresent()) {
      await logoutButton.click();
      await browser.wait(until.invisibilityOf(logoutButton), 5000);
    }
}

export async function login(login: string, password: string) {
    await MenuPrompt.loginButton().click();
    await UserLoginPrompt.username().sendKeys(login);
    await UserLoginPrompt.password().sendKeys(password);
    await UserLoginPrompt.button().click();

    const loginElement = MenuPrompt.username();
    await browser.wait(until.presenceOf(loginElement), 5000);
    expect(await loginElement.getText()).toEqual(login);
}

export async function loginAdmin(login: string, password: string) {
    await assertLoggedAs(login, password);

    // open admin prompt
    const mainAppMenu = MenuPrompt.menuButton();
    const openAdminLoginPromptButton = MenuPrompt.adminSessionButton();

    await mainAppMenu.click();
    await browser.wait(until.presenceOf(openAdminLoginPromptButton), 5000,
                          'LoginAdmin - waiting for presence of open Admin prompt button');
    await browser.wait(until.elementToBeClickable(openAdminLoginPromptButton), 5000,
                          'LoginAdmin - waiting for Admin prompt button to be clickable');
    await openAdminLoginPromptButton.click();

    // fill and send login form
    const adminPasswordInput = AdminLoginPrompt.password();
    const adminLoginButton = AdminLoginPrompt.button();

    await browser.wait(until.presenceOf(adminPasswordInput), 5000,
                          'LoginAdmin - waiting for presence of AdminPasswordInput');
    await adminPasswordInput.sendKeys(password);
    await adminLoginButton.click();

    // wait for login to complete
    const logoutAdminButton = element(by.id('admin-warning-logout'));

    await browser.wait(until.presenceOf(logoutAdminButton), 5000,
                          'LoginAdmin - waiting for Admin Warning Button');
}

export function highlightElement(byObject: any) {
    function setStyle(element: any, style: any) {
      const previous = element.getAttribute('style');
      element.setAttribute('style', style);
      setTimeout(() => {
        element.setAttribute('style', previous);
      }, 200);
      return 'highlighted';
    }

    let highlightStyle = 'color: red; background-color: yellow;';
    return browser.executeScript(setStyle, element(byObject).getWebElement(), highlightStyle);
}
