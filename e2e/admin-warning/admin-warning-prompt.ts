import { browser, by, element, ElementFinder } from 'protractor';

export class AdminWarningPrompt {
    public static dropPrivilege(): ElementFinder { return element(by.id('admin-warning-logout')); }
}
