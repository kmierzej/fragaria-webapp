import { browser } from 'protractor';
import { login, loginAdmin, assertLoggedOut, assertLoggedOutAdmin } from '../auth-util';

describe('Login Module', () => {
  beforeAll(async () => {
    await browser.get('/');
  });

  it('should be able to log in', async () => {
    await assertLoggedOut();
    await login('admin', 'admin');
  });

  it('should be able to log as Admin', async () => {
    await assertLoggedOutAdmin();
    await loginAdmin('admin', 'admin');
  });
});
