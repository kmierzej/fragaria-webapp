import { browser, by, element, ElementFinder } from 'protractor';

export class AdminLoginPrompt {
    public static button(): ElementFinder { return element(by.id('admin-login')); }
    public static password(): ElementFinder { return element(by.id('admin-password')); }
}
