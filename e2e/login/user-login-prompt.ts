import { browser, by, element, ElementFinder } from 'protractor';

export class UserLoginPrompt {
    public static button(): ElementFinder { return element(by.buttonText('Zaloguj się')); }
    public static password(): ElementFinder { return element(by.id('password')); }
    public static username(): ElementFinder { return element(by.id('username')); }
}
