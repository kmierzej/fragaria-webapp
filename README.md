# Fragaria Webapp #

### What it is? ###

Fragaria webapp is the frontend for https://bitbucket.org/kmierzej/fragaria-server.

### How to set it up? ###

You need to have npm and maven(for deployment) installed on your computer.
Yarn is highly suggested as dependency manager.

Install dependencies: 
```
yarn

```

Run dev build (fragaria-server should be running independently on localhost:8080) and navigate to http://localhost:3000/
```
yarn start
```

To deploy production optimized package (this may take a while) perform:
```
yarn deploy
```

### How to run tests? ###
To run e2e tests you have to run fragaria application (either dev or prod) in separate terminal, and then run
```
yarn e2e
```

### How to log in? ###
You can use default user credentials:
```
username: admin
password: admin
```