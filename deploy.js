var config = {
 "groupId": "pl.eightandcounting.fragaria",
 "type": "jar",
 "finalName": "{name}-{version}"
};
var maven = require('maven-deploy');
maven.config(config);
maven.install();

