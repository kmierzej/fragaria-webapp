import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpService, USER, ADMIN } from 'app/auth/token-services/http.service';
import { USER_RESOURCE } from 'app/endpoints';
import { IUser } from 'app/model/api';

@Injectable()
export class ManageUsersService {
    constructor(private http: HttpService) { }

    public findUserByName(name: string): Observable<IUser> {
        return this.http.get(USER_RESOURCE + '/' + name, [ADMIN, USER]).map((x) => x.json());
    }
}
