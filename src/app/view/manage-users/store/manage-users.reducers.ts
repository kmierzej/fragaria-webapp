import { MngResourceActions, MngResourceReducers } from 'app/shared/managed-resource/store';
import { IPagedResourceState } from 'app/shared/managed-resource/model';
import { IUser } from 'app/model/api';
import { USER_RESOURCE } from 'app/endpoints';

export const actions = new MngResourceActions.EventFactory('[USER]');
export type IState = IPagedResourceState<IUser>;
type PermissionSet = MngResourceReducers.IPermissionSet;

const permissions: PermissionSet = {
    view: ['ADMIN', 'USER'],
    modify: ['ADMIN'],
    remove: ['ADMIN'],
};

export const selectors = MngResourceReducers.managedResourceSelectorsFactory<IUser>();
export const initialState = MngResourceReducers.initialStateFactory(USER_RESOURCE, 'login', permissions, 'resources');
export const reducer = MngResourceReducers.reducerFactory(
    '[USER]',
    MngResourceActions.baseTags,
    initialState,
);
