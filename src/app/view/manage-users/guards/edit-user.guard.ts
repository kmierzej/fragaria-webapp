import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { LoggerActions } from 'app/prompt/logger/store';
import { ManageUsersService } from '../services';

import { AppReducers } from 'app/store';
import { MngUsersReducers } from '../store';

@Injectable()
export class EditUserGuard implements CanActivate {
    constructor(private store: Store<AppReducers.IState>,
                private usersService: ManageUsersService,
               ) {}

    public canActivate(route: ActivatedRouteSnapshot,
                       _: RouterStateSnapshot): Observable<boolean> {
        const id = route.params['id'];
        const selectedUser = this.store.select(AppReducers.MngUsersSelectors.getUser(id)).take(1);
        return  selectedUser.switchMap((user) => {
            if (user === undefined || user === null || user.value.login !== id) {
                return this.usersService.findUserByName(id)
                    .map((receivedUser) => {
                        const action = MngUsersReducers.actions.select(receivedUser, receivedUser.login);
                        this.store.dispatch(action);
                        return true;
                    })
                    .catch(() => {
                        const msg = 'Użytkownik niedostępny: ' + route.url;
                        this.store.dispatch(new LoggerActions.LogMessageAction(msg));
                        return Observable.of(false);
                    });
            }
            return Observable.of(true);
        });
    }
}
