import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { IUser, userStatesMap } from 'app/model/api';

@Component ({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: '[frg-user-list-entry]', // tslint:disable-line
    template: `
        <td class="user-login" width="30%">
          <button md-button style="text-align: left;" (click)="edit.emit(user.value.login)">
            {{user.value.login}}
          </button>
        </td>
        <td width="30%">{{user.value.fullname}}</td>
        <td> {{getPermissions()}} </td>
        <td>
          <button md-button (click)="log.emit(user.value.login)">
            <md-icon>timer</md-icon>
          </button>
          <button md-button (click)="delete.emit(user.value.login)">
            <md-icon>delete_forever</md-icon>
          </button>
        </td>
    `,
})
export class UserListEntryComponent {
    @Input()
    public user: {value: IUser, key: string};

    @Output()
    public edit = new EventEmitter();

    @Output()
    public delete = new EventEmitter();

    @Output()
    public log = new EventEmitter();

    public getPermissions() {
      return this.user.value.permissions.map((it) => userStatesMap[it].display).join(', ');
    }
}
