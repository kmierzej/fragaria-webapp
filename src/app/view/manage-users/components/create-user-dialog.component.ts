import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MdDialogRef } from '@angular/material';

import { ManageUsersService } from 'app/view/manage-users/services';
import { IUser } from 'app/model/api';

import { AsyncValidator, getErrorMessage } from 'app/shared/paged-dialogs/utils';

@Component({
    providers: [ManageUsersService],
    selector: 'frg-create-user-dialog',
    template: `
    <h2 md-dialog-title style="text-align:center">Nowe konto</h2>
    <form role="form" [formGroup]="form"
        (ngSubmit)="createNewAccount()">
      <div class="form-group" style="margin-bottom: 10px">
        <md-input-container>
          <input mdInput type="text" #login id="login" placeholder="Login"
          formControlName="login">
          <md-hint *ngIf="hasError('login')" [ngStyle]="{'color': 'red'}" align="start">{{getLoginError()}}</md-hint>
        </md-input-container>
      </div>
      <div>
        <md-input-container>
          <input mdInput type="text" #email id="email" placeholder="E-mail" formControlName="email">
        </md-input-container>
      </div>
      <div>
        <md-input-container>
          <input mdInput type="text" #fullname id="fullname" placeholder="Imię i nazwisko" formControlName="fullname">
        </md-input-container>
      </div>
      <div class="form-group" >
        <md-input-container>
          <input mdInput [type]="passwordInputType(showPassword.checked)"
                    #password id="password"
                    placeholder="Hasło"
                    autocomplete="new-password"
                    formControlName="password">
          <md-hint *ngIf="hasError('password')" [ngStyle]="{'color': 'red'}" align="start">
            {{getPasswordError()}}
          </md-hint>
        </md-input-container>
        <button md-button type="button" (click)="generatePassword()">Wygeneruj hasło</button>
      </div>
      <md-checkbox #showPassword id="showPassword" formControlName="showPassword">
        Pokaż hasło.
      </md-checkbox>
      <div class="form-group" style="text-align: center; margin-top: 10px;">
        <button md-raised-button color="accent" type="submit"
          [disabled]="!form.valid">Utwórz nowe konto</button>
        <button md-raised-button color="warn" type="button"
          (click)="dialogRef.close()"> Anuluj </button>
      </div>
    </form>
    `,
})
export class CreateUserDialogComponent implements OnInit {
    public form: FormGroup;

    constructor(
      public userService: ManageUsersService,
      public dialogRef: MdDialogRef<IUser>,
      private fb: FormBuilder,
    ) {}

    public ngOnInit() {
      this.form = this.createForm();
    }

    public getLoginError(): string {
      const dict = {
        unique: 'Użytkownik o tym loginie już istnieje.',
        required: 'Login jest pusty.',
        minlength: 'Ten login jest za krótki.',
        maxlength: 'Ten login jest za długi.',
      };
      const login = this.form.get('login');
      return getErrorMessage(login, dict);
    }

    public getPasswordError(): string {
      const dict = { required: 'Hasło jest puste.' };
      const comp = this.form.get('password');
      return getErrorMessage(comp, dict);
    }

    public hasError(comp: string): boolean {
      const login = this.form.get(comp);
      return login.hasError && login.touched;
    }

    public passwordInputType = (checked: boolean) => checked ? 'text' : 'password';

    public createNewAccount() {
      const raw = this.form.getRawValue();
      const result = Object.assign(raw, {permissions: ['USER']});
      delete result['showPassword'];
      return this.dialogRef.close(result);
    }

    public generatePassword() {
      const password = Math.random().toString(36).substr(2, 8);
      this.form.patchValue({password, showPassword: true});
    }

    private createForm() {
      return this.fb.group({
          email: [ null, Validators.required ],
          fullname: [],
          login: [
            null,
            Validators.compose([
              Validators.required,
              Validators.minLength(3),
              Validators.maxLength(30),
            ]),
            new AsyncValidator(this.checkUser()).validate],
          password: [null, Validators.required],
          showPassword: [null],
        });
    }

    private checkUser() {
      return (name: string) => this.userService.findUserByName(name).map((it) => it != null);
    }
}
