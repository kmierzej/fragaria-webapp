import { Component, ChangeDetectorRef, OnDestroy, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';
import { AppReducers } from 'app/store';
import { MngUsersReducers } from '../store';

import { CreateUserDialogComponent } from './create-user-dialog.component';

import { QueryParamsService } from 'app/shared/managed-resource/services';
import { PagedResourceDialogsService } from 'app/shared/paged-dialogs/services';

import { userOption, adminOption, allUserStates} from 'app/model/api';

const pageSelector = AppReducers.MngUsersSelectors.getPage;
const actions = MngUsersReducers.actions;

@Component({
    selector: 'frg-manage-users',
    template: `
    <md-card>
      <table id="user-table">
        <tr>
          <th width="30%">
            <frg-paged-header id="users-control" class="nowrap"
              [state]="state.pageState"
              [title]="'login'"
              (find)="openFilterDialog()"
              (next)="next()"
              (previous)="previous()"
            ></frg-paged-header>
          </th>
          <th>imię i nazwisko</th>
          <th>
            <button md-button (click)="selectStatus()"> uprawnienia </button>
          </th>
          <th></th>
        </tr>
        <tr frg-user-list-entry *ngFor="let user of state.resources"
          [user]="user"
          (edit)="editUser($event)"
          (log)="logUser($event)"
          (delete)="deleteUser($event)">
        </tr>
      </table>
      <md-spinner *ngIf="state.pageState === 'INITIAL'"
                  class="centered-spinner"
                  mode="indeterminate"
                  color="accent"
                  id="users-init-spinner">
      </md-spinner>

      <div id="users-loading" *ngIf="state.pageState === 'LOADING_ALL'"> </div>

      <div width="100%" style="text-align: right;">
        <button md-fab id="create-user-button"
          type="button" color="accent"
          (click)="openCreateUserDialog()">
          <md-icon class="md-24">add</md-icon>
        </button>
      </div>
    </md-card>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManageUsersComponent implements OnDestroy, OnInit {
  public state: MngUsersReducers.IState = MngUsersReducers.initialState;

  private subs: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private store: Store<AppReducers.IState>,
    private dialogService: PagedResourceDialogsService,
    private queryParams: QueryParamsService,
  ) { }

  public ngOnInit() {
   // Load state
   const stateSubscription = this.store.select(AppReducers.MngUsersSelectors.getState)
       .subscribe((it) => {
          this.state = it;
          this.cdr.markForCheck();
       });

   // Adjust metadata on url change
   const urlChangeSubscription = this.queryParams.getSubscription(this.route.queryParams, [], ['permission'])
       .subscribe((action) => {
         actions.setMeta(action).dispatch(this.store);
       });

   this.subs.push(stateSubscription);
   this.subs.push(urlChangeSubscription);
  }

  public ngOnDestroy() {
    this.subs.forEach((it) => it.unsubscribe());
    this.dialogService.cleanup();
  }

  public next() {
    this.queryParams.next(this.store, pageSelector, this.router, actions);
  }

  public previous() {
    this.queryParams.previous(this.store, pageSelector, this.router, actions);
  }

  public openCreateUserDialog() {
    this.dialogService.createDialog(CreateUserDialogComponent)
      .subscribe((x) => {
        actions.create(x, x.login).dispatch(this.store);
      });
  }

  public editUser(login: string) {
    this.router.navigateByUrl('admin/users/' + login);
  }

  // SETUP SHARED DIALOGS
  public selectStatus() {
    const filtered = this.state.filters['permission'] || [adminOption.value, userOption.value];
    const options = allUserStates.map((permission) => {
      const isFiltered = filtered.indexOf(permission.value) > -1;
      return Object.assign({}, permission, {checked: isFiltered});
    });
    this.dialogService.openSelectOptionDialog(options).subscribe((x) => {
      this.queryParams.setQueryParam(this.store, pageSelector,  this.router, actions, 'permission', x);
    });
  }

  public openFilterDialog() {
    this.dialogService.openFilterDialog().subscribe((result) => {
      this.queryParams.from(this.store, pageSelector, this.router, actions, result);
    });
  }

  public deleteUser(id: string) {
    this.dialogService.openDeleteDialog(this.state.confirmDelete, id)
        .subscribe((x: any) => {
          this.store.dispatch(actions.setDeleteConfirmation(x.confirmDelete));
          this.store.dispatch(actions.delete(id));
        });
  }

  public logUser(id: string) {
    this.router.navigate(['/admin/users', id, 'dashboard']);
  }
}
