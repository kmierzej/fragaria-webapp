import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'frg-user-dashboard',
    template: `
    <md-card>
      <div style="padding-bottom: 10px;">
        <frg-navigate-before [label]="message | async" [back]="'../..'" [editVisible]="false">
        </frg-navigate-before>
      </div>
      <frg-dashboard [loginObs]="loginObs">
      </frg-dashboard>
    </md-card>
    `,
})
export class UserDashboardComponent implements OnInit {
    public loginObs: Observable<string>;
    public message: Observable<string>;

    constructor(
        private route: ActivatedRoute,
    ) {}

    public ngOnInit() {
      this.loginObs = this.route.params.map((it) => it.id);
      this.message = this.loginObs.map((it) => 'Dashboard użytkownika ' + it);
    }
}
