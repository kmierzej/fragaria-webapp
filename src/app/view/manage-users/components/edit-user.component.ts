import { Component, ChangeDetectionStrategy,
         ChangeDetectorRef, OnDestroy, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { IUser } from 'app/model/api';
import { AppReducers } from 'app/store';

import { LoggerActions } from 'app/prompt/logger/store';
import { EditPropertyComponent, IEditProperty, EditPropertyService } from 'app/shared/edit-property';

import * as MngUsersActions from 'app/view/manage-users/store';
const actions = MngUsersActions.MngUsersReducers.actions;

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-edit-user',
    template: `
    <md-card>
      <div style="padding-bottom: 10px;">
        <frg-navigate-before [label]="'Edytuj użytkownika: ' + user.value.login" [editVisible]="false">
        </frg-navigate-before>
      </div>

      <frg-edit-property [label]="'Imię i nazwisko'"
                     [property]="user.value.fullname"
                     (propertyChange)="setProperty('fullname', $event)"
                     #fullname>
      </frg-edit-property>

      <frg-edit-property [label]="'email'"
                     [property]="user.value.email"
                     (propertyChange)="setProperty('email', $event)"
                     #email>
      </frg-edit-property>

      <h4>Zmiana hasła:</h4>
      <frg-edit-property [label]="'Hasło'"
                     [property]="'Nowe hasło'"
                     [type]="'password'"
                     [edit]="true"
                     (propertyChange)="setProperty('password', $event)"
                     #password>
      </frg-edit-property>

      <h4>Zarządzanie uprawnieniami:</h4>
      <frg-select-property [selectedProperties] = "user.value.permissions"
                       [allProperties] = "allPermissions"
                       [label] = "'Aktualne uprawnienia:'"
                       [buttonLabel] = "'Nadaj uprawnienie'"
                       [placeholder] = "'Uprawnienia'"
                       (change) = "setProperty('permissions', $event)"
                       #permissions>
      </frg-select-property>
    </md-card>
    `,
})
export class EditUserComponent implements OnDestroy, OnInit {
    @ViewChild('fullname') public fullname: IEditProperty;
    @ViewChild('email') public email: IEditProperty;
    @ViewChild('password') public password: IEditProperty;
    @ViewChild('permissions') public permissions: IEditProperty;

    public user: {value: IUser, key: string} = {value: { email: null, password: null, fullname: null}, key: null};
    public allPermissions = ['ADMIN', 'USER'];
    private userSub: Subscription;

    constructor(
      private store: Store<AppReducers.IState>,
      private route: ActivatedRoute,
      private usersPermissions: EditPropertyService,
      private cdr: ChangeDetectorRef,
    ) {}

    public ngOnInit() {
        this.userSub = this.route.params.map((it) => it['id'])
          .switchMap((it) => this.store.select(AppReducers.MngUsersSelectors.getUser(it)))
          .subscribe((it) => {
            this.user = it;
            this.cdr.markForCheck();
          });
    }

    public ngOnDestroy() {
      this.userSub.unsubscribe();
    }

    public setProperty(label: string, property: string | string[]) {
      this.modifyUser(label, property)
        .finally(() => ((this as any)[label] as EditPropertyComponent).enable())
        .subscribe((user) => {
          this.store.dispatch(actions.select(user, user.login));
        });
    }

    private modifyUser(key: string, value: any): Observable<IUser> {
      return this.usersPermissions.editUserProperty(this.user.value.login, key, value)
        .catch((error) => {
          const message = 'Nie udało się zmodifikować użytkownika';
          this.store.dispatch(new LoggerActions.LogMessageAction(message));
          return Observable.throw(error);
        });
    }
}
