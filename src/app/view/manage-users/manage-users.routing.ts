import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminAuthGuard } from 'app/guards';
import { ManageUsersComponent, EditUserComponent, UserDashboardComponent } from './components';
import { EditUserGuard } from './guards';
import { ManageUsersService } from './services';

const routes: Routes = [
    {
      canActivate: [AdminAuthGuard],
      component: ManageUsersComponent,
      path: '',
    },
    {
      canActivate: [AdminAuthGuard, EditUserGuard],
      component: EditUserComponent,
      path: ':id',
    },
    {
      component: UserDashboardComponent,
      path: ':id/dashboard',
    },
];

@NgModule({
    exports: [ RouterModule ],
    imports: [
      RouterModule.forChild(routes),
    ],
    providers: [
      AdminAuthGuard,
      EditUserGuard,
      ManageUsersService,
    ],
})
export class ManageUsersRoutingModule { }
