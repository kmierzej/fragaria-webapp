import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/shared/material';

import { RouterModule } from '@angular/router';

import {
    ManageUsersComponent,
    UserListEntryComponent,
    EditUserComponent,
    CreateUserDialogComponent,
    UserDashboardComponent,
} from './components';

import { EditPropertyModule } from 'app/shared/edit-property';
import { PagedHeaderModule } from 'app/shared/paged-header';

import { PagedResourceDialogsModule } from 'app/shared/paged-dialogs';

import { ManageUsersRoutingModule } from './manage-users.routing';
import { QueryParamsService } from 'app/shared/managed-resource/services';
import { ManageUsersService } from './services';
import { DashboardModule } from 'app/view/dashboard';
import { ProjectsCommonModule } from 'app/shared/projects-common';

const DIALOGS = [ CreateUserDialogComponent ];

@NgModule({
    declarations: [
        ManageUsersComponent,
        EditUserComponent,
        UserListEntryComponent,
        UserDashboardComponent,
        ... DIALOGS,
    ],
    exports: [
        ManageUsersComponent,
    ],
    entryComponents: [ ... DIALOGS ],
    imports: [
        MaterialModule,
        ManageUsersRoutingModule,
        CommonModule,
        FormsModule,
        PagedHeaderModule,
        ReactiveFormsModule,
        RouterModule,
        DashboardModule,
        ProjectsCommonModule,

        EditPropertyModule,
        PagedResourceDialogsModule,
    ],
    providers: [
        QueryParamsService,
        ManageUsersService,
    ],
})
export class ManageUsersModule {}
