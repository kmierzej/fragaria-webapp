import { Component } from '@angular/core';

@Component({
    selector: 'frg-default',
    template: `
    <md-card>
      <md-card-title style="text-align: center;"> Nieznany adres strony ...</md-card-title>
    </md-card>
    `,
})
export class DefaultComponent {

}
