import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectsComponent, ProjectsModule } from './project-list';
import { ProjectDetailsComponent, TaskDetailsComponent, ProjectDetailsModule } from './project-details';
import { TasksComponent, TasksModule } from './project-tasks-list';

const routes: Routes = [
    {
      component: ProjectsComponent,
      path: '',
    },
    {
      component: TasksComponent,
      path: ':id',
    },
    {
      component: ProjectDetailsComponent,
      path: ':id/edit',
    },
    {
      component: TaskDetailsComponent,
      path: ':pid/:tid',
    },
];

@NgModule({
    exports: [ RouterModule ],
    imports: [
      RouterModule.forChild(routes),
      ProjectsModule,
      ProjectDetailsModule,
      TasksModule,
    ],
})
export class ProjectsRoutingModule { }
