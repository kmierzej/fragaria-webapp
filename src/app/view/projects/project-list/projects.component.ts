import { Component, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';

import { AppReducers } from 'app/store';
import { actions, IState, initialState } from '../store/projects.reducers';

import { CreateProjectDialogComponent } from './components';
import { PagedResourceDialogsService } from 'app/shared/paged-dialogs/services';
import { QueryParamsService } from 'app/shared/managed-resource/services';

import { allStatus, openStatus, pendingStatus } from 'app/model/api';

import { AuthStore } from 'app/auth/token-services/auth.store';
import { getAdminState } from 'app/auth/token-services/auth.reducers';

@Component({
    selector: 'frg-projects-component',
    template: `
    <md-card>
      <table id="user-table">
        <tr>
          <th width="30%">
            <frg-paged-header id="users-control" class="nowrap"
              [state]="state.pageState"
              [title]="'kod'"
              (find)="openFilterDialog()"
              (next)="next()"
              (previous)="previous()"
            ></frg-paged-header>
          </th>
          <th>tytuł</th>
          <th>
            <button md-button (click)="selectStatus()"> status </button>
          </th>
          <th></th>
        </tr>
        <tr project-user-list-entry *ngFor="let project of state.resources"
          [project]="project"
          [isAdmin]="adminState.isAdmin"
          [canAdmin]="adminState.canBeAdmin"
          (edit)="editProject($event)"
          (delete)="deleteProject($event)">
        </tr>
      </table>
      <md-spinner *ngIf="state.pageState === 'INITIAL'"
                  class="centered-spinner"
                  mode="indeterminate"
                  color="accent"
                  id="users-init-spinner">
      </md-spinner>

      <div id="users-loading" *ngIf="state.pageState === 'LOADING_ALL'"> </div>

      <div *ngIf="adminState.canBeAdmin " width="100%" style="text-align: right;">
        <button md-fab id="create-project-button"
          type="button" color="accent"
          [disabled]="!(adminState.isAdmin)"
          (click)="openCreateProjectDialog()">
          <md-icon class="md-24">add</md-icon>
        </button>
      </div>
    </md-card>
    `,
})
export class ProjectsComponent implements OnDestroy, OnInit {
  public state: IState = initialState;
  public adminState = { isAdmin: false, canBeAdmin: false};

  private subs: Subscription[] = [];
  private pageSelector = AppReducers.MngProjectsSelectors.getPage;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: PagedResourceDialogsService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppReducers.IState>,
    private auth: AuthStore,
    private queryParams: QueryParamsService,
  ) { }

  public openCreateProjectDialog() {
    this.dialogService.createDialog(CreateProjectDialogComponent)
      .subscribe((x) => {
        actions.create(x, x.id).dispatch(this.store);
      });
  }

  public ngOnInit() {
   const adminSub = this.auth.observe(getAdminState).subscribe((state) => {
     this.adminState = state;
     this.cdr.markForCheck();
   });
   this.subs.push(adminSub);

   // Load state
   const stateSub = this.store.select(AppReducers.MngProjectsSelectors.getState)
     .subscribe((it) => {
       this.state = it;
       this.cdr.markForCheck();
     });
   this.subs.push(stateSub);

   // Adjust metadata on url change
   const urlChangeSub = this.queryParams.getSubscription(this.route.queryParams, [], ['status'])
     .subscribe((action) => {
       actions.setMeta(action).dispatch(this.store);
     });
   this.subs.push(urlChangeSub);
  }

  public ngOnDestroy() {
    this.subs.forEach((it) => it.unsubscribe());
    this.dialogService.cleanup();
  }

  public next() {
    this.queryParams.next(this.store, this.pageSelector, this.router, actions);
  }

  public previous() {
    this.queryParams.previous(this.store, this.pageSelector, this.router, actions);
  }

  public editProject(projectId: string) {
    this.router.navigate([projectId], {relativeTo: this.route});
  }

  public selectStatus() {
    const filtered = this.state.filters['status'] || [openStatus.value, pendingStatus.value];
    const options = allStatus.map((status) => {
      const isFiltered = filtered.indexOf(status.value) > -1;
      return Object.assign({}, status, {checked: isFiltered});
    });

    this.dialogService.openSelectOptionDialog(options).subscribe((x) => {
      this.queryParams.setQueryParam(this.store, this.pageSelector,  this.router, actions, 'status', x);
    });
  }

  public openFilterDialog() {
    this.dialogService.openFilterDialog().subscribe((result) => {
        this.queryParams.from(this.store, this.pageSelector, this.router, actions, result);
    });
  }

  public deleteProject(id: string) {
    this.dialogService.openDeleteDialog(this.state.confirmDelete, id)
        .subscribe((x: any) => {
          this.store.dispatch(actions.setDeleteConfirmation(x.confirmDelete));
          this.store.dispatch(actions.delete(id));
        });
  }
}
