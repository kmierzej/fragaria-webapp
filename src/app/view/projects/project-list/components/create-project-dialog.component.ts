import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MdDialogRef } from '@angular/material';

import { ProjectsService } from 'app/model/api-services';
import {
  allStatus, projectVisibilityOptions, openStatus, IProject,
  projectCreateTaskOptions, projectLoggingWhoOptions, assignedSetting, managerSetting,
} from 'app/model/api/project';
import { estimateValidatorFactory } from 'app/shared/estimate-validator';
import { estimateStringToMinutes } from 'app/shared/estimate-validator/utils';

import { AsyncValidator, getErrorMessage } from 'app/shared/paged-dialogs/utils';

@Component({
    selector: 'frg-create-user-dialog',
    template: `
    <h2 md-dialog-title style="text-align:center">Nowy projekt</h2>
    <md-dialog-content>
    <form role="form" [formGroup]="form" (ngSubmit)="createNewAccount()">

      <div style="margin-bottom: 10px">
        <md-input-container>
          <input mdInput type="text" #id id="project-id" placeholder="Kod"
          formControlName="id">
          <md-hint *ngIf="hasError('id')" [ngStyle]="{'color': 'red'}" align="start">{{getIdError()}}</md-hint>
        </md-input-container>
      </div>

      <div style="margin-bottom: 10px">
        <md-input-container>
          <input mdInput type="text" #title id="project-title" placeholder="Tytuł" formControlName="title">
        </md-input-container>
      </div>

      <div style="margin-bottom: 10px">
        <md-input-container>
          <input mdInput type="text" #estimatedTime id="project-estimate"
            placeholder="Czas wykonania" formControlName="estimatedTime">
        <md-error *ngIf="!estimatedTime.valid">{{badEstimate}}</md-error>
        </md-input-container>
      </div>

      <div style="margin-bottom: 25px">
        <md-select placeholder="Status" #status id="project-status" formControlName="status">
          <md-option *ngFor="let state of allStatus" [value]="state.value">{{ state.display }}</md-option>
        </md-select>
      </div>

      <div style="margin-bottom: 25px">
        <md-select placeholder="Widoczność" #visibility id="project-visibility" formControlName="visibilityStrategy">
          <md-option *ngFor="let state of visibilityOptions" [value]="state.value">{{ state.display }}</md-option>
        </md-select>
      </div>

      <div style="margin-bottom: 25px">
        <md-select placeholder="Logowanie czasu" #status id="project-logWho" formControlName="loggingWhoStrategy">
          <md-option *ngFor="let state of loggingWhoOptions" [value]="state.value">{{ state.display }}</md-option>
        </md-select>
      </div>

      <div style="margin-bottom: 10px">
        <md-select placeholder="Tworzenie zadań" #status id="project-createTask" formControlName="createTaskStrategy">
          <md-option *ngFor="let state of createTaskOptions" [value]="state.value">{{ state.display }}</md-option>
        </md-select>
      </div>

      <div style="margin-bottom: 10px">
        <frg-select-manager [manager]="manager"
          (selectManager)="selectManager($event)">
        </frg-select-manager>
      </div>

      <frg-select-assignees (addUsers)="setAssignees($event)">
      </frg-select-assignees>

      <div style="text-align: center; margin-top: 10px;" class="nowrap">
        <button md-raised-button color="accent" type="submit"
          [disabled]="!form.valid">Utwórz nowy projekt</button>
        <button md-raised-button color="warn" type="button"
          (click)="dialogRef.close()"> Anuluj </button>
      </div>
    </form>
    </md-dialog-content>
    `,
})
export class CreateProjectDialogComponent implements OnInit {
    public form: FormGroup;

    public visibilityOptions = projectVisibilityOptions;
    public createTaskOptions = projectCreateTaskOptions;
    public loggingWhoOptions = projectLoggingWhoOptions;

    public allStatus = allStatus;
    public defaultStatus = openStatus;

    public manager: string;
    public assignees: string[] = [];
    public badEstimate = 'Nieprawidłowy format. Przykładowa poprawna wartość to: 1MO 1W 3D 3H 10M';

    constructor(
      public projectsService: ProjectsService,
      public dialogRef: MdDialogRef<IProject>,
      private fb: FormBuilder,
    ) {}

    public ngOnInit() {
      this.form = this.createForm();
    }

    public setAssignees(assignees: string[]) {
      this.assignees = assignees;
    }

    public getIdError(): string {
      const dict = {
        unique: 'Projekt o tym kodzie już istnieje.',
        required: 'Kod jest pusty.',
        minlength: 'Ten kod jest za krótki.',
        maxlength: 'Ten kod jest za długi.',
      };
      const comp = this.form.get('id');
      return getErrorMessage(comp, dict);
    }

    public hasError(comp: string): boolean {
      const component = this.form.get(comp);
      return component.hasError && component.touched;
    }

    public selectManager(manager: {display: string, value: any}) {
      this.manager = manager.value.login;
    }

    public createNewAccount() {
      const raw = this.form.getRawValue();
      const result = Object.assign(raw, {
        manager: this.manager,
        assignees: this.assignees,
        estimatedTime: estimateStringToMinutes(raw.estimatedTime),
      });
      return this.dialogRef.close(result);
    }

    private createForm() {
      return this.fb.group({
          id: [null,
                  Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(5),
                  ]),
                  new AsyncValidator(this.checkProject()).validate],
          title: [null, Validators.required],
          status: [this.defaultStatus.value],
          visibilityStrategy: [assignedSetting.value],
          loggingWhoStrategy: [assignedSetting.value],
          createTaskStrategy: [managerSetting.value],
          estimatedTime: [null, estimateValidatorFactory() ],
        });
    }

    private checkProject() {
      return (name: string) => this.projectsService.findProjectByName(name).mapTo(true);
    }
}
