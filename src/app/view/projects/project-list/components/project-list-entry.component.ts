import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { IProject, statusMap } from 'app/model/api';

@Component ({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: '[project-user-list-entry]', // tslint:disable-line
    template: `
    <td class="project-id" width="30%">
      <button md-button style="text-align: left;" (click)="edit.emit(project.value.id)">
        {{project.value.id}}
      </button>
    </td>
    <td>{{project.value.title}}</td>
    <td>{{getStatus(project.value)}}</td>
    <td>
      <button md-button *ngIf="canAdmin"
        [disabled]="!isAdmin"
        (click)="delete.emit(project.value.id)">
       <md-icon>delete_forever</md-icon>
      </button>
    </td>
    `,
})
export class ProjectListEntryComponent {
    public statusMap = statusMap;

    @Input()
    public project: {value: IProject, key: string};

    @Input()
    public isAdmin: boolean;
    @Input()
    public canAdmin: boolean;

    @Output()
    public edit = new EventEmitter();

    @Output()
    public delete = new EventEmitter();

    public getStatus(project: IProject) {
      return statusMap[project.status].display;
    }

}
