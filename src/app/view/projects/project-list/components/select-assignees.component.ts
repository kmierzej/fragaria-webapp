import { Component, Output, EventEmitter } from '@angular/core';

import { IUser } from 'app/model/api';

@Component({
    selector: 'frg-select-assignees',
    template: `
    <div>
      <span>Pracownicy:</span>
      <frg-assignee-details *ngFor="let assignee of users"
                            [assignee]="assignee"
                            [canModify]="true"
                            (delete)="deleteAssignee($event)" >
      </frg-assignee-details>
    </div>

    <frg-add-assignees
        [users]="filtered"
        (aUsers)="addQuerried($event)"
        (qUsers)="filterQuerried($event)" >
    </frg-add-assignees>
    `,
})
export class SelectAssigneesComponent {
  @Output()
  public addUsers: EventEmitter<string[]> = new EventEmitter();

  public users: Array<{display: string, value: IUser}> = [];
  public querried: Array<{display: string, value: IUser}> = [];
  public filtered: Array<{display: string, value: IUser}> = [];

  public addQuerried(users: Array<{display: string, value: IUser}>) {
    let changed = false;
    users.forEach((user) => {
      if (this.users.findIndex((it) => it.value.login === user.value.login) > 0) {
        return;
      }
      changed = true;
      this.users.push(user);
    });
    if (changed) {
      this.filter();
      const toEmit = this.users.map((it) => it.value.login);
      this.addUsers.emit(toEmit);
    }
  }

  public deleteAssignee(user: {display: string, value: IUser}) {
    this.users = this.users.filter((it) => it.value.login !== user.value.login);
    this.filter();
    const toEmit = this.users.map((it) => it.value.login);
    this.addUsers.emit(toEmit);
  }

  public filterQuerried(users: Array<{display: string, value: IUser}>) {
    this.querried = users;
    this.filter();
  }

  private filter() {
    this.filtered = this.querried.filter((entry) =>
      this.users.findIndex((it) => it.value.login === entry.value.login) < 0,
    );
  }
}
