import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'app/shared/material';

import { PagedHeaderModule } from 'app/shared/paged-header';

import {
    ProjectListEntryComponent,
    CreateProjectDialogComponent,
    SelectAssigneesComponent,
 } from './components';
import { ProjectsComponent } from './projects.component';
import { ProjectDetailsModule } from '../project-details';

import { QueryParamsService } from 'app/shared/managed-resource/services';

import { PagedResourceDialogsModule } from 'app/shared/paged-dialogs';

import { TasksModule } from '../project-tasks-list';
import { ProjectsCommonModule } from 'app/shared/projects-common';
import { SelectAssigneesModule } from 'app/shared/select-assignees';

const DIALOGS = [ CreateProjectDialogComponent ];
const SERVICES = [
    QueryParamsService,
];

@NgModule({
    declarations: [
        ProjectsComponent,
        ProjectListEntryComponent,
        SelectAssigneesComponent,
        ...DIALOGS,
    ],
    exports: [
        ProjectsComponent,
    ],
    entryComponents: [ ... DIALOGS ],
    imports: [
        CommonModule,
        MaterialModule,
        PagedHeaderModule,
        PagedResourceDialogsModule,
        ProjectDetailsModule,
        FormsModule,
        ReactiveFormsModule,
        TasksModule,
        ProjectsCommonModule,
        SelectAssigneesModule,
    ],
    providers: [ ... SERVICES ],
})
export class ProjectsModule {}
