import {
  Component,
  ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { actions } from 'app/view/projects/project-tasks-list/store/tasks.reducers';
import { PROJECT_RESOURCE } from 'app/endpoints';

import { ITask } from 'app/model/api';

import { TasksService } from 'app/model/api-services';

import { MngTasksReducers } from '../project-tasks-list/store';

@Component ({
    selector: 'frg-task-details',
    template: `
      <md-card>
        <md-card-title>
          <frg-navigate-before [label]="project.title" [editVisible]="false">
          </frg-navigate-before>
        </md-card-title>

        <frg-select-title [title]="project.title" (newTitle)="changeStrategy('title', $event)">
        </frg-select-title>

        <frg-select-option [defaultOption]="project.status"
                           [options]="statusOptions"
                           [label]="'Status: '"
                           (choice)="changeStrategy('status', $event)">
        </frg-select-option>

        <frg-select-estimate [estimate]="project.estimatedTime"
           (newEstimate)="changeStrategy('estimatedTime', $event)">
        </frg-select-estimate>

      </md-card>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskDetailsComponent implements OnDestroy, OnInit {
  public project: ITask = {} as any;

  public statusOptions = [
    {display: 'Otwarty', value: 'OPEN'},
    {display: 'Oczekujący', value: 'PENDING'},
    {display: 'Zakończony', value: 'CLOSED'},
  ];

  private subs: Subscription[] = [];

  constructor(
    private tasksService: TasksService,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private store: Store<any>,
  ) {}

  public ngOnInit() {
    const pathParams = this.route.params.map((it) => it['tid']).distinctUntilChanged();
    const projectIdSub = this.route.params.map((it) => it['pid'])
    .distinctUntilChanged().subscribe((it) => {
      const url = PROJECT_RESOURCE + '/' + it + '/tasks';
      actions.setUrl(url).dispatch(this.store);
    });

    const projectSubscription = pathParams.switchMap((id) => this.tasksService.findTaskByNameCached(id))
      .subscribe((project) => {
        this.project = project;
        this.cdr.markForCheck();
      });
    this.subs.push(projectSubscription);
    this.subs.push(projectIdSub);
  }

  public ngOnDestroy() {
    this.subs.forEach((it) => it.unsubscribe());
  }

  public setProject(project: ITask) {
    this.project = project;
  }

  public changeStrategy(strategy: string, visibility: string) {
    this.tasksService.patchTask(this.project.id, strategy, visibility).subscribe((it) => {
      this.store.dispatch(MngTasksReducers.actions.select(it, it.id));
      this.project = it;
      this.cdr.markForCheck();
    });
  }
}
