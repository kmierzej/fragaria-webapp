import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'app/shared/material';

import {
    SelectOptionComponent,
    SelectTitleComponent,
    SelectEstimateComponent,
    EditAssigneesComponent,
 } from './components';
import { ProjectDetailsComponent } from './project-details.component';
import { TaskDetailsComponent } from './task-details.component';

import { QueryParamsService } from 'app/shared/managed-resource/services';

import { PagedResourceDialogsModule } from 'app/shared/paged-dialogs';

import { ProjectsCommonModule } from 'app/shared/projects-common';
import { SelectAssigneesModule } from 'app/shared/select-assignees';
import { EstimateValidatorModule } from 'app/shared/estimate-validator';

const SERVICES = [
    QueryParamsService,
];

@NgModule({
    declarations: [
        ProjectDetailsComponent,
        TaskDetailsComponent,

        SelectOptionComponent,
        SelectEstimateComponent,
        SelectTitleComponent,
        EditAssigneesComponent,
    ],
    exports: [
        ProjectDetailsComponent,
        TaskDetailsComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule,
        PagedResourceDialogsModule,
        FormsModule,
        ReactiveFormsModule,

        EstimateValidatorModule,
        ProjectsCommonModule,
        SelectAssigneesModule,
    ],
    providers: [ ... SERVICES ],
})
export class ProjectDetailsModule {}
