import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'frg-select-title',
    template: `
    <frg-expand-wrapper [buttonLabel]="'Tytuł:'" [label]="title"
                        [expand]="expand"
                        (expandEvent)="toggle()">
      <form #f="ngForm" (ngSubmit)="changeTitle(model.value)">
      Tytuł:
      <md-input-container>
        <input type="text" mdInput name="model" #model minlength="3">
        <md-error *ngIf="!model.valid">Nazwa jest za krótka - minimum 3 znaki </md-error>
      </md-input-container>
      <button md-button type="submit">
        <md-icon>done</md-icon>
      </button>
      <button md-button type="button" (click)="toggle()">
        <md-icon>clear</md-icon>
      </button>
      </form>
    </frg-expand-wrapper>
    `,
})
export class SelectTitleComponent {
  @Input()
  public title: string;

  @Output()
  public newTitle = new EventEmitter();

  public expand: boolean = false;

  public toggle() {
    this.expand = !this.expand;
  }

  public changeTitle(title: string) {
    this.newTitle.emit(title);
    this.expand = false;
  }
}
