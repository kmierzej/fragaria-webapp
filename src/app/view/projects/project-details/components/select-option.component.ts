import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'frg-select-option',
    template: `
    <div>
      <frg-expand-wrapper [buttonLabel]="label" [label]="defaultValue"
                          [expand]="expandVisibility "
                          (expandEvent)="toggleVisibility()">
        <form>
          <label> {{label}} </label>
          <md-select [(ngModel)]="selectedVisibility" name="choice">
            <md-option *ngFor="let optionA of options" [value]="optionA.value"> {{optionA.display}} </md-option>
          </md-select>

          <button md-button (click)="assertChoice()">
            <md-icon>done</md-icon>
          </button>
        </form>
      </frg-expand-wrapper>
    </div>
    `,
})
export class SelectOptionComponent {
  @Input()
  public label: string;

  @Input()
  public options: Array<{ value: string, display: string }>;

  @Input()
  set defaultOption(option: string) {
    this.expandVisibility = false;
    this.initialChoice = option;
    this.selectedVisibility = option;
    this.defaultValue = (this.options.find((it) => it.value === option) || {display: 'unknown'}).display;
  }

  @Output()
  public choice = new EventEmitter();

  public defaultValue: string;
  public expandVisibility: boolean;
  public selectedVisibility: string;

  private initialChoice: string;

  public assertChoice() {
    if (this.selectedVisibility !== this.initialChoice) {
      this.choice.emit(this.selectedVisibility);
    } else {
      this.toggleVisibility();
    }
  }

  public toggleVisibility() {
    this.expandVisibility = !this.expandVisibility;
  }
}
