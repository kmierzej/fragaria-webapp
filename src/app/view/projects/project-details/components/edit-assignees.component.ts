import { Component, OnInit, OnDestroy, Input, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { AssigneeController } from '../model';
import { IUser } from 'app/model/api';

import { AssigneesService } from 'app/model/api-services';

@Component({
    selector: 'frg-edit-assignees',
    template: `
    <div>
      Pracownicy:
      <frg-assignee-list [assignees]="controller.currentAssignees"
                         (delete)="controller.remove($event)"> </frg-assignee-list>
    </div>
    <div *ngIf="!controller.isRemoveEmpty()">
      <button md-button (click)="deleteUsers()"> Zatwierdź usunięcie pracownika: </button>
      <frg-assignee-list [assignees]="controller.assigneesToRemove"
                         (delete)="controller.add($event)"> </frg-assignee-list>
    </div>
    <div *ngIf="!controller.isAddEmpty()">
      <button md-button (click)="addUsers()"> Zatwierdź dodanie pracownika: </button>
      <frg-assignee-list [assignees]="controller.assigneesToAdd"
                         (delete)="controller.remove($event)"> </frg-assignee-list>
    </div>

    <frg-add-assignees
        [users]="controller.filteredQuerried"
        (aUsers)="addQuerried($event)"
        (qUsers)="filterQuerried($event)" >
    </frg-add-assignees>
    `,
})
export class EditAssigneesComponent implements OnInit, OnDestroy {
  @Input()
  public projectId: string = '';

  public controller: AssigneeController = new AssigneeController();

  private subs: Subscription[] = [];

  constructor(
    private assigneeService: AssigneesService,
    private cdr: ChangeDetectorRef,
  ) {}

  public ngOnInit() {
    const assigneeSubscription = this.assigneeService.getAssignees(this.projectId) .subscribe((assignees) => {
        const a = assignees.map((it) => ({display: it.login, value: it}));
        this.controller.reset(a);
        this.cdr.markForCheck();
      });
    this.subs.push(assigneeSubscription);
  }

  public ngOnDestroy() {
    this.subs.forEach((it) => it.unsubscribe());
  }

  public deleteUsers() {
    this.assigneeService.removeAssignees(this.projectId, this.controller.assigneesToRemove).subscribe((it) => {
      const a = it.map((b) => ({display: b.login, value: b}));
      this.controller.update(a);
      this.cdr.markForCheck();
    });
  }

  public addUsers() {
    this.assigneeService.addAssignees(this.projectId, this.controller.assigneesToAdd).subscribe((it) => {
      const a = it.map((b) => ({display: b.login, value: b}));
      this.controller.update(a);
      this.cdr.markForCheck();
    });
  }

  public addQuerried(users: Array<{display: string, value: IUser}>) {
    users.forEach((user) => {
      this.controller.add(user);
    });
  }

  public filterQuerried(users: Array<{display: string, value: IUser}>) {
      this.controller.setQuerried(users);
  }
}
