import { Component, Input, Output, EventEmitter } from '@angular/core';
import {
  minutesToEstimate, extractEstimate, estimateToMinutes, estimateToString,
} from 'app/shared/estimate-validator/utils';

@Component({
    selector: 'frg-select-estimate',
    template: `
    <frg-expand-wrapper [buttonLabel]="'Szacowany czas:'" [label]="estimateString"
                        [expand]="expand || !estimateString"
                        (expandEvent)="toggle()">
      <form #f="ngForm" (ngSubmit)="changeEstimate(model.value)">
      Szacowany czas:
      <md-input-container>
        <input type="text" mdInput name="model" ngModel #model frgEstimateValidator>
        <md-error *ngIf="!model.valid">{{errorMessage}}</md-error>
      </md-input-container>
      <button md-button type="submit">
        <md-icon>done</md-icon>
      </button>
      <button md-button type="button" (click)="toggle()">
        <md-icon>clear</md-icon>
      </button>
      </form>
    </frg-expand-wrapper>
    `,
})
export class SelectEstimateComponent {
  public estimateString: string;
  public errorMessage = 'Nieprawidłowy format. Przykładowa poprawna wartość to: 1MO 1W 3D 3H 10M';

  @Input()
  set estimate(estimate: number) {
    this.estimateString = estimateToString(minutesToEstimate(estimate));
  }

  @Output()
  public newEstimate: EventEmitter<number> = new EventEmitter();

  public expand: boolean = false;

  public toggle() {
    this.expand = !this.expand;
  }

  public changeEstimate(title: string) {
    const value = estimateToMinutes(extractEstimate(title));
    this.newEstimate.emit(value);
    this.expand = false;
  }
}
