interface IValue {display: string; value: any; }

export class AssigneeController {
  public currentAssignees: IValue[] = [];
  public assigneesToRemove: IValue[] = [];
  public assigneesToAdd: IValue[] = [];

  public filteredQuerried: IValue[] = [];
  private allQueried: IValue[] = [];

  public reset(assignees: IValue[]) {
      this.currentAssignees = assignees;
      this.assigneesToAdd = [];
      this.assigneesToRemove = [];
      this.refreshQuerried();
  }

  public add(assignee: IValue) {
      if (this.inToAdd(assignee.display) || this.inCurrent(assignee.display) ) {
          // DO NOTHING
      } else if (this.inToRemove(assignee.display)) {
         this.assigneesToRemove = this.assigneesToRemove.filter((it) => it.display !== assignee.display);
         this.currentAssignees = this.currentAssignees.concat(assignee);
      } else {
         this.assigneesToAdd = this.assigneesToAdd.concat(assignee);
      }
      this.refreshQuerried();
  }

  public remove(assignee: IValue) {
      if (this.inToAdd(assignee.display)) {
         this.assigneesToAdd = this.assigneesToAdd.filter((it) => it.display !== assignee.display);
      } else if (this.inCurrent(assignee.display)) {
         const toAdd = this.currentAssignees.find((it) => it.display === assignee.display);
         this.assigneesToRemove = this.assigneesToRemove.concat(toAdd);
         this.currentAssignees = this.currentAssignees.filter((it) => it.display !== assignee.display);
      }
      this.refreshQuerried();
  }

  public update(assignees: IValue[]) {
      this.assigneesToAdd = this.assigneesToAdd.filter((it) => !this.isPresent(assignees, it.display));
      this.assigneesToRemove = this.assigneesToRemove.filter((it) => this.isPresent(assignees, it.display));
      this.currentAssignees = assignees.filter((it) => !this.isPresent(this.assigneesToRemove, it.display));

      this.refreshQuerried();
  }

  public setQuerried(assignees: IValue[]) {
      this.allQueried = assignees;
      this.refreshQuerried();
  }

  public flushAdd() {
      this.currentAssignees = this.currentAssignees.concat(this.assigneesToAdd);
      this.assigneesToAdd = [];
  }

  public flushRemove() {
      this.assigneesToRemove = [];
  }

  public inCurrent(item: string) {
      return this.isPresent(this.currentAssignees, item);
  }

  public inToAdd(item: string) {
      return this.isPresent(this.assigneesToAdd, item);
  }

  public inToRemove(item: string) {
      return this.isPresent(this.assigneesToRemove, item);
  }

  public isRemoveEmpty(): boolean {
      return this.assigneesToRemove.length === 0;
  }

  public isAddEmpty(): boolean {
      return this.assigneesToAdd.length === 0;
  }

  private refreshQuerried() {
      this.filteredQuerried = this.allQueried.filter((filter) => !this.inCurrent(filter.display))
                                   .filter((filter) => !this.inToAdd(filter.display))
                                   .filter((filter) => !this.inToRemove(filter.display));
  }

  private isPresent(coll: IValue[], item: string) {
      return coll.findIndex((it) => it.display === item) >= 0;
  }
}
