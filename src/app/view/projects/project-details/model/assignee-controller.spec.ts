import { AssigneeController } from './assignee-controller';

const el = (a: string) => ({display: a, value: a});

describe(`AssigneeControler`, () => {
  it(`Adding single element`, () => {
      const control = new AssigneeController();
      control.add(el('a'));

      expect(control.assigneesToAdd).toEqual([el('a')]);
      expect(control.currentAssignees).toEqual([]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Adding existing element`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a'), el('b'), el('c')];

      control.add(el('a'));

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('a'), el('b'), el('c')]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Adding element twice`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a'), el('b'), el('c')];

      control.add(el('d'));
      control.add(el('d'));

      expect(control.assigneesToAdd).toEqual([el('d')]);
      expect(control.currentAssignees).toEqual([el('a'), el('b'), el('c')]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Remove single element`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a'), el('b'), el('c')];

      control.remove(el('b'));

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('a'), el('c')]);
      expect(control.assigneesToRemove).toEqual([el('b')]);
  });

  it(`Remove not existing element`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a'), el('b'), el('c')];

      control.remove(el('d'));

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('a'), el('b'), el('c')]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Remove element twice`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a'), el('b'), el('c')];

      control.remove(el('c'));
      control.remove(el('c'));

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('a'), el('b')]);
      expect(control.assigneesToRemove).toEqual([el('c')]);
  });

  it(`Remove added element`, () => {
      const control = new AssigneeController();

      control.add(el('c'));
      control.remove(el('c'));

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Add removed element`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a')];

      control.remove(el('a'));
      control.add(el('a'));

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('a')]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Reset`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a')];
      control.assigneesToAdd = [el('b')];
      control.assigneesToRemove = [el('c')];

      control.reset([el('d')]);

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('d')]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Update - add not fulfilled`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a')];
      control.assigneesToAdd = [el('b')];

      control.update([el('a')]);

      expect(control.currentAssignees).toEqual([el('a')]);
      expect(control.assigneesToAdd).toEqual([el('b')]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Update - delete not fulfilled`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [];
      control.assigneesToRemove = [el('b')];

      control.update([el('b')]);

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([]);
      expect(control.assigneesToRemove).toEqual([el('b')]);
  });

  it(`Update - add and delete not fulfilled`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a'), el('b'), el('c')];
      control.assigneesToAdd = [el('d')];
      control.assigneesToRemove = [el('e')];

      control.update([el('e'), el('f'), el('g'), el('e')]);

      expect(control.assigneesToAdd).toEqual([el('d')]);
      expect(control.currentAssignees).toEqual([el('f'), el('g')]);
      expect(control.assigneesToRemove).toEqual([el('e')]);
  });

  it(`Update - add and delete fulfilled`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a'), el('b'), el('c')];
      control.assigneesToAdd = [el('d')];
      control.assigneesToRemove = [el('e')];

      control.update([el('d')]);

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('d')]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Flush add`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a')];
      control.assigneesToAdd = [el('b'), el('c')];
      control.assigneesToRemove = [];

      control.flushAdd();

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('a'), el('b'), el('c')]);
      expect(control.assigneesToRemove).toEqual([]);
  });

  it(`Flush remove`, () => {
      const control = new AssigneeController();
      control.currentAssignees = [el('a')];
      control.assigneesToAdd = [];
      control.assigneesToRemove = [el('b'), el('c')];

      control.flushRemove();

      expect(control.assigneesToAdd).toEqual([]);
      expect(control.currentAssignees).toEqual([el('a')]);
      expect(control.assigneesToRemove).toEqual([]);
  });
});
