import {
  Component,
  ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';

import { IProject } from 'app/model/api';

import { ProjectsService, AssigneesService } from 'app/model/api-services';

import { MngProjectsReducers } from '../store';
import {
  allStatus, projectVisibilityOptions,
  projectCreateTaskOptions, projectLoggingWhoOptions,
} from 'app/model/api/project';

@Component ({
    selector: 'frg-project-details',
    template: `
      <md-card>
        <md-card-title>
          <frg-navigate-before [label]="project.title" [editVisible]="false">
          </frg-navigate-before>
        </md-card-title>

        <frg-select-title [title]="project.title" (newTitle)="changeStrategy('title', $event)">
        </frg-select-title>

        <frg-select-option [defaultOption]="project.status"
                           [options]="statusOptions"
                           [label]="'Status: '"
                           (choice)="changeStrategy('status', $event)">
        </frg-select-option>

        <frg-edit-assignees [projectId]="projectId | async">
        </frg-edit-assignees>

        <div>
          <frg-select-manager [manager]="project.manager"
            (selectManager)="selectManager($event)">
          </frg-select-manager>
        </div>

        <frg-select-option [defaultOption]="project.visibilityStrategy"
                           [options]="visibilityOptions"
                           [label]="'Widoczność: '"
                           (choice)="changeStrategy('visibilityStrategy', $event)">
        </frg-select-option>
        <frg-select-option [defaultOption]="project.createTaskStrategy"
                           [options]="createTaskOptions"
                           [label]="'Dodawanie zadań: '"
                           (choice)="changeStrategy('createTaskStrategy', $event)">
        </frg-select-option>
        <frg-select-option [defaultOption]="project.loggingWhoStrategy"
                           [options]="loggingWhoOptions"
                           [label]="'Logowanie czasu: '"
                           (choice)="changeStrategy('loggingWhoStrategy', $event)">
        </frg-select-option>
        <frg-select-estimate [estimate]="project.estimatedTime"
           (newEstimate)="changeStrategy('estimatedTime', $event)">
        </frg-select-estimate>
      </md-card>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectDetailsComponent implements OnDestroy, OnInit {
  public project: IProject = {} as any;
  public projectId: Observable<string>;

  public statusOptions = allStatus;
  public visibilityOptions = projectVisibilityOptions;
  public createTaskOptions = projectCreateTaskOptions;
  public loggingWhoOptions = projectLoggingWhoOptions;

  private subs: Subscription[] = [];

  constructor(
    private projectsService: ProjectsService,
    private assigneeService: AssigneesService,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private store: Store<any>,
  ) {}

  public ngOnInit() {
    this.projectId = this.route.params.map((it) => it.id).distinctUntilChanged();
    const projectObs = this.projectId.switchMap((id) => this.projectsService.findProjectByNameCached(id));

    const projectSubscription = projectObs.subscribe((project) => {
        this.project = project;
        this.cdr.markForCheck();
      });
    this.subs.push(projectSubscription);
  }

  public ngOnDestroy() {
    this.subs.forEach((it) => it.unsubscribe());
  }

  public setProject(project: IProject) {
    this.store.dispatch(MngProjectsReducers.actions.select(project, project.id));
    this.project = project;
    this.cdr.markForCheck();
  }

  public changeStrategy(strategy: string, visibility: string) {
    this.projectsService.patchProject(this.project.id, strategy, visibility).subscribe((it) => {
      this.setProject(it);
    });
  }

  public selectManager(manager: {display: string, value: any}) {
    this.projectId.first().subscribe((id) => {
      this.assigneeService.addManager(id, manager).subscribe((it) => {
        this.setProject(it);
      });
    });
  }
}
