import { MngResourceActions, MngResourceReducers } from 'app/shared/managed-resource/store';
import { IPagedResourceState } from 'app/shared/managed-resource/model';
import { ITask } from 'app/model/api';

export const actions = new MngResourceActions.EventFactory('[TASKS]');
export type IState = IPagedResourceState<ITask>;
type PermissionSet = MngResourceReducers.IPermissionSet;

const permissions: PermissionSet = {
    view: ['ADMIN', 'USER'],
    modify: ['ADMIN', 'USER'],
    remove: ['ADMIN', 'USER'],
};

export const selectors = MngResourceReducers.managedResourceSelectorsFactory<ITask>();
export const initialState = MngResourceReducers.initialStateFactory(null, 'id', permissions, 'resources');
export const reducer = MngResourceReducers.reducerFactory(
    '[TASKS]',
    MngResourceActions.baseTags,
    initialState,
);
