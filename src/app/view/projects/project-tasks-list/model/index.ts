export interface IButtonState {
    visible: boolean;
    disabled: boolean;
}
