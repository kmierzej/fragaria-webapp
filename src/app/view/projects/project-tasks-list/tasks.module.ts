import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/shared/material';

import { PagedHeaderModule } from 'app/shared/paged-header';
import { LogFormModule } from 'app/shared/log-form/log-form';

import {
    TaskListEntryComponent,
    TasksTableComponent,
    CreateTaskDialogComponent,
    LogTimeDialogComponent,
 } from './components';
import { TasksComponent } from './tasks.component';
import { ManageTasksService } from './services';

import { PagedResourceDialogsModule } from 'app/shared/paged-dialogs';
import { QueryParamsService } from 'app/shared/managed-resource/services';
import { ProjectsCommonModule } from 'app/shared/projects-common';

const DIALOGS = [ CreateTaskDialogComponent, LogTimeDialogComponent ];

@NgModule({
    declarations: [
        TasksComponent,
        TasksTableComponent,
        TaskListEntryComponent,
         ...DIALOGS,
    ],
    exports: [
        TasksComponent,
    ],
     entryComponents: [ ... DIALOGS ],
    imports: [
        CommonModule,
        MaterialModule,
        PagedHeaderModule,
        PagedResourceDialogsModule,
        ReactiveFormsModule,
        ProjectsCommonModule,
        LogFormModule,
    ],
    providers: [
        QueryParamsService,
        ManageTasksService,
    ],
})
export class TasksModule {}
