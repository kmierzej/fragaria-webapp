import {
  Component, ChangeDetectionStrategy,
  ChangeDetectorRef, OnDestroy, OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProject } from 'app/model/api';
import { Subscription } from 'rxjs/Subscription';

import { ProjectsService } from 'app/model/api-services';
import { IAdminState } from 'app/model';
import { minutesToEstimateString } from 'app/shared/estimate-validator/utils';
import { AuthStore } from 'app/auth/token-services/auth.store';
import { getLogin, getAdminState } from 'app/auth/token-services/auth.reducers';

@Component ({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-tasks-component',
    template: `
      <md-card>
        <md-card-title>
          <frg-navigate-before [label]="project.title" [editVisible]="editVisible()" [editEnabled]="editEnabled()">
          </frg-navigate-before>
        </md-card-title>
        <div *ngIf="project.totalLogged || project.estimatedTime">
        <div style="padding-bottom: 5px">
        Zalogowany czas: {{timeToString(project.totalLogged) || 0}}
        </div>
        <div style="padding-bottom: 10px">
        Oszacowany czas: {{timeToString(project.estimatedTime) || 0}}
        </div>
        </div>

        <frg-tasks-table-component> </frg-tasks-table-component>
      </md-card>
    `,
})
export class TasksComponent implements OnDestroy, OnInit {
  public subs: Subscription[] = [];

  public project: IProject = {} as any;
  public adminState: IAdminState = { isAdmin: false, canBeAdmin: false };
  public login: string = null;

  constructor(
    private projectsService: ProjectsService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthStore,
  ) { }

  public ngOnInit() {
    const projectId = this.route.params.map((it) => it.id).distinctUntilChanged();
    const projectSub = projectId.switchMap((id) => this.projectsService.findProjectByNameCached(id))
      .subscribe((project) => {
        this.project = project;
        this.cdr.markForCheck();
      });
    this.subs.push(projectSub);

    const adminSub = this.auth.observe(getAdminState).subscribe((it) => {
      this.adminState = it;
      this.cdr.markForCheck();
    });
    this.subs.push(adminSub);

    const loginSub = this.auth.observe(getLogin).subscribe((it) => {
      this.login = it;
      this.cdr.markForCheck();
    });
    this.subs.push(loginSub);
  }

  public ngOnDestroy() {
    this.subs.forEach((it) => it.unsubscribe());
  }

  public goBack() {
    this.router.navigate(['..'], {relativeTo: this.route});
  }

  public goToEdit() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  public editVisible(): boolean {
    return this.project.manager === this.login || this.adminState.canBeAdmin;
  }

  public editEnabled(): boolean {
    return this.project.manager === this.login || this.adminState.isAdmin;
  }

  public timeToString(value: number) {
    return minutesToEstimateString(value);
  }
}
