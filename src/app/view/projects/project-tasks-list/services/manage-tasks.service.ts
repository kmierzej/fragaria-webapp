import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpService, USER, ADMIN } from 'app/auth/token-services/http.service';
import { PROJECT_RESOURCE } from 'app/endpoints';
import { ITask } from 'app/model/api';
import { IProjectStatus } from 'app/model/api';

@Injectable()
export class ManageTasksService {
    constructor(private http: HttpService) { }

    public findTaskById(projectId: Observable<string>, name: string): Observable<ITask> {
        return projectId.first().flatMap((project) =>
          this.http.get(PROJECT_RESOURCE + '/' + project + '/' + name, [ADMIN, USER]).map((x) => x.json()),
        );
    }

    public canCreateTasks(projectId: Observable<string>): Observable<IProjectStatus> {
        return projectId.switchMap((id) => {
          return this.http.get(PROJECT_RESOURCE + '/' + id + '/canCreateTasks', [ADMIN, USER])
            .map((it) => it.json()).catch(() => Observable.of({
                visible: false, editable: false, loggable: false, manager: false,
            }));
        });
    }
}
