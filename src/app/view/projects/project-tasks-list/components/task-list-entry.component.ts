import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { IButtonState } from '../model';
import { ITask } from 'app/model/api';
import { minutesToEstimateString } from 'app/shared/estimate-validator/utils';

import { statusMap } from 'app/model/api';

@Component ({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: '[task-list-entry]', // tslint:disable-line
    template: `
      <td class="task-id">
        <ng-container *ngIf="canEdit || canAdmin; else noButton">
        <button md-button [disabled]="!canEdit" (click)="edit.emit(task.value.id)">
          {{task.value.id}}
        </button>
        </ng-container>
        <ng-template #noButton>
          {{task.value.id}}
        </ng-template>
      </td>
      <td>{{task.value.title}}</td>
      <td>{{getStatus(task.value)}}</td>
      <td> {{timeToString(task.value.totalLogged)}}</td>
      <td> {{timeToString(task.value.estimatedTime)}}</td>
      <td>
        <button *ngIf="canLog" md-button (click)="log.emit(task.value.id)">
          <md-icon>timer</md-icon>
        </button>
      </td>
    `,
})
export class TaskListEntryComponent {
    @Input()
    public task: {value: ITask, key: string};

    @Input()
    public canEdit: boolean;

    @Input()
    public canLog: boolean;

    @Input()
    public canAdmin: boolean;

    @Input()
    public editState: IButtonState;

    @Output()
    public edit = new EventEmitter();

    @Output()
    public delete = new EventEmitter();

    @Output()
    public log = new EventEmitter();

    public timeToString(value: number) {
      return minutesToEstimateString(value);
    }

    public getStatus(project: ITask) {
      return statusMap[project.status].display;
    }
}
