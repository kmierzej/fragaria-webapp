import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { IProject } from 'app/model/api';

@Component({
    selector: 'frg-create-task-dialog',
    template: `
    <h2 md-dialog-title style="text-align:center">Zaloguj czas</h2>
      <frg-log-form [wrap]="true"
                    [enabled]="logFormEnabled"
                    [defaults]="logFormDefault"
                    (newTimeEntry)="newTimeEntry($event)">
      </frg-log-form>
    `,
})
export class LogTimeDialogComponent implements OnInit {
    public logFormEnabled = {user: true, task: true};
    public logFormDefault = {user: '', task: ''};

    public user: string;
    public task: string;

    constructor(
      public dialogRef: MdDialogRef<IProject>,
    ) {}

    public ngOnInit() {
      this.logFormDefault = { user: this.user, task: this.task };
    }

    public newTimeEntry(entry: any) {
      return this.dialogRef.close(entry);
    }
}
