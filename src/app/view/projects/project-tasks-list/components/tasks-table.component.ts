import { Component, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';

import { AppReducers } from 'app/store';
import { actions, IState, initialState } from '../store/tasks.reducers';

import { IButtonState } from '../model';

import { PagedResourceDialogsService } from 'app/shared/paged-dialogs/services';
import { QueryParamsService } from 'app/shared/managed-resource/services';
import { PROJECT_RESOURCE } from 'app/endpoints';
import { ManageTasksService } from '../services';
import { CreateTaskDialogComponent } from './create-task-dialog.component';
import { LogTimeDialogComponent } from './log-time-dialog.component';

import { UserLogService } from 'app/shared/log-form/log-form/services';
import { pendingStatus, openStatus, allStatus} from 'app/model/api';
import { AuthStore } from 'app/auth/token-services/auth.store';
import { getLogin, getAdminState } from 'app/auth/token-services/auth.reducers';

@Component({
    selector: 'frg-tasks-table-component',
    template: `
      <table id="user-table">
        <tr>
          <th>
            <frg-paged-header id="users-control" class="nowrap"
              [state]="state.pageState"
              [title]="'kod'"
              (find)="openFilterDialog()"
              (next)="next()"
              (previous)="previous()"
            ></frg-paged-header>
          </th>
          <th>tytuł</th>
          <th>
            <button md-button (click)="selectStatus()"> status </button>
          </th>
          <th>Zalogowano</th>
          <th>Oszacowany czas</th>
          <th></th>
        </tr>
        <tr task-list-entry *ngFor="let task of state.resources"
          [task]="task"
          [editState]="editState"
          [canEdit]="canCreateTasks"
          [canLog]="canLogTime"
          [canAdmin]="canAdmin"
          (edit)="editTask($event)"
          (delete)="deleteTask($event)"
          (log)="logTime($event)">
        </tr>
      </table>
      <md-spinner *ngIf="state.pageState === 'INITIAL'"
                  class="centered-spinner"
                  mode="indeterminate"
                  color="accent"
                  id="users-init-spinner">
      </md-spinner>

      <div id="users-loading" *ngIf="state.pageState === 'LOADING_ALL'"> </div>

      <div *ngIf="canAdmin || canCreateTasks " width="100%" style="text-align: right;">
        <button md-fab id="create-project-button"
          type="button" color="accent"
          [disabled]="!canCreateTasks"
          (click)="openCreateTaskDialog()">
          <md-icon class="md-24">add</md-icon>
        </button>
      </div>
    `,
})
export class TasksTableComponent implements OnDestroy, OnInit {
  public editState: IButtonState = {visible: true, disabled: true};
  public state: IState = initialState;

  public createState = { canCreate: false, canAdmin: false};

  public canCreateTasks = false;
  public canLogTime = false;
  public canAdmin = false;
  public isAdmin = false;
  public manager = false;

  private subs: Subscription[] = [];
  private pageSelector = AppReducers.MngTasksSelectors.getPage;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: PagedResourceDialogsService,
    private manageTasksService: ManageTasksService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppReducers.IState>,
    private auth: AuthStore,
    private queryParams: QueryParamsService,
    private userLogService: UserLogService,
  ) {}

  public ngOnInit() {
    // Load state
    const stateSub = this.store.select(AppReducers.MngTasksSelectors.getState)
      .subscribe((it) => {
        this.state = it;
        this.cdr.markForCheck();
      });
    this.subs.push(stateSub);

    const projectId = this.route.params.map((it) => it.id);
    const projectIdSub = projectId.subscribe((id) => {
      const url = PROJECT_RESOURCE + '/' + id + '/tasks';
      actions.setUrl(url).dispatch(this.store);
    });
    this.subs.push(projectIdSub);

    // Adjust metadata on url change
    const urlChangeSub = this.queryParams.getSubscription(this.route.queryParams, [], ['status'])
        .subscribe((action) => { actions.setMeta(action).dispatch(this.store); });
    this.subs.push(urlChangeSub);

    const canCreate = this.manageTasksService.canCreateTasks(projectId).subscribe((it) => {
      this.canCreateTasks = it.editable;
      this.canLogTime = it.loggable;
      this.manager = it.manager;
      this.cdr.markForCheck();
    });
    this.subs.push(canCreate);

    const adminSub = this.auth.observe(getAdminState).subscribe((it) => {
      this.canAdmin = it.canBeAdmin;
      this.isAdmin = it.isAdmin;
      this.cdr.markForCheck();
    });
    this.subs.push(adminSub);
  }

  public openCreateTaskDialog() {
    this.dialogService.createDialog(CreateTaskDialogComponent).subscribe((it) => {
      actions.create(it, it.id).dispatch(this.store);
    });
  }

  public ngOnDestroy() {
    this.subs.forEach((it) => it.unsubscribe());
    this.dialogService.cleanup();
  }

  public next() {
    this.queryParams.next(this.store, this.pageSelector, this.router, actions);
  }

  public previous() {
    this.queryParams.previous(this.store, this.pageSelector, this.router, actions);
  }

  public editTask(taskId: string) {
    this.router.navigate([taskId], {relativeTo: this.route});
  }

  public selectStatus() {
    const filtered = this.state.filters['status'] || [openStatus.value, pendingStatus.value];
    const options = allStatus.map((status) => {
      const isFiltered = filtered.indexOf(status.value) > -1;
      return Object.assign({}, status, {checked: isFiltered});
    });

    this.dialogService.openSelectOptionDialog(options).subscribe((x) => {
      this.queryParams.setQueryParam(this.store, this.pageSelector,  this.router, actions, 'status', x);
    });
  }

  public openFilterDialog() {
    this.dialogService.openFilterDialog().subscribe((result) => {
        this.queryParams.from(this.store, this.pageSelector, this.router, actions, result);
    });
  }

  public deleteTask(id: string) {
    this.dialogService.openDeleteDialog(this.state.confirmDelete, id)
        .subscribe((x: any) => {
          this.store.dispatch(actions.setDeleteConfirmation(x.confirmDelete));
          this.store.dispatch(actions.delete(id));
        });
  }

  public logTime(taskId: string) {
    this.openLogDialog(taskId, this.auth.peek(getLogin));
  }

  public openLogDialog(taskId: string, login: string) {
    const initializer = (inst: LogTimeDialogComponent) => {
      inst.task = taskId;
      inst.logFormEnabled = {user: this.isAdmin || this.manager, task: true};
      inst.user = login;
    };
    this.dialogService.createDialog(LogTimeDialogComponent, initializer).subscribe((entry) => {
        this.userLogService.createUserLog(entry).subscribe(() => null);
        this.store.dispatch(actions.reload());
    });
  }
}
