import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MdDialogRef } from '@angular/material';

import { ManageTasksService } from '../services';
import { IProject } from 'app/model/api';

import { estimateValidatorFactory } from 'app/shared/estimate-validator';
import { estimateStringToMinutes } from 'app/shared/estimate-validator/utils';

import { allStatus, openStatus } from 'app/model/api';

@Component({
    selector: 'frg-create-task-dialog',
    template: `
    <h2 md-dialog-title style="text-align:center">Nowe zadanie</h2>
    <form role="form" [formGroup]="form" (ngSubmit)="createNewTask()">

      <div style="margin-bottom: 10px">
        <md-input-container>
          <input mdInput type="text" #title id="task-title" placeholder="Tytuł" formControlName="title">
        </md-input-container>
      </div>

      <div style="margin-bottom: 10px">
        <md-select placeholder="Status" #status id="project-status" formControlName="status">
          <md-option *ngFor="let state of allStatus" [value]="state.value">{{ state.display }}</md-option>
        </md-select>
      </div>

      <div style="margin-bottom: 10px">
        <md-input-container>
          <input mdInput type="text" #estimatedTime id="project-estimate"
            placeholder="Czas wykonania" formControlName="estimatedTime">
        <md-error *ngIf="!estimatedTime.valid">{{badEstimate}}</md-error>
        </md-input-container>
      </div>

      <div style="text-align: center; margin-top: 10px;">
        <button md-raised-button color="accent" type="submit"
          [disabled]="!form.valid">Utwórz nowe zadanie</button>
        <button md-raised-button color="warn" type="button"
          (click)="dialogRef.close()"> Anuluj </button>
      </div>
    </form>
    `,
})
export class CreateTaskDialogComponent implements OnInit {
    public form: FormGroup;

    public allStatus = allStatus;
    public defaultStatus = openStatus;

    public badEstimate = 'Nieprawidłowy format. Przykładowa poprawna wartość to: 1MO 1W 3D 3H 10M';

    constructor(
      public projectsService: ManageTasksService,
      public dialogRef: MdDialogRef<IProject>,
      private fb: FormBuilder,
    ) {}

    public ngOnInit() {
      this.form = this.createForm();
    }

    public createNewTask() {
      const raw = this.form.getRawValue();
      const result = Object.assign(raw, {
        estimatedTime: estimateStringToMinutes(raw.estimatedTime),
      });
      return this.dialogRef.close(result);
    }

    private createForm() {
      return this.fb.group({
          title: [null, Validators.required],
          status: [this.defaultStatus.value],
          estimatedTime: [null, estimateValidatorFactory() ],
        });
    }
}
