import { MngResourceActions, MngResourceReducers } from 'app/shared/managed-resource/store';
import { IPagedResourceState } from 'app/shared/managed-resource/model';
import { IProject } from 'app/model/api';

import { PROJECT_RESOURCE } from 'app/endpoints';

export const actions = new MngResourceActions.EventFactory('[PROJECTS]');
export type IState = IPagedResourceState<IProject>;
type PermissionSet = MngResourceReducers.IPermissionSet;

const permissions: PermissionSet = {
    view: ['ADMIN', 'USER'],
    modify: ['ADMIN'],
    remove: ['ADMIN'],
};

export const selectors = MngResourceReducers.managedResourceSelectorsFactory<IProject>();
export const initialState = MngResourceReducers.initialStateFactory(PROJECT_RESOURCE, 'id', permissions, 'projects');
export const reducer = MngResourceReducers.reducerFactory(
    '[PROJECTS]',
    MngResourceActions.baseTags,
    initialState,
);
