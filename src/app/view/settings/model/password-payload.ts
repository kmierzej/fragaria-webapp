export interface IPasswordPayload {
  oldPassword: string;
  newPassword: string;
}
