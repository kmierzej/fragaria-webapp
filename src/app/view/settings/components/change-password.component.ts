import { Component, ChangeDetectionStrategy, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { IPasswordPayload } from '../model';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-change-password',
    template: `
    <md-card>
      <md-card-title>Zmiana hasła</md-card-title>
      <form [formGroup]="changePassForm" (ngSubmit)="changePassword($event)">
        <div>
        <md-input-container>
          <input mdInput formControlName="oldPass" type="password" placeholder="Stare hasło">
            <md-hint *ngIf="checkValidator('oldPass')" [ngStyle]="{'color': 'red'}" align="start">
            {{emptyField}}
            </md-hint>
        </md-input-container>
        </div>
        <div class="container">
        <md-input-container>
          <input mdInput formControlName="newPass" type="password" placeholder="Nowe hasło" #password>
            <md-hint *ngIf="checkValidator('newPass')" [ngStyle]="{'color': 'red'}" align="start">
            {{emptyField}}
            </md-hint>
        </md-input-container>
          <frg-password-strength-bar class="flex-item" [passwordToCheck]="password.value">
          </frg-password-strength-bar>
        </div>
        <div>
        <md-input-container>
          <input mdInput formControlName="confirmPass" type="password" placeholder="Powtórz hasło">
            <md-hint *ngIf="checkValidator('confirmPass')"
              [ngStyle]="{'color': 'red'}" align="start">{{emptyField}}</md-hint>
            <md-hint *ngIf="matchingPasswordValidator()" [ngStyle]="{'color': 'red'}" align="start">
            Hasła są różne
            </md-hint>
        </md-input-container>
        </div>
        <button md-button [disabled]="!changePassForm.valid" type="submit">Zmień hasło</button>
      </form>
    </md-card>
    `,
    styles: [`
      .container{
          display: flex;
          align-items: center;
          justify-content: center;
      }
      .fixed{
          width: 200px;
      }
      .flex-item{
          flex-grow: 1;
      }
    `],
})
export class ChangePasswordComponent implements OnInit {
    @Output()
    public passwordChange = new EventEmitter<IPasswordPayload>();

    public changePassForm: FormGroup;

    public emptyField= 'To pole jest puste';

    constructor(
      private fb: FormBuilder,
    ) {}

    public ngOnInit() {
      this.changePassForm = this.createForm();
    }

    public matchingPasswordValidator(): boolean {
      return this.changePassForm.get('confirmPass').hasError('notEquivalent')
        && this.changePassForm.get('confirmPass').touched;
    }

    public checkValidator(field: string): boolean {
        return this.changePassForm.get(field).hasError('required')
            && this.changePassForm.get(field).touched;
    }

    public changePassword(event: Event) {
      event.preventDefault();
      const value = this.changePassForm.value;
      const payload = {
        newPassword: value['confirmPass'],
        oldPassword: value['oldPass'],
      };
      this.passwordChange.emit(payload);
    }

    private createForm() {
      return this.fb.group({
        confirmPass: ['', Validators.required],
        newPass: ['', Validators.required],
        oldPass: ['', Validators.required],
      }, {validator: this.matchingPasswords('newPass', 'confirmPass')});
    }

    private matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
      return (group: FormGroup) => {
        const passwordInput = group.controls[passwordKey];
        const passwordConfirmationInput = group.controls[passwordConfirmationKey];
        const equal = passwordInput.value === passwordConfirmationInput.value;

        if (!equal) {
          return passwordConfirmationInput.setErrors({notEquivalent: true});
        }

        const errors = passwordConfirmationInput.errors;
        if (errors !== null && errors['notEquivalent'] !== undefined) {
          passwordConfirmationInput.updateValueAndValidity();
        }
        return false;
      };
    }
}
