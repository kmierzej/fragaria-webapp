import {
  Component, ChangeDetectionStrategy, ChangeDetectorRef,
  ViewChild, OnDestroy, OnInit,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';
import { LoggerActions } from 'app/prompt/logger/store';
import { AppReducers } from 'app/store';
import { EditPropertyService, EditPropertyComponent } from 'app/shared/edit-property';
import { IUser } from 'app/model/api';

import { SettingsService } from './settings.service';

import { IPasswordPayload } from './model';
import { MngAccountActions } from './store';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-settings',
    template: `
    <md-card>
      <md-card-title>Edycja danych</md-card-title>
      <frg-edit-property [label]="'Imię i nazwisko'"
                     [property]="user.fullname"
                     (propertyChange)="setProperty('fullname', $event)"
                     #fullname>
      </frg-edit-property>
      <frg-edit-property [label]="'email'"
                     [property]="user.email"
                     (propertyChange)="setProperty('email', $event)"
                     #email>
      </frg-edit-property>
    </md-card>
    <frg-change-password (passwordChange)="changePassword($event)">
    </frg-change-password>
    <md-card>
      <button md-button (click)="openTokens()">
        Zarządzaj sesjami
      </button>
    </md-card>
    `,
})
export class SettingsComponent implements OnDestroy, OnInit {
    @ViewChild('fullname') public fullname: EditPropertyComponent;
    @ViewChild('email') public email: EditPropertyComponent;

    public user: IUser = {fullname: null, email: null};
    public loading: boolean = true;
    public userSource: Subscription;

    constructor(
      private accountResource: SettingsService,
      private propertyResource: EditPropertyService,
      private cdr: ChangeDetectorRef,
      private store: Store<AppReducers.IState>,
      private router: Router,
      private route: ActivatedRoute,
    ) {}

    public ngOnInit() {
      this.store.dispatch(new MngAccountActions.LoadSettingsAction());
      this.userSource = this.store.select(AppReducers.getAccountState).subscribe((user) => {
        this.user = user;
        this.loading = user.loading;
        this.cdr.markForCheck();
      });
    }

    public ngOnDestroy() {
      this.userSource.unsubscribe();
    }

    public setProperty(label: string, property: string) {
      this.modifyUser(label, property)
        .finally(() => ((this as any)[label] as EditPropertyComponent).enable())
        .subscribe((user) => {
          this.store.dispatch(new MngAccountActions.SetSettingsAction(user));
        });
    }

    public modifyUser(key: string, value: any): Observable<IUser> {
      return this.propertyResource.editAccountProperty(key, value)
        .catch((error) => {
          const message = 'Nie udało się zmodifikować danych';
          this.store.dispatch(new LoggerActions.LogMessageAction(message));
          return Observable.throw(error);
        });
    }

    public changePassword(payload: IPasswordPayload) {
      this.accountResource.changePassword(payload)
        .catch((it) => {
          const message = it.json()['description'];
          this.store.dispatch(new LoggerActions.LogMessageAction(message));
          return Observable.throw(it);
        })
        .subscribe(() => {
          const message = 'Hasło zostało zmienione. Wyloguj się i zaloguj ponownie.';
          this.store.dispatch(new LoggerActions.LogNeutralMessageAction(message));
        });
    }

    public openTokens() {
      this.router.navigate(['../tokens'], {relativeTo: this.route});
    }
}
