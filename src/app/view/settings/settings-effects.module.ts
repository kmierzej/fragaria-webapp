import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';

import { SettingsService } from './settings.service';
import { ManageAccountEffects } from './store';

@NgModule({
    imports: [
        EffectsModule.run(ManageAccountEffects),
    ] ,
    providers: [
        SettingsService,
    ],
})
export class SettingsEffectsModule { }
