import { Component, OnChanges, Input, SimpleChange, ChangeDetectionStrategy } from '@angular/core';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-password-strength-bar',
    styles: [`
    ul#strengthBar {
        display:inline;
        list-style:none;
        margin:0;
        margin-left:15px;
        padding:0;
        vertical-align:2px;
    }
    .point:last {
        margin:0 !important;
    }
    .point {
        background:#DDD;
        border-radius:2px;
        display:inline-block;
        height:5px;
        margin-right:1px;
        width:20px;
    }`],
    template: `
    <div id="strength" >
        <ul id="strengthBar" #strength>
            <li class="point" [style.background-color]="bar0"></li>
            <li class="point" [style.background-color]="bar1"></li>
            <li class="point" [style.background-color]="bar2"></li>
            <li class="point" [style.background-color]="bar3"></li>
            <li class="point" [style.background-color]="bar4"></li>
        </ul>
    </div>
    `,
})
export class PasswordStrengthBarComponent implements OnChanges {
    private static measureStrength(p: string) {
        let force = 0;
        const regex = /[$-/:-?{-~!"^_`\[\]]/g; // "

        const lowerLetters = /[a-z]+/.test(p);
        const upperLetters = /[A-Z]+/.test(p);
        const numbers = /[0-9]+/.test(p);
        const symbols = regex.test(p);

        const flags = [lowerLetters, upperLetters, numbers, symbols];

        let passedMatches = 0;
        for (const flag of flags) {
            passedMatches += flag === true ? 1 : 0;
        }

        force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
        force += passedMatches * 10;

        // penality (short password)
        force = (p.length <= 6) ? Math.min(force, 10) : force;

        // penality (poor variety of characters)
        force = (passedMatches === 1) ? Math.min(force, 10) : force;
        force = (passedMatches === 2) ? Math.min(force, 20) : force;
        force = (passedMatches === 3) ? Math.min(force, 40) : force;

        return force;
    }

    @Input() public passwordToCheck: string;

    public bar0: string;
    public bar1: string;
    public bar2: string;
    public bar3: string;
    public bar4: string;

    private colors = ['#F00', '#F90', '#FF0', '#9F0', '#0F0'];

    public ngOnChanges(changes: {[propName: string]: SimpleChange}): void {
        const password = changes['passwordToCheck'].currentValue;
        this.setBarColors(5, '#DDD');
        if (password) {
            const c = this.getStrengthIndexAndColor(password);
            this.setBarColors(c.idx, c.col);
        }
    }

    private getColor(s: number) {
        let idx = 0;
        if (s <= 10) {
            idx = 0;
        } else if (s <= 20) {
            idx = 1;
        } else if (s <= 30) {
            idx = 2;
        } else if (s <= 40) {
            idx = 3;
        } else {
            idx = 4;
        }
        return {
            col: this.colors[idx],
            idx: idx + 1,
        };
    }

    private getStrengthIndexAndColor(password: string) {
        return this.getColor(PasswordStrengthBarComponent.measureStrength(password));
    }

    private setBarColors(count: number, col: string) {
        for (let n = 0; n < count; n++) {
            const index: string = 'bar' + n;
            (this as any)[index] = col;
        }
    }
}
