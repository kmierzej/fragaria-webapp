import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PasswordStrengthBarComponent } from './password-strength-bar.component';

// https://github.com/rnadler/ng2-password-strength-bar
// 1.0.1
@NgModule({
    declarations: [ PasswordStrengthBarComponent ],
    exports: [ PasswordStrengthBarComponent ],
    imports: [ CommonModule ],
})
export class PasswordStrengthBarModule {}
