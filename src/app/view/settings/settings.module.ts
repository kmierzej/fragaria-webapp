import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MaterialModule } from 'app/shared/material';

import { RouterModule } from '@angular/router';

import { PasswordStrengthBarModule } from './passbar';
// import { PasswordStrengthBar } from 'ng2-password-strength-bar';

import { EditPropertyModule } from 'app/shared/edit-property';

import { ChangePasswordComponent } from './components';
import { SettingsComponent } from './settings.component';
import { SettingsService } from './settings.service';

@NgModule({
    declarations: [
        ChangePasswordComponent,
        SettingsComponent,
    ],
    exports: [
        SettingsComponent,
    ],
    imports: [
        CommonModule,
        EditPropertyModule,
        HttpModule,
        MaterialModule,
        PasswordStrengthBarModule,
        ReactiveFormsModule,
        RouterModule.forChild([{ path: '', component: SettingsComponent }]),
    ],
    providers: [
        SettingsService,
    ],
})
export class SettingsModule {}
