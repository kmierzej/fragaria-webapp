import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpService } from 'app/auth/token-services/http.service';
import { IPasswordPayload } from './model';

import { ACCOUNT_RESOURCE } from 'app/endpoints';
import { IUser } from 'app/model/api';

@Injectable()
export class SettingsService {
    constructor(private http: HttpService) { }

    public getAccount(): Observable<IUser> {
        return this.http.get(ACCOUNT_RESOURCE)
            .map((it) => it.json());
    }

    public changePassword(password: IPasswordPayload): Observable<IUser> {
        return this.http.put(ACCOUNT_RESOURCE + '/password', password)
            .map((it) => it.json());
    }
}
