import { IUser } from 'app/model/api';
import * as manageAccount from './manage-account.actions';

export interface IState extends IUser {
    loading: boolean;
}

const initialState: IState = {
    loading: true,
};

export function reducer(state = initialState, action: manageAccount.Actions): IState {
    switch (action.type) {
        case manageAccount.ActionTypes.LOAD_SETTINGS: {
            return {
                ... state,
                loading: true,
            };
        } case manageAccount.ActionTypes.SET_SETTINGS: {
            // Maybe PATCH_SETTINGS would be usefull.
            const payload = (action as manageAccount.SetSettingsAction).payload;
            return {
                ... payload,
                loading: false,
            };
        } default: {
            return state;
        }
    }
}
