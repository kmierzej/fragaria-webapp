import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { LoggerActions } from 'app/prompt/logger/store';
import { MngAccountActions } from './';

import { SettingsService } from '../settings.service';

@Injectable()
export class ManageAccountEffects {

    @Effect()
    public loadSettings: Observable<Action> = this.actions
        .ofType( MngAccountActions.ActionTypes.LOAD_SETTINGS)
        .switchMap(() => this.settingsService.getAccount()
            .map( (user) => new MngAccountActions.SetSettingsAction(user))
            .catch( () => Observable.of(
               new LoggerActions.LogMessageAction('Nie udało się wczytać danych. Spróbuj ponownie.'),
            )),
        );

    constructor(
        private actions: Actions,
        private settingsService: SettingsService,
    ) { }
}
