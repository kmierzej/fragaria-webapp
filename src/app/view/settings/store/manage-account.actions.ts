/* tslint:disable:max-classes-per-file */
import { Action } from '@ngrx/store';
import { type } from 'app/util';
import { IUser } from 'app/model/api';

export const ActionTypes = {
    LOAD_SETTINGS: type('[ManageAccount] Load Settings'),
    SET_SETTINGS: type('[ManageAccount] Set Settings'),
};

export class LoadSettingsAction implements Action {
    public type = ActionTypes.LOAD_SETTINGS;
}

export class SetSettingsAction implements Action {
    public type = ActionTypes.SET_SETTINGS;

    constructor(public payload: IUser) {}
}

export type Actions =
  | LoadSettingsAction
  | SetSettingsAction
  ;
