import { Component, OnInit } from '@angular/core';
import { AuthStore } from 'app/auth/token-services/auth.store';
import { isLoggedAs } from 'app/auth/token-services/auth.reducers';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'frg-home',
    template: `
    <md-card>
      <md-card-title>
        Witaj we Fragarii
      </md-card-title>
      <md-card-content>
        <div *ngIf="isLogged | async">
        Jesteś zalogowany! Kliknij na ikonę w prawym górnym rogu by otworzyć menu.
        <hr/>
        </div>
        <p> Fragaria to aplikacja służąca do rejestrowania czasu pracy. </p>
        <p> Logowanie czasu jest możliwe po zalogowaniu się na konto. </p>
        <p> Jeśli nie posiadasz konta, lub jeśli twoje konto jest nieaktywne, skontaktuj się z administratorem. </p>

        <div style="margin:30px">
        </div>

        <hr/>
        <p> Autorem ikony jest Madebyoliver. Ikona pobrana ze strony: <a href="www.flaticon.com">flaticon.com</a></p>
        <p> Repozytorium aplikacji, wraz ze stroną do zgłaszania błędów znajduje się na stronie:
          <a href="https://bitbucket.org/kmierzej/fragaria-webapp">bitbucket.org</a>
        </p>
      </md-card-content>
    </md-card>
    `,
})
export class HomeComponent implements OnInit {
  public isLogged: Observable<boolean>;

  constructor(private auth: AuthStore) {}

  public ngOnInit() {
    this.isLogged = this.auth.observe(isLoggedAs('USER'));
  }
}
