import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home.component';

import { MaterialModule } from 'app/shared/material';

@NgModule({
    declarations: [
        HomeComponent,
    ],
    exports: [
        HomeComponent,
    ],
    imports: [
        MaterialModule,
        CommonModule,
    ],
})
export class HomeModule {}
