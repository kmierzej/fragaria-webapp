import { Component, ChangeDetectorRef } from '@angular/core';
import { getErrorMessage2 } from 'app/shared/paged-dialogs/utils';
import { ReportService } from './report.service';
const FileSaver = require('file-saver') // tslint:disable-line

@Component({
    selector: 'frg-report',
    template: `
    <md-card>
      <form role="form" #form="ngForm" (ngSubmit)="getReport()">
        <div>
          <md-input-container>
            <input mdInput #fromDate="ngModel" name="fromDate" [(ngModel)]="from"
             placeholder="Od" pattern="\\d{4}-\\d{2}-\\d{2}" required>
            <md-error> {{getError(fromDate.errors)}} </md-error>
          </md-input-container>
          <md-input-container>
            <input mdInput #toDate="ngModel" name="toDate" [(ngModel)]="to"
             placeholder="Do" pattern="\\d{4}-\\d{2}-\\d{2}" required>
            <md-error> {{getError(toDate.errors)}} </md-error>
          </md-input-container>
        </div>
        <button md-button [disabled]="progress || !form.valid" type="submit">Wygeneruj raport</button>
      </form>
    </md-card>
    `,
    providers: [ ReportService ],
})
export class ReportComponent {
    public progress = false;

    public from: string;
    public to: string;

    constructor(
        private reportService: ReportService,
        private cdr: ChangeDetectorRef,
    ) {}

    public getError(errors: any) {
      const error = {
        required: 'Data nie może być pusta',
        pattern: 'YYYY-MM-DD',
      };
      return getErrorMessage2(errors, error);
    }

    public getReport() {
        this.progress = true;
        this.reportService.getReport(this.from, this.to).subscribe((it) => {
            FileSaver.saveAs(it, 'report.csv');
            this.progress = false;
            this.cdr.markForCheck();
        }, () => {
            this.progress = false;
            this.cdr.markForCheck();
        });
    }
}
