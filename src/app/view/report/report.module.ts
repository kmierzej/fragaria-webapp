import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ReportComponent } from './report.component';

import { LogFormModule } from 'app/shared/log-form/log-form';
import { LogFormControllerModule } from 'app/shared/log-form/log-form-controller';
import { TimeValidatorModule } from 'app/shared/time-validator';

import { MaterialModule } from 'app/shared/material';

@NgModule({
    declarations: [
        ReportComponent,
    ],
    exports: [
        ReportComponent,
    ],
    imports: [
        MaterialModule,
        CommonModule,
        FormsModule,
        LogFormModule,
        LogFormControllerModule,
        TimeValidatorModule,
        RouterModule.forChild([{ path: '', component: ReportComponent }]),
    ],
})
export class ReportModule {}
