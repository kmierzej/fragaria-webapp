import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { HttpService, ADMIN } from 'app/auth/token-services/http.service';
import { GLOBAL_LOG_RESOURCE } from 'app/endpoints';

@Injectable()
export class ReportService {
    constructor(private http: HttpService) {}

    public getReport(from: string, to: string) {
        const params = new URLSearchParams();
        params.append('from', from);
        params.append('to', to);

        return this.http.getBlob(GLOBAL_LOG_RESOURCE, params, [ADMIN]).map((it) => {
            return new Blob([it.blob()], {type: 'text/plain' });
        });
    }
}
