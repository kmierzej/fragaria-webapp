import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardComponent } from './dashboard.component';
import { DashboardWrapperComponent } from './dashboard-wrapper.component';
import {
    ProjectWrapperComponent,
    TimePipe,
    FocusedInputDirective,
} from './components';

import { LogFormModule } from 'app/shared/log-form/log-form';
import { LogFormControllerModule } from 'app/shared/log-form/log-form-controller';
import { TimeValidatorModule } from 'app/shared/time-validator';

import { MaterialModule } from 'app/shared/material';

@NgModule({
    declarations: [
        DashboardComponent,
        DashboardWrapperComponent,
        ProjectWrapperComponent,
        FocusedInputDirective,

        TimePipe,
    ],
    exports: [
        DashboardWrapperComponent,
        DashboardComponent,
    ],
    imports: [
        MaterialModule,
        CommonModule,
        FormsModule,
        LogFormModule,
        LogFormControllerModule,
        TimeValidatorModule,
    ],
})
export class DashboardModule {}
