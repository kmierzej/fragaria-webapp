import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { AuthStore } from 'app/auth/token-services/auth.store';
import { getLogin } from 'app/auth/token-services/auth.reducers';

@Component({
    selector: 'frg-dashboard-wrapper',
    template: `
    <md-card>
      <frg-dashboard [loginObs]="loginObs">
      </frg-dashboard>
    </md-card>
    `,
})
export class DashboardWrapperComponent implements OnInit {
    public loginObs: Observable<string>;

    constructor(
        private auth: AuthStore,
    ) {}

    public ngOnInit() {
      this.loginObs = this.auth.observe(getLogin);
    }
}
