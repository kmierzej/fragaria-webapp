import { Component, ChangeDetectorRef, OnDestroy, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { UserLogService, ILogResource } from 'app/shared/log-form/log-form/services';
import { LogFormControllerService } from 'app/shared/log-form/log-form-controller';
import { ITimeEntry } from 'app/model/api';
import { addDays, getMonday} from 'app/utils/time';

import { AuthStore } from 'app/auth/token-services/auth.store';
import { isLoggedAs } from 'app/auth/token-services/auth.reducers';
import { LoggerActions } from 'app/prompt/logger/store';

@Component({
    selector: 'frg-dashboard',
    template: `
    <button md-button (click)="previousWeek()">
      <md-icon>navigate_before</md-icon> Poprzedni
    </button>
    <button md-button (click)="reloadData()">
      Odśwież <md-icon>refresh</md-icon>
    </button>
    <button md-button (click)="nextWeek()">
       Następny <md-icon>navigate_next</md-icon>
    </button>

    <frg-project-wrapper [logs]="logs"
                         (create)="newTimeEntry($event)"
                         (delete)="deleteTimeEntry($event)">
    </frg-project-wrapper>

    <div style="padding-top: 5px; margin-top: 10px;">
      <frg-log-form [progress]="createTimeEntryProgress"
                    [enabled]="logFormEnabled | async"
                    [defaults]="logFormDefault"
                    (newTimeEntry)="newTimeEntry($event)">
      </frg-log-form>
    </div>
    `,
})
export class DashboardComponent implements OnDestroy, OnInit {
    @Input()
    public loginObs: Observable<string>;

    public loginValue: string;
    public logs: ILogResource= {range: [], values: [], summary: []};
    public startDate = getMonday(new Date());
    public endDate = addDays(this.startDate, 7);

    public subscriptions: Subscription[] = [];

    public createTimeEntryProgress: boolean = false;

    public logFormEnabled: Observable<{user: boolean}>;
    public logFormDefault = {user: ''};

    constructor(
        private auth: AuthStore,
        private store: Store<any>,
        private userLogService: UserLogService,
        private selectLogService: LogFormControllerService,
        private cdr: ChangeDetectorRef,
    ) {}

    public nextWeek() {
        this.startDate = addDays(this.startDate, 7);
        this.endDate = addDays(this.startDate, 7);
        this.reloadData();
        this.selectLogService.cleanDate();
    }

    public previousWeek() {
        this.startDate = addDays(this.startDate, -7);
        this.endDate = addDays(this.startDate, -7);
        this.reloadData() ;
        this.selectLogService.cleanDate();
    }

    public reloadData() {
      this.userLogService.getUserLogs(this.loginValue, this.startDate, addDays(this.startDate, 7))
       .subscribe((logs) => {
           this.logs = logs;
           this.cdr.markForCheck();
       });
    }

    public ngOnInit() {
      const loginCheck = this.loginObs
        .filter((it) => it !== undefined)
        .subscribe((login) => {
          this.loginValue = login;
          this.logFormDefault = {user: login};
          this.reloadData();
          this.cdr.markForCheck();
        });
      this.subscriptions.push(loginCheck);
      this.logFormEnabled = this.auth.observe(isLoggedAs('ADMIN')).map((user) => ({user}));
    }

    public ngOnDestroy() {
        this.subscriptions.forEach((it) => it.unsubscribe());
        this.subscriptions = [];
    }

    public newTimeEntry(entry: ITimeEntry) {
        this.createTimeEntryProgress = true;
        const successFn = () => {
            this.createTimeEntryProgress = false;
            this.reloadData();
            this.cdr.markForCheck();
        };

        this.userLogService.createUserLog(entry).subscribe(successFn);
    }

    public deleteTimeEntry(entry: ITimeEntry) {
        this.createTimeEntryProgress = true;
        const resumeFn = () => {
            this.reloadData();
            this.createTimeEntryProgress = false;
            this.cdr.markForCheck();
        };
        this.userLogService.deleteUserLog(entry).catch((error: Response) => {
            const description = (error.json() as any).description as string;
            const message = 'Usunięcie logu zakończone niepowodzeniem - ';
            const action = new LoggerActions.LogMessageAction(message + description);
            this.store.dispatch(action);

            return Observable.of(description);
        }).subscribe(resumeFn);
    }
}
