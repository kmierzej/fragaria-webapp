import { Component, Input, Output, EventEmitter } from '@angular/core';

import { ILogResource, ICompositeKey } from 'app/shared/log-form/log-form/services';
import { extractTimeFromString, getTimeString } from 'app/utils/time';

import { groupByArray } from 'app/utils/collections';

import { LogFormControllerService } from 'app/shared/log-form/log-form-controller';

interface IEntry {key: ICompositeKey; values: number[]; }
interface IByProject {key: string; values: IEntry[]; summary: number[]; }

@Component({
    selector: 'frg-project-wrapper',
    template: `
      <table>
      <tr>
        <th> </th>
        <th *ngFor="let dateObj of rangeDate" class="nowrap">
          <button md-button (click)="emitDate(dateObj)">
            {{dateObj | date}}
          </button>
        </th>
      </tr>
      <ng-container *ngFor="let project of byProject">
          <tr class="bottom-border">
            <td> {{project.key}}</td>
            <td *ngFor="let time of project.summary">
              {{time | time}}
            </td>
          </tr>
        <ng-container *ngFor="let task of project.values">
          <tr>
            <td>
              <button md-button (click)="emitTask(task.key.task)">
                {{task.key.task}}
              </button>
            </td>
            <td *ngFor="let time of task.values; let i = index;"
                [style.width]="rowWidth"
                [ngSwitch]="contentSwitch(task, i)">
              <button md-button *ngSwitchCase="'hide'" (click)="addEntry(task.key, i)">
                <md-icon>add</md-icon>
              </button>
              <ng-container *ngSwitchCase="'show'">
                {{time | time}}
                <button md-icon-button (click)="deleteEntry(task.key, i)">
                 <md-icon>delete</md-icon>
                </button>
              </ng-container>
              <form #form="ngForm" (ngSubmit)="postEntry(task.key, i, timeInput.value)"
                    *ngSwitchCase="'input'" class="nowrap">
                <md-input-container style="width: 50%;">
                  <input mdInput frgFocusedInput name="timeInput" #timeInput ngModel required frgTimeValidator >
                </md-input-container>
                <button md-icon-button type="submit" [disabled]="!form.valid" >
                 <md-icon>add_circle</md-icon>
                </button>
              </form>
            </td>
          </tr>
        </ng-container>
      </ng-container>
      <tr class="top-border">
        <td> </td>
        <td *ngFor="let date of summary">
            {{date | time}}
        </td>
      </tr>
      </table>
    `,
})
export class ProjectWrapperComponent {
    public rowWidth = '100%';
    public byProject: IByProject[];

    public rangeDate: Date[] = [];
    public summary: number[]= [];

    public previousTasks: any[] = [];

    public currentInput: {key: ICompositeKey, index: number} = null;

    @Output()
    public create: EventEmitter<any> = new EventEmitter();

    @Output()
    public delete: EventEmitter<any> = new EventEmitter();

    constructor(private selectLogService: LogFormControllerService) { }

    @Input()
    set logs(log: ILogResource) {
        this.currentInput = null;
        this.summary = log.summary;
        this.rangeDate = log.range;
        this.rowWidth =  (100 / log.range.length) + '%';

        const receivedTasks = log.values.map((task) => ({
          key: task.key,
          values: Array(task.values.length).fill(0),
        }));

        this.previousTasks.forEach((task) => {
            const findTask = log.values.find((it) =>
              it.key.project === task.key.project
              && it.key.task === task.key.task
              && it.key.user === task.key.user,
            );
            if (!findTask) {
              log.values.push(task);
            }
        });
        log.values.sort((a, b) => a.key.task.localeCompare(b.key.task));

        if (receivedTasks.length > 0) {
          this.previousTasks = receivedTasks;
        }

        // lets group by Project
        const byProject = groupByArray(log.values, (entry) => entry.key.project);

        // and let's count summary!
        this.byProject = byProject.map((project) => {
          const summary: number[] = Array(7).fill(0);
          project.values.forEach((task) => {
            task.values.forEach((entry, index) => {
              summary[index] += entry;
            });
          });
          return {...project, summary };
        });
   }

   public emitDate(date: Date) {
     if (date) { this.selectLogService.selectDate(date); }
   }

   public emitTask(task: string) {
     if (task) { this.selectLogService.selectTask(task); }
   }

   public contentSwitch(entry: IEntry, index: number): string {
     if (this.isCurrentInput(entry.key, index)) { return 'input'; }
     return (entry.values[index]) ? 'show' : 'hide';
   }

   public addEntry(key: ICompositeKey, index: number) {
     this.currentInput = {key, index};
   }

   public postEntry(key: ICompositeKey, index: number, value: string) {
     this.currentInput = null;
     const result = this.findByKey(key);
     if (result == null) { return; }

     const newValue = extractTimeFromString(value);
     result.project.summary[index] += newValue;
     result.task.values[index] += newValue;
     this.summary[index] += newValue;

     this.create.emit({...key, date: getTimeString(this.rangeDate[index]), time: newValue});
   }

   public deleteEntry(key: ICompositeKey, index: number) {
     this.delete.emit({...key, date: getTimeString(this.rangeDate[index])});
     const result = this.findByKey(key);
     if (result == null) { return; }

     const previous = result.task.values[index];
     this.summary[index] -= previous;
     result.project.summary[index] -= previous;
     result.task.values[index] = 0;
   }

   private findByKey(key: ICompositeKey): {task: IEntry, project: IByProject} | null {
     const project = this.byProject.find((it) => it.key === key.project);
     const tasks = (project) ? project.values : [];
     const task = tasks.find((it) => it.key.task === key.task && it.key.user === key.user);
     if (task == null) { return; }
     return { task, project };
   }

   private isCurrentInput(key: ICompositeKey, index: number) {
     return this.currentInput
       && this.currentInput.key.task === key.task
       && this.currentInput.key.user === key.user
       && this.currentInput.index === index;
   }
}
