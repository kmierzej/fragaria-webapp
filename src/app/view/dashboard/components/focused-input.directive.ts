import { Directive, Renderer, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector : '[frgFocusedInput]',
})
export class FocusedInputDirective implements OnInit {
  constructor(public renderer: Renderer, public elementRef: ElementRef) {}

  public ngOnInit() {
    this.renderer.invokeElementMethod(
      this.elementRef.nativeElement, 'focus', []);
  }
}
