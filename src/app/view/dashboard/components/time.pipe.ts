import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'time' })
export class TimePipe implements PipeTransform {
    public transform(value: number): string {
        if (value == null || value <= 0) { return null; }
        const hours = Math.trunc(value / 60);
        const minutes = value % 60;

        if (hours > 0) {
           return hours + 'h ' + minutes + 'm';
        }
        return minutes + 'm';
    }
}
