import { Injectable } from '@angular/core';
import { HttpService, ADMIN, USER } from 'app/auth/token-services/http.service';
import { Observable } from 'rxjs/Observable';
import { TOKENS_RESOURCE, TOKEN_RESOURCE } from 'app/endpoints';
import { IToken } from './token';

@Injectable()
export class TokensService {
    constructor(private http: HttpService) {}

    public getTokens(): Observable<IToken[]> {
        return this.http.get(TOKENS_RESOURCE, [ADMIN, USER])
          .map((it) => it.json())
          .map((it) => it as IToken[]);
    }

    public invalidateToken(token: string): Observable<IToken[]> {
        return this.http.delete(TOKEN_RESOURCE(token), [ADMIN, USER])
          .map((it) => it.json())
          .map((it) => it as IToken[]);
    }
}
