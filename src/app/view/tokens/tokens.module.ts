import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TokensComponent } from './tokens.component';

import { MaterialModule } from 'app/shared/material';
import { NavigateBeforeButtonModule } from 'app/shared/navigateBefore/navigate-before-button.module';

@NgModule({
    declarations: [
        TokensComponent,
    ],
    exports: [
        TokensComponent,
    ],
    imports: [
        NavigateBeforeButtonModule,
        MaterialModule,
        CommonModule,
        RouterModule.forChild([{ path: '', component: TokensComponent }]),
    ],
})
export class TokensModule {}
