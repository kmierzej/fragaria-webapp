import { Component, ChangeDetectorRef } from '@angular/core';
import { TokensService } from './tokens.service';
import { IToken } from './token';

@Component({
    selector: 'frg-tokens',
    template: `
    <md-card>
    <md-card-title>
      <frg-navigate-before-button [back]="'../settings'"></frg-navigate-before-button>
      Rozpoczęte sesje:
    </md-card-title>

    <md-list *ngFor="let token of tokens">
      <md-list-item>
        <md-icon md-list-icon style="float:left">{{getIcon(token)}}</md-icon>
        <h4 md-line>{{token.userAgent.name}} {{token.userAgent.version}}</h4>
        <p md-line>{{token.userAgent.os}}, {{token.source}}</p>
        <p md-line>Ostatni dostęp: {{token.lastAccess | date:'medium'}}</p>
        <button md-icon-button (click)="invalidateToken(token)">
          <md-icon>delete</md-icon>
        </button>
      </md-list-item>
    </md-list>
    </md-card>
    `,
    providers: [ TokensService ],
})
export class TokensComponent {
  public tokens: IToken[] = null;
    constructor(
        private tokensService: TokensService,
        private cdr: ChangeDetectorRef,
    ) {
      this.tokensService.getTokens().subscribe((it) => {
        this.tokens = it;
        this.cdr.markForCheck();
      });
    }

  public lastAccessed(token: IToken): string {
    const date = new Date(token.expTime);
    return date.toLocaleString();
  }

  public getIcon(token: IToken): string {
    const category = token.userAgent.category;
    if (category === 'pc') {
      return 'computer';
    } else if (category === 'smartphone' || category === 'mobilephone') {
      return 'smartphone';
    } else {
      return 'tv';
    }
  }

  public invalidateToken(token: IToken) {
    this.tokens = this.tokens.filter((it) => it.tokenHash !== token.tokenHash);
    this.tokensService.invalidateToken(token.tokenHash).subscribe((it) => {
        this.tokens = it;
        this.cdr.markForCheck();
      });
  }
}
