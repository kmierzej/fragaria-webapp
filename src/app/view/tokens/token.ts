
interface IUserAgent {
    name: string;
    category: string;
    os: string;
    version: string;
    vendor: string;
}

export interface IToken {
    userAgent: IUserAgent;
    tokenHash: string;
    valid: boolean;
    lastAccess: number;
    expTime: number;
    source: string;
}
