import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { AppReducers } from 'app/store';
import { AuthStore } from 'app/auth/token-services/auth.store';
import { getNavbarState, isLoggedAs } from 'app/auth/token-services/auth.reducers';

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'frg-app',
    template: `
        <frg-navbar [state]="navbarState | async" (navigate)="navigate($event)"></frg-navbar>
        <frg-admin-warning *ngIf="isAdmin | async"></frg-admin-warning>
        <frg-login [state]="loginState | async"></frg-login>
        <frg-logger [messages]="loggerState | async"></frg-logger>

        <main>
            <router-outlet></router-outlet>
        </main>
    `,
})
export class AppComponent implements OnInit {
    public navbarState: Observable<any>;
    public loggerState: Observable<any>;
    public loginState: Observable<any>;

    public isAdmin: Observable<boolean>;

    constructor(
        private router: Router,
        private store: Store<any>,
        private auth: AuthStore,
    ) {}

    public navigate(tab: string) {
        this.router.navigateByUrl(tab);
    }

    public ngOnInit() {
        this.loginState = this.store.select(AppReducers.getLoginPromptState);
        this.loggerState = this.store.select(AppReducers.getLoggerMessages);

        // hack to prevent ugly changed before checked exception.
        // this.isAdmin = this.store.select(AppReducers.isLoggedAdmin).delay(201);
        this.isAdmin = this.auth.observe(isLoggedAs('ADMIN'));
        this.navbarState = this.auth.observe(getNavbarState);
    }
}
