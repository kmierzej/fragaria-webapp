import {
  ApplicationRef,
  enableProdMode,
} from '@angular/core';
import {
  disableDebugTools,
  enableDebugTools,
} from '@angular/platform-browser';

let _decorateModuleRef = <T>(value: T): T => { return value; }; // tslint:disable-line

if ('production' === ENV) {
  enableProdMode();

  // Production
  _decorateModuleRef = (modRef: any) => {
    disableDebugTools();

    return modRef;
  };
} else {
  _decorateModuleRef = (modRef: any) => {
    const appRef = modRef.injector.get(ApplicationRef);
    const cmpRef = appRef.components[0];

    enableDebugTools(cmpRef);
    return modRef;
  };
}

export const decorateModuleRef = _decorateModuleRef;
