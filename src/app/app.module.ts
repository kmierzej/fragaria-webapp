import { ApplicationRef, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  createInputTransfer,
  createNewHosts,
  removeNgStyles,
} from '@angularclass/hmr';
import { Store, StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';

import { PromptModule } from './prompt';

import { AppRoutingModule } from './app.routing';
import { TokenServicesModule } from './auth/token-services';

import { AppActions, AppReducers } from './store';

import { SettingsEffectsModule } from './view/settings/settings-effects.module';
import { ManagedResourceEffectsModule } from './shared/managed-resource';

import { DashboardModule } from './view/dashboard';
import { HomeModule } from './view/home';
import { DefaultComponent } from './view/default';
import { MaterialModule } from 'app/shared/material';

import '../styles/app.theme.scss';
import '../styles/app.icons.scss';
import '../styles/user-table.style.scss';

import { AssigneesService, SearchUsersService, ProjectsService, TasksService } from './model/api-services';
const GLOBAL_SERVICES = [
    AssigneesService,
    SearchUsersService,
    ProjectsService,
    TasksService,
];

interface IStoreType {
  state: AppReducers.IState;
  restoreInputValues: () => void;
  disposeOldHosts: () => void;
}

@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [ AppComponent, DefaultComponent ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        PromptModule,
        StoreModule.provideStore(AppReducers.reducer),
        TokenServicesModule,
        ReactiveFormsModule,
        DashboardModule,
        HomeModule,
        MaterialModule,

        // EFFECTS MODULE - to be removed after angular bugfix
        SettingsEffectsModule,
        ManagedResourceEffectsModule,
    ],
    providers: GLOBAL_SERVICES,
})
export class AppModule {
    constructor(
        public appRef: ApplicationRef,
        public store: Store<AppReducers.IState>,
    ) {
        store.dispatch(new AppActions.AppInitAction());
    }

    public hmrOnInit(store: IStoreType) {
        if (!store || !store.state) {
            return;
        }
        if (store.state) {
            this.store.dispatch(new AppActions.SetAppStateAction(store.state));
        }
        if ('restoreInputValues' in store) {
            const restoreInputValues = store.restoreInputValues;
            setTimeout(restoreInputValues);
        }

        this.appRef.tick();
        delete store.state;
        delete store.restoreInputValues;
    }

    public hmrOnDestroy(store: IStoreType) {
        const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
        this.store.take(1).subscribe((s) => store.state = s);
        store.disposeOldHosts = createNewHosts(cmpLocation);
        store.restoreInputValues = createInputTransfer();
        removeNgStyles();
    }

    public hmrAfterDestroy(store: IStoreType) {
        store.disposeOldHosts();
        delete store.disposeOldHosts;
    }
}
