import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { IStoreStateProvider } from './store-state-provider.service';
import { IFState, ILoginState, ISAccessToken } from './model';

@Injectable()
export class AuthStore { //tslint:disable-line
    private stateObservable: Observable<IFState>;
    private stateEmitter: BehaviorSubject<IFState>;
    private state: IFState;

    constructor(private statePersistService: IStoreStateProvider) {
        this.state = this.statePersistService.getInitialState();
        this.stateEmitter = new BehaviorSubject(this.state);
        this.stateObservable = this.stateEmitter.asObservable();
    }

    public peek<T>(fun: (state: IFState) => T): T {
        return fun(this.state);
    }

    public observe<T>(fun: (state: IFState) => T): Observable<T> {
        return this.stateObservable.map(fun);
    }

    public registerTokens(input: ILoginState, prevRT?: string) {
        this.state.login = input.login;
        this.state.allPerm = input.allPerm;

        if (input.refreshToken != null) {
            this.state.refreshTokens.push(input.refreshToken);
            this.removeRefreshToken(prevRT);
        }
        this.statePersistService.persistState(this.state);

        if (input.accessTokens != null) {
            const refreshToken = (input.refreshToken && input.refreshToken.token ) || prevRT;
            if ( refreshToken == null) {
              // TODO: this is server error: throw error or sumfin
            }
            for (const token of input.accessTokens) {
                const newToken: ISAccessToken = { token: token.token, refreshToken };
                for ( const permission of token.permissions) {
                    this.state.accessTokens[permission] = newToken;
                }
            }
        }
        this.stateEmitter.next(this.state);
    }

    public removeAccessToken(permission: string, token: string) {
        const found = this.state.accessTokens[permission];
        if ( found && found.token === token) {
            delete this.state.accessTokens[permission];
            this.stateEmitter.next(this.state);
        }
    }

    public removeRefreshToken(token: string, persist: boolean = true) {
        if (token == null) {
            return;
        }
        this.state.refreshTokens = this.state.refreshTokens.filter((it) => it.token !== token);
        for (const key of Object.keys(this.state.accessTokens)) {
            if (this.state.accessTokens[key].refreshToken === token) {
                delete this.state.accessTokens[key];
            }
        }
        if (persist) {
          this.stateEmitter.next(this.state);
          this.statePersistService.persistState(this.state);
        }
    }

    public logoutPermission(permission: string) {
        // There is a problem here. What if token has more than one permission?
        this.state.refreshTokens = this.state.refreshTokens.filter((it) => it.permissions.indexOf(permission) === -1);
        delete this.state.accessTokens[permission];

        this.stateEmitter.next(this.state);
        this.statePersistService.persistState(this.state);
    }

    public logout() {
        this.state.accessTokens = {};
        this.state.refreshTokens = [];
        this.state.allPerm = [];
        this.stateEmitter.next(this.state);
        this.statePersistService.logout();
    }
}
