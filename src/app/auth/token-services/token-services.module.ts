import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { TokenService } from './services';

import { AuthStore } from './auth.store';
import { HttpService } from './http.service';

import { IAtRefresherService, AtRefresherService } from './at-refresher.service';
import { IStoreStateProvider, StoreStateProviderService } from './store-state-provider.service';
import { ResourceFetcherService } from './resource-fetcher.service';

@NgModule({
    imports: [
      HttpModule,
    ],
    providers: [
      AuthStore,
      HttpService,
      TokenService,
      ResourceFetcherService,
      { provide: IAtRefresherService, useClass: AtRefresherService },
      { provide: IStoreStateProvider, useClass: StoreStateProviderService },
    ],
})
export class TokenServicesModule { }
