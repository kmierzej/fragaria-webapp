import { StoreStateProviderMock } from './store-state-provider.service';
import { AuthStore } from './auth.store';
import { getLogin, getCurrentPermissions, getRT } from './auth.reducers';
import { ILoginState} from './model';

describe(`ResourceFetcher`, () => {
  let authStore: AuthStore;

  beforeEach(() => {
      const stateProvider = new StoreStateProviderMock();
      authStore = new AuthStore(stateProvider);
  });

  it(`simple register/unregister`, () => {
      const loginState: ILoginState = {
          refreshToken: {
              expirationTime: 10,
              permissions: ['ADMIN'],
              token: 'rt',
          },
          accessTokens: [],
          allPerm: ['ADMIN'],
          login: 'TEST',
      };
      authStore.registerTokens(loginState);

      expect(authStore.peek(getLogin)).toEqual('TEST');
      expect(authStore.peek(getCurrentPermissions)).toEqual(['ADMIN']);

      authStore.logout();
      expect(authStore.peek(getCurrentPermissions)).toEqual([]);
  });

  it(`override old token`, () => {
      const loginState: ILoginState = {
          refreshToken: {
              expirationTime: 10,
              permissions: ['ADMIN'],
              token: 'rt',
          },
          accessTokens: [],
          allPerm: ['ADMIN'],
          login: 'TEST',
      };
      authStore.registerTokens(loginState);
      expect(authStore.peek(getRT('ADMIN'))).toEqual('rt');

      const loginState2: ILoginState = {
          refreshToken: {
              expirationTime: 10,
              permissions: ['ADMIN'],
              token: 'rt2',
          },
          accessTokens: [],
          allPerm: ['ADMIN'],
          login: 'TEST',
      };
      authStore.registerTokens(loginState2, 'rt');
      expect(authStore.peek(getRT('ADMIN'))).toEqual('rt2');
  });

  it(`invalidate old token`, () => {
      const loginState: ILoginState = {
          refreshToken: {
              expirationTime: 10,
              permissions: ['ADMIN'],
              token: 'rt',
          },
          accessTokens: [],
          allPerm: ['ADMIN', 'USER'],
          login: 'TEST',
      };
      authStore.registerTokens(loginState);
      expect(authStore.peek(getRT('ADMIN'))).toEqual('rt');

      const loginState2: ILoginState = {
          refreshToken: {
              expirationTime: 10,
              permissions: ['USER'],
              token: 'rt2',
          },
          accessTokens: [],
          allPerm: ['USER'],
          login: 'TEST',
      };
      authStore.registerTokens(loginState2);
      expect(authStore.peek(getRT('ADMIN'))).toBeFalsy();
      expect(authStore.peek(getRT('USER'))).toBeTruthy();
  });

  it(`logout from one permission`, () => {
      const loginState: ILoginState = {
          refreshToken: {
              expirationTime: 10,
              permissions: ['ADMIN'],
              token: 'rt',
          },
          accessTokens: [],
          allPerm: ['ADMIN', 'USER'],
          login: 'TEST',
      };

      const loginState2: ILoginState = {
          refreshToken: {
              expirationTime: 10,
              permissions: ['USER'],
              token: 'rt2',
          },
          accessTokens: [],
          allPerm: ['USER', 'ADMIN'],
          login: 'TEST',
      };
      authStore.registerTokens(loginState);
      authStore.registerTokens(loginState2);
      authStore.logoutPermission('ADMIN');

      expect(authStore.peek(getRT('ADMIN'))).toBeFalsy();
      expect(authStore.peek(getRT('USER'))).toBeTruthy();
  });
});
