import { IRefreshToken, IFState, ISAccessToken } from './model';

function isTokenHasPermission(permission: string) {
    return (token: IRefreshToken) => token.permissions.indexOf(permission) > -1;
}

export function getAccessToken(permission: string): (state: IFState) => ISAccessToken {
    return (state: IFState) => {
        const access = state.accessTokens[permission];
        if ( access != null ) {
            return access;
        }
        const refresh = state.refreshTokens.find((it) => it.permissions.indexOf(permission) > -1);
        return {token: undefined, refreshToken: refresh && refresh.token};
    };
}

export function getAT(permission: string): (state: IFState) => ISAccessToken {
    return (state: IFState) => state.accessTokens[permission];
}

export function getRT(permission: string): (state: IFState) => string {
    return (state: IFState) => {
        if (state.allPerm.indexOf(permission) < 0) {
            return null;
        }
        const refresh = state.refreshTokens.find((it) => it.permissions.indexOf(permission) > -1);
        return refresh && refresh.token;
    };
}

export const getCurrentPermissions = (state: IFState) => {
    const permissions = state.refreshTokens.map((it) => it.permissions);
    const flattened = [].concat.apply([], permissions);
    const result = Array.from(new Set(flattened)) as string[];
    return result.filter((it) => state.allPerm.indexOf(it) > -1);
};

function* flatten(arr: any[]): any {
  if (!Array.isArray(arr)) {
      yield arr;
  } else {
    for (const el of arr) { yield* flatten(el); }
  }
}

export function isLoggedAs(permission: string): (state: IFState) => boolean {
  const hasPermission = isTokenHasPermission(permission);
  return (state: IFState) => state.refreshTokens.findIndex(hasPermission) > -1;
}

export function canLogAs(permission: string): (state: IFState) => boolean {
  return (state: IFState) => state.allPerm.indexOf(permission) > -1;
}

export const getLogin = (state: IFState) => state.login;
export const getNavbarState = (state: IFState) =>  ({
    login: getLogin(state),
    isUser: isLoggedAs('USER')(state),
    isAdmin: isLoggedAs('ADMIN')(state),
    canBeAdmin: canLogAs('ADMIN')(state),
});

export const getAdminState = (state: IFState) =>  ({
    isUser: isLoggedAs('USER')(state),
    isAdmin: isLoggedAs('ADMIN')(state),
    canBeAdmin: canLogAs('ADMIN')(state),
});
