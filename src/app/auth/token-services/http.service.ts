import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ResourceFetcherService } from './resource-fetcher.service';
import { IResourceRequest } from './resource-fetcher.model';

export const ADMIN = 'ADMIN';
export const USER = 'USER';
export const ACCOUNTANT = 'ACCOUNTANT';
export type Permission = 'ADMIN' | 'USER' | 'ACCOUNTANT';

@Injectable()
export class HttpService {
    private i = 0;
    constructor(
        private http: Http,
        private resourceFetcher: ResourceFetcherService,
    ) { }

    public get(
        url: string,
        permissions: Permission[] = [USER],
    ): Observable<Response> {
        const resource: IResourceRequest = {
            sender: 'any',
            id: (this.i++) + '',
            permissions,
            request: (headers) => this.http.get(url, { headers }),
        };
        return this.resourceFetcher.requestResource(resource).first();
    }

    public search(
        url: string,
        search: URLSearchParams,
        permissions: Permission[] = [USER],
    ): Observable<Response> {
        const resource: IResourceRequest = {
            sender: 'any',
            id: (this.i++) + '',
            permissions,
            request: (headers) => this.http.get(url, { headers, search }),
        };
        return this.resourceFetcher.requestResource(resource).first();
    }

    public getBlob(
        url: string, search: URLSearchParams = new URLSearchParams(),
        permissions: Permission[] = [USER],
    ): Observable<Response> {
        const resource: IResourceRequest = {
            sender: 'any',
            id: (this.i++) + '',
            permissions,
            request: (headers) => {
              headers.append('Accept', 'application/octet-stream');
              return this.http.get(url, { headers, search, responseType: ResponseContentType.Blob });
            },
        };
        return this.resourceFetcher.requestResource(resource).first();
    }

    public post(
        url: string,
        body: any,
        permissions: Permission[] = [USER],
    ): Observable<Response> {
        const resource: IResourceRequest = {
            sender: 'any',
            id: (this.i++) + '',
            permissions,
            request: (headers) => this.http.post(url, body, { headers }),
        };
        return this.resourceFetcher.requestResource(resource).first();
    }

    public patch(
        url: string,
        body: any,
        permissions: Permission[] = [USER],
    ): Observable<Response> {
        const resource: IResourceRequest = {
            sender: 'any',
            id: (this.i++) + '',
            permissions,
            request: (headers) =>
              this.http.patch(url, body, { headers }),
        };
        return this.resourceFetcher.requestResource(resource).first();
    }

    public put(
        url: string,
        body: any,
        permissions: Permission[] = [USER],
    ): Observable<Response> {
        const resource: IResourceRequest = {
            sender: 'any',
            id: (this.i++) + '',
            permissions,
            request: (headers) =>
              this.http.put(url, body, { headers }),
        };
        return this.resourceFetcher.requestResource(resource).first();
    }

    public delete(
        url: string,
        permissions: Permission[] = [USER],
    ): Observable<Response> {
        const resource: IResourceRequest = {
            sender: 'any',
            id: (this.i++) + '',
            permissions,
            request: (headers) =>
              this.http.delete(url, { headers }),
        };
        return this.resourceFetcher.requestResource(resource).first();
    }

    public deleteWithBody(
        url: string,
        body: any,
        permissions: Permission[] = [USER],
    ): Observable<Response> {
        const resource: IResourceRequest = {
            sender: 'any',
            id: (this.i++) + '',
            permissions,
            request: (headers) =>
              this.http.delete(url, getBody(headers, body)),
        };
        return this.resourceFetcher.requestResource(resource).first();
    }
}

function getBody(headers: Headers, body?: any) {
    if (body == null) {
        return new RequestOptions({headers, body}) ;
    }
    return {headers, body};
}
