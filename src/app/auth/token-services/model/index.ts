export interface IRefreshToken {
    expirationTime: number;
    permissions: string[];
    token: string;
}

export interface ILoginState {
    refreshToken: IRefreshToken;
    accessTokens: IRefreshToken[];
    allPerm: string[];
    login: string;
}

export interface ITokenSender {
    permission: string;
    success: boolean;
    errorType: ERROR_TYPE;
    token: string;
}

export const ADMIN_PERMISSIONS = ['ADMIN'];
export type ERROR_TYPE = 'NoLogin' | 'NoPermission' | 'NoServer';

export interface ISAccessToken {
    token: string;
    refreshToken: string;
}

export interface IFState {
    accessTokens: {[key: string]: ISAccessToken};
    refreshTokens: IRefreshToken[];
    allPerm: string[];
    login: string;
}
