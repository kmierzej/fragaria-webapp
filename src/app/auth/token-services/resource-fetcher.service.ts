import { Injectable } from '@angular/core';
import { Response, ResponseOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { AuthStore } from './auth.store';
import { IFState } from './model';
import { getAT, getRT, getCurrentPermissions } from './auth.reducers';

import {
    IStoredRequest,
    IResourceRequest, IResourceResponse } from './resource-fetcher.model';
import { IAtRefresherService } from './at-refresher.service';

@Injectable()
export class ResourceFetcherService {
    private eventStore: IStoredRequest[] = [];

    private requestQueue: Subject<IResourceRequest> = new Subject();
    private responseQueue: Subject<IResourceResponse> = new Subject();

    constructor(
        private store: AuthStore,
        private atRefresher: IAtRefresherService,
    ) {
        this.requestQueue.subscribe((it) => this.handleRequest(it));
        this.store.observe((it) => it).subscribe((it) => this.verifyEventStore(it));
    }

    public verifyEventStore(state: IFState) {
        for (const event of this.eventStore) {
            const at = getAT(event.permission)(state);
            if (at != null) {
                this.requestQueue.next(event.request);
                continue;
            }

            const currentPermissions = getCurrentPermissions(state);
            const hasAnyPerm = event.request.permissions.find((it) => currentPermissions.indexOf(it) > -1);
            if (hasAnyPerm != null) {
                const request = new Response(new ResponseOptions({status: 403}));
                const response = {
                    id: event.request.id,
                    success: false,
                    payload: request,
                };
                this.responseQueue.next(response);
                continue;
            }

            const rt = event.request.permissions.find((it) => getRT(it)(state) != null);
            if (rt != null) {
                // if permission is same as before, that means at is already bakin, so no need to retry
                if (rt !== event.permission) {
                this.requestQueue.next(event.request);
                }
                continue;
            }

            // request login rt
            console.log('please login as' + event.request.permissions);
        }
    }

    public requestResource(request: IResourceRequest): Observable<Response> {
        // first item sets up piping
        return Observable.from([undefined, request])
          .do((it) => {
              if (it) { this.requestQueue.next(it); }
          })
          .flatMapTo(this.watchForResource(request.id));
    }

    public watchForResource(id: string): Observable<Response> {
        return this.responseQueue
        .filter((it) => it.id === id)
        .flatMap((it) => {
              if (it.success) {
                  return Observable.of(it.payload);
              } else {
                  return Observable.throw(it.payload);
              }
          });
    }

    public handleHttpRequest(token: string, permission: string, request: IResourceRequest) {
        const header = createHeader(token);
        const query = request.request(header);
        query.subscribe((res) => {
            const response = {
                id: request.id,
                success: true,
                payload: res,
            };
            this.responseQueue.next(response);
        }, (res: Response) => {
            if (res.status === 401) {
                const toStore: IStoredRequest = {
                    request, token, permission,
                    reason: 'RefreshAT',
                };
                this.eventStore.push(toStore);
                this.atRefresher.requestAT(permission);
            } else {
                const response = {
                    id: request.id,
                    success: false,
                    payload: res,
                };
                this.responseQueue.next(response);
            }
        });
    }

    private handleRequest(request: IResourceRequest) {
        for ( const permission of request.permissions) {
            const at = this.store.peek(getAT(permission));
            if (at != null) {
                this.handleHttpRequest(at.token, permission, request);
                return;
            }

            const rt = this.store.peek(getRT(permission));
            if (rt != null) {
                const toStore: IStoredRequest = {
                    request, permission,
                    token: undefined,
                    reason: 'RefreshAT',
                };
                this.eventStore.push(toStore);
                this.atRefresher.requestAT(permission);
                return;
            }
        }
        console.log('please login as' + JSON.stringify(request.permissions));
    }
}

function createHeader(token: string) {
    const headers = new Headers();
    headers.append('authToken', token);
    return headers;
}
