import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { AuthStore } from './auth.store';
import { getRT } from './auth.reducers';
import { TokenService } from './services/token.service';

export abstract class IAtRefresherService {
    public abstract requestAT(permission: string): void;
}

export class ATRefresherMock implements IAtRefresherService { //tslint:disable-line
    public requestAT() { return; }
}

@Injectable()
export class AtRefresherService implements IAtRefresherService { //tslint:disable-line
    private processed: {[key: string]: boolean} = {};

    constructor(
        private store: AuthStore,
        private tokenService: TokenService,
    ) {}

    public requestAT(permission: string) {
        if (this.processed[permission]) {
            return;
        }
        this.processed[permission] = true;
        const rt = this.store.peek(getRT(permission));
        if (rt == null) {
            return;
        }

        this.tokenService.refreshAccess(rt, [permission]).subscribe((it) => {
          this.store.registerTokens(it);
          delete this.processed[permission];
        }, (error: Response) => {
            delete this.processed[permission];
            if (error.status === 403 || error.status === 500) {
              this.store.removeRefreshToken(rt);
            }
        });
    }
}
