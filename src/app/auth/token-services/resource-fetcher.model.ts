import { Observable } from 'rxjs/Observable';
import { Response, Headers } from '@angular/http';

export interface IResourceRequest {
    sender: string;
    id: string;
    permissions: string[];
    request: (headers: Headers) => Observable<Response>;
}

export interface IResourceResponse {
    id: string;
    success: boolean;
    payload: Response;
}

export interface IStoredRequest {
    request: IResourceRequest;
    reason: 'RefreshAT' | 'Unavailable';
    token?: string;
    permission?: string;
}
