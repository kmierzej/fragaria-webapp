import { Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { StoreStateProviderMock } from './store-state-provider.service';
import { ResourceFetcherService } from './resource-fetcher.service';
import { ATRefresherMock } from './at-refresher.service';
import { AuthStore } from './auth.store';
import { IFState } from './model';

describe(`ResourceFetcher`, () => {
  let resourceFetcher: ResourceFetcherService;
  let authStore: AuthStore;
  const state: IFState = {
      accessTokens: { 'ADMIN': { token: 'dd', refreshToken: 'dd' } },
      refreshTokens: [ { token: 'dd', expirationTime: 10, permissions: ['ADMIN'] } ],
      allPerm: ['ADMIN'],
      login: null,
  };

  beforeEach(() => {
      const stateProvider = new StoreStateProviderMock();
      stateProvider.setState(state);
      const refresherMock = new ATRefresherMock();
      authStore = new AuthStore(stateProvider);
      resourceFetcher = new ResourceFetcherService(authStore, refresherMock);
  });

  it(`failed query`, () => {
      const request = createRequest('id', createFailedResponse(500));
      resourceFetcher.requestResource(request).subscribe(() => fail(), (response) => {
          expect(response.status).toEqual(500);
      });
  });

  it(`successful query`, () => {
      const request = createRequest('id', createSuccessfulResponse('body'));
      resourceFetcher.requestResource(request).subscribe((response) => {
          expect(response.text()).toEqual('body');
      }, () => fail());
  });

  it(`filtering query`, () => {
      const request = createRequest('id', createSuccessfulResponse('body'));
      const filtered = createRequest('filtered', createSuccessfulResponse('filtered'));

      resourceFetcher.watchForResource('id').subscribe((response) => {
          expect(response.text()).toEqual('body');
      }, () => fail());

      resourceFetcher.watchForResource('filtered').subscribe((response) => {
          expect(response.text()).toEqual('filtered');
      }, () => fail());

      resourceFetcher.handleHttpRequest('token', 'ADMIN', request);
      resourceFetcher.handleHttpRequest('token', 'ADMIN', filtered);
  });

});

function createRequest(id: string, request: (_: Headers) => Observable<Response>) {
    return {
        sender: 'test',
        id,
        permissions: ['ADMIN'],
        request,
    };
}

function createFailedResponse(status: number, body?: any) {
  const options = new ResponseOptions({ body, status });
  const response = new Response(options);

  return (_: Headers) => Observable.throw(response) as Observable<Response>;
}

function createSuccessfulResponse(body?: any) {
  const options = new ResponseOptions({ body });
  const response = new Response(options);

  return (_: Headers) => Observable.of(response);
}
