import { Injectable } from '@angular/core';

import { IFState, IRefreshToken } from './model';

export abstract class IStoreStateProvider {
    public abstract logout(): void;
    public abstract persistState(state: IFState): void;
    public abstract getInitialState(): IFState;
}

export class StoreStateProviderMock implements IStoreStateProvider { //tslint:disable-line
    private state: IFState = { accessTokens: {}, refreshTokens: [], allPerm: [], login: null };
    public getInitialState = () => this.state;
    public setState = (state: IFState) => this.state = state;
    public logout() { return; }
    public persistState() { return; }
}

@Injectable()
export class StoreStateProviderService implements IStoreStateProvider { //tslint:disable-line
    public logout() {
        localStorage.removeItem('jwt4');
    }

    public persistState(state: IFState) {
        const toSave: IFState = {
            allPerm: state.allPerm,
            login: state.login,
            refreshTokens: this.getRefreshTokensToPersist(state.refreshTokens),
            accessTokens: {},
        };
        localStorage.setItem('jwt4', JSON.stringify(toSave));
    }

    public getInitialState(): IFState {
        const local = localStorage.getItem('jwt4');
        if (local != null) {
            return JSON.parse(local);
        }

        return {
          accessTokens: {},
          refreshTokens: [],
          allPerm: [],
          login: null,
        };
    }

    private getRefreshTokensToPersist(tokens: IRefreshToken[]) {
        if ('production' === ENV) {
            return tokens
              .filter((it) => it.permissions.every((perm) => isLowerPermission(perm)));
        }
        return tokens;
    }
}

function isLowerPermission(permission: string) {
    return permission !== 'ADMIN';
}
