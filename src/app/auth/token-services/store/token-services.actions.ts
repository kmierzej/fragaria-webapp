/* tslint:disable:max-classes-per-file */
import { Action } from '@ngrx/store';

import { type } from 'app/util';

export const ActionTypes = { // tslint:disable-line:variable-name
    ADMIN_TOKEN_EXPIRED:  type('[Auth] Admin Token Expired'),
    CHECK_WITH_SERVER:    type('[Auth] Check with server'),
    LOGOUT_ADMIN:         type('[Auth] Logout Admin'),
    LOGOUT_USER:          type('[Auth] Logout User'),
    REFRESH_TOKEN_ADMIN:  type('[Auth] Admin Token Refresh'),
    REFRESH_TOKEN_USER:   type('[Auth] User Token Refresh'),
    SAVE_TOKEN:           type('[Auth] [Token] Save'),
    VERIFY_ADMIN:         type('[Auth] Verify Admin'),
    VERIFY_USER:          type('[Auth] Verify User'),
};

export class AdminTokenExpiredAction implements Action {
    public type = ActionTypes.ADMIN_TOKEN_EXPIRED;
}

export class LogoutUserAction implements Action {
    public type = ActionTypes.LOGOUT_USER;
}

export class LogoutAdminAction implements Action {
    public type = ActionTypes.LOGOUT_ADMIN;
}

export class RefreshTokenAdminAction implements Action {
    public type = ActionTypes.REFRESH_TOKEN_ADMIN;
}

export class RefreshTokenUserAction implements Action {
    public type = ActionTypes.REFRESH_TOKEN_USER;
}

export type Actions
 =
 | AdminTokenExpiredAction
 | LogoutAdminAction
 | LogoutUserAction
 | RefreshTokenAdminAction
 | RefreshTokenUserAction;
