import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { TOKEN_LOGIN, TOKEN_LOGOUT, TOKEN_REFRESH, TOKEN_ACCESS} from 'app/endpoints';
import { ILoginState } from '../model';

@Injectable()
export class TokenService {
    constructor(private http: Http) {}

    public login(login: string, password: string, permissions?: string[]): Observable<ILoginState> {
        const credentials = {login, password};
        const params = getUrlParams(permissions);
        return this.http.post(TOKEN_LOGIN, credentials, { params }).map((res) => res.json());
    }

    public logout(token: string) {
        return this.http.post(TOKEN_LOGOUT, {token}).map((res) => res.json());
    }

    public refreshAccess(token: string, permissions: string[]): Observable<ILoginState> {
        return this.http.post(TOKEN_ACCESS, {token}, {params: {permissions}}).map((res) => res.json());
    }

    public refreshRefresh(token: string): Observable<ILoginState> {
        return this.http.post(TOKEN_REFRESH, {token}).map((res) => res.json());
    }
}

function getUrlParams(permissions: string[]): URLSearchParams | undefined {
    if (permissions == null) { return undefined; }
    const params = new URLSearchParams();
    for (const permission of permissions) { params.append('permissions', permission); }
    return params;
}
