export const ADMIN_LOGIN = BASE_URL + '/api/auth/admin/login';
export const USER_LOGIN = BASE_URL + '/api/auth/login';
export const ADMIN_LOGOUT = BASE_URL + '/api/auth/admin/logout';
export const ADMIN_REFRESH = BASE_URL + '/api/auth/admin/refresh';
export const USER_REFRESH = BASE_URL + '/api/auth/refresh';

export const SAMPLE_RESOURCE = BASE_URL + '/api/sample/hello-world';
export const SAMPLE_SECURE_RESOURCE = BASE_URL + '/api/sample/user';

export const ACCOUNT_RESOURCE = BASE_URL + '/api/account';
export const USER_RESOURCE = BASE_URL + '/api/admin/user';
export const PROJECT_RESOURCE = BASE_URL + '/api/projects';
export const USER_PERMISSION_RESOURCE_TEMPLATE = BASE_URL + '/api/admin/user/id/permissions';

export const GLOBAL_LOG_RESOURCE = BASE_URL + '/api/timesheet';
export const USER_LOG_RESOURCE = (user: string) => BASE_URL + '/api/timesheet/user/' + user;
export const TASK_LOG_RESOURCE = (task: string) => BASE_URL + '/api/timesheet/task/' + task;

export const TOKENS_RESOURCE = BASE_URL + '/api/auth/tokens';
export const TOKEN_RESOURCE = (hash: string) => TOKENS_RESOURCE + '/' + hash;

export const ASSIGNEE_RESOURCE = (project: string) => PROJECT_RESOURCE + '/' + project + '/assignee/assignee';
export const MANAGER_RESOURCE = (project: string) => PROJECT_RESOURCE + '/' + project + '/assignee/manager';

export const TASK_RESOURCE = (task: string) => {
  const project = task.substring(0, task.lastIndexOf('-'));
  return PROJECT_RESOURCE + '/' + project + '/tasks/' + task;
};

export const TOKEN_LOGIN = BASE_URL + '/api/auth/login';
export const TOKEN_REFRESH = BASE_URL + '/api/auth/refreshToken';
export const TOKEN_ACCESS = BASE_URL + '/api/auth/accessTokens';
export const TOKEN_LOGOUT = BASE_URL + '/api/auth/logout';
