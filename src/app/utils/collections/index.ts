export function groupBy(xs: any[], key: any) {
  return xs.reduce((rv, x) => {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
}

export function groupByArray<T, G>(xs: T[], key: ((a: T) => G) | G): Array<{key: G, values: T[]}> {
  return xs.reduce((rv, x) => {
    const v = key instanceof Function ? key(x) : (x as any)[key];
    const el = rv.find((r: any) => r && r.key === v);
    if (el) {
      el.values.push(x);
    } else {
      rv.push({ key: v, values: [x] });
    }
    return rv;
  }, []);
}

export function groupByCompositeArray<T, K>(
  xs: T[],
  keyGetter: (_: T) => K,
  keyComparator: (a: K, b: K) => boolean,
): Array<{key: K, values: T[]}> {
  return xs.reduce((rv, x) => {
    const key = keyGetter(x);
    const el = rv.find((r: any) => r && keyComparator(r.key, key));
    if (el) {
      el.values.push(x);
    } else {
      rv.push({ key, values: [x] });
    }
    return rv;
  }, []);
}

export function groupByArray2<T>(xs: T[], key: string): Array<{key: string, values: T[]}> {
  return xs.reduce((rv, x) => {
    const v = (x as any)[key];
    const el = rv.find((r: any) => r && r.key === v);
    delete (x as any)[key];
    if (el) {
      el.values.push(x);
    } else {
      rv.push({ key: v, values: [x] });
    }
    return rv;
  }, []);
}
