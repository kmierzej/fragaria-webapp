import { Observable } from 'rxjs/Observable';
import { ComponentType, MdDialogRef, MdDialog } from '@angular/material';

export class DialogController {
  private dialogs: { [key: string]: MdDialogRef<any> | null } = {};
  constructor(private dialog: MdDialog) {}

  public createDialog<T>(
    componentRef: ComponentType<T>,
    initializer: ((instance: T) => void) | null = null,
  ): Observable<any> {
      const name = componentRef.name;
      const dialog = this.dialog.open(componentRef, {});
      this.dialogs[name] = dialog;
      if (initializer) { initializer(dialog.componentInstance); }
      dialog.afterClosed().subscribe(() => delete this.dialogs[name]);
      return dialog.afterClosed().filter((x) => x !== undefined);
  }

  public cleanup() {
    const dialogs = this.dialogs;
    for (const i in dialogs) {
      if (dialogs.hasOwnProperty(i)) {
        dialogs[i].close();
        delete dialogs[i];
      }
    }
  }
}
