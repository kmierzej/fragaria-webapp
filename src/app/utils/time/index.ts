export const timePattern = new RegExp(/^\s*(?:([0-9]{1,2})\s*[Hh])?\s*(?:([0-9]{1,2})\s*[Mm])?\s*$/);

export function extractTimeFromString(value: string): number {
   const result = timePattern.exec(value);
   const hours: number = +result[1] || 0;
   const minutes: number = +result[2] || 0;

   return minutes + 60 * hours;
}

export function getTimeString(date: Date): string {
  const mm = date.getMonth() + 1; // getMonth() is zero-based
  const dd = date.getDate();

  return [
      date.getFullYear(),
      (mm > 9 ? '' : '0') + mm,
      (dd > 9 ? '' : '0') + dd,
  ].join('-');
}

export function getDateString(x: number) {
    const date = new Date(x * 1000);
    return date.toUTCString();
}

export function addDays(date: Date, days: number) {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

export function getMonday(date: Date) {
    const nDate = new Date(date);
    nDate.setHours(0, 0, 0, 0);
    const day = date.getDay() || 7;
    if (day !== 1) {
        nDate.setHours(-24 * (day - 1));
    }
    return nDate;
}

export function getTimeArray(start: Date, end: Date): Date[] {
  const length = daysBetween(start, end);
  return Array(length).fill(0).map((_, index) => addDays(start, index));
}

export function daysBetween(start: Date, end: Date) {
  const msPerDay = 24 * 60 * 60 * 1000;
  return Math.round((end.getTime() - start.getTime()) / msPerDay);
}
