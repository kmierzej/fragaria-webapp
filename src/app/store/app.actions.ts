/* tslint:disable:max-classes-per-file */
import { Action } from '@ngrx/store';
import { type } from 'app/util';

import { IState } from './app.reducers';

export const ActionTypes = { // tslint:disable-line:variable-name
    APP_INIT: type('[Fragaria] App init'),
    SET_APP_STATE: type('[Fragaria] setting app state'),
};

export class AppInitAction implements Action {
    public type = ActionTypes.APP_INIT;
}

export class SetAppStateAction implements Action {
    public type = ActionTypes.SET_APP_STATE;

    constructor(public payload: IState) {}
}

export type Actions = AppInitAction | SetAppStateAction;
