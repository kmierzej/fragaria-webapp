import { compose } from '@ngrx/core/compose';
import { combineReducers } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { createSelector } from 'reselect';

import { LoggerReducers } from 'app/prompt/logger/store';
import { LoginReducers } from 'app/prompt/login/store';
import { MngUsersReducers } from 'app/view/manage-users/store';
import { MngAccountReducers } from 'app/view/settings/store';
import { MngProjectsReducers } from 'app/view/projects/store';
import { MngTasksReducers } from 'app/view/projects/project-tasks-list/store';

import * as AppActions from './app.actions';

export interface IState {
    account: MngAccountReducers.IState;
    logger: LoggerReducers.IState;
    loginPrompt: LoginReducers.IState;
    manageUsers: MngUsersReducers.IState;
    manageProjects: MngProjectsReducers.IState;
    manageTasks: MngTasksReducers.IState;
}

const reducers = {
    account: MngAccountReducers.reducer,
    logger: LoggerReducers.reducer,
    loginPrompt: LoginReducers.reducer,
    manageUsers: MngUsersReducers.reducer,
    manageProjects: MngProjectsReducers.reducer,
    manageTasks: MngTasksReducers.reducer,
};

export function reducer(state: any, action: any) {
    state = globalReducer(state, action);
    if ('production' === ENV) {
        const productionReducer = combineReducers(reducers);
        return productionReducer(state, action);
    } else {
        const developmentReducer = compose(storeFreeze, combineReducers)(reducers);
        return developmentReducer(state, action);
    }
}

function globalReducer(state: IState, action: any) {
    switch (action.type) {
        case AppActions.ActionTypes.SET_APP_STATE: {
            const payload = (action as AppActions.SetAppStateAction).payload;
            return payload;
        }
        default: {
            return state;
        }
    }
}

export const getState = (state: IState) => state;

// Login Prompt Reducers
export const getLoginPromptState = (state: IState) => state.loginPrompt;
export const getAdminPromptState = createSelector(getLoginPromptState,
                                                  LoginReducers.getAdminPromptState);
export const getUserPromptState = createSelector(getLoginPromptState,
                                                 LoginReducers.getUserPromptState);

// ManageAccount State Reducers
export const getAccountState = (state: IState) => state.account;

// ManageUsers State Reducers
const getMngUsers = (state: IState) => state.manageUsers;
export const MngUsersSelectors = {
    getState: getMngUsers,
    getDeleteConfirmation: createSelector(getMngUsers, MngUsersReducers.selectors.getDeleteConfirmation),
    getPageMeta: createSelector(getMngUsers, MngUsersReducers.selectors.getPageMeta),
    getPage: createSelector(getMngUsers, MngUsersReducers.selectors.getPage),
    getPageState: createSelector(getMngUsers, MngUsersReducers.selectors.getPageState),
    getUser: (id: string) => createSelector(getMngUsers, MngUsersReducers.selectors.getResource(id)),
    getUsers: createSelector(getMngUsers, MngUsersReducers.selectors.getResources),
};

// ManageProjects State Reducers
const getMngProjects = (state: IState) => state.manageProjects;
export const MngProjectsSelectors = {
    getState: getMngProjects,
    getDeleteConfirmation: createSelector(getMngProjects, MngProjectsReducers.selectors.getDeleteConfirmation),
    getPageMeta: createSelector(getMngProjects, MngProjectsReducers.selectors.getPageMeta),
    getPage: createSelector(getMngProjects, MngProjectsReducers.selectors.getPage),
    getPageState: createSelector(getMngProjects, MngProjectsReducers.selectors.getPageState),
    getProject: (id: string) => createSelector(getMngProjects, MngProjectsReducers.selectors.getResource(id)),
    getProjects: createSelector(getMngProjects, MngProjectsReducers.selectors.getResources),
};

// ManageTasks State Reducers
const getTasksProjects = (state: IState) => state.manageTasks;
export const MngTasksSelectors = {
    getState: getTasksProjects,
    getDeleteConfirmation: createSelector(getTasksProjects, MngTasksReducers.selectors.getDeleteConfirmation),
    getPageMeta: createSelector(getTasksProjects, MngTasksReducers.selectors.getPageMeta),
    getPage: createSelector(getTasksProjects, MngTasksReducers.selectors.getPage),
    getPageState: createSelector(getTasksProjects, MngTasksReducers.selectors.getPageState),
    getProject: (id: string) => createSelector(getTasksProjects, MngTasksReducers.selectors.getResource(id)),
    getProjects: createSelector(getTasksProjects, MngTasksReducers.selectors.getResources),
};

// LoggerReducers
export const getLoggerState = (state: IState) => state.logger;
export const getLoggerMessages = createSelector(getLoggerState, LoggerReducers.getMessages);
