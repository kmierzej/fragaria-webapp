import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthStore } from 'app/auth/token-services/auth.store';
import { isLoggedAs } from 'app/auth/token-services/auth.reducers';

@Injectable()
export class UserAuthGuard implements CanActivate {

    constructor(private store: AuthStore, private router: Router) { }

    public canActivate(): boolean {
        const isLoggedUser = this.store.peek(isLoggedAs('USER'));
        if (!isLoggedUser)  {
            this.router.navigate(['/home']);
        }
        return isLoggedUser;
    }
}
