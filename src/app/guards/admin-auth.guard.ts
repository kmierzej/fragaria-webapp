import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AuthStore } from 'app/auth/token-services/auth.store';
import { isLoggedAs } from 'app/auth/token-services/auth.reducers';

@Injectable()
export class AdminAuthGuard implements CanActivate {
    constructor(private store: AuthStore) { }

    public canActivate(): boolean {
        return this.store.peek(isLoggedAs('ADMIN'));
    }
}
