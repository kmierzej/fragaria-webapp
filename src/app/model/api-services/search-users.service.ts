import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { HttpService, ADMIN, USER } from 'app/auth/token-services/http.service';
import { USER_RESOURCE } from 'app/endpoints';

import { IUser } from 'app/model/api';

@Injectable()
export class SearchUsersService {
    constructor(private http: HttpService) { }

    public findUserByName(name: string): Observable<IUser[]> {
        const params = new URLSearchParams();
        params.append('from', name);
        params.append('limit', 5 + '');
        return this.http.search(USER_RESOURCE, params, [ADMIN, USER])
          .map((x) => x.json())
          .map((x) => x['resources']);
    }
}
