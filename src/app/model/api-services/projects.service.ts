import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { URLSearchParams } from '@angular/http';

import { HttpService, ADMIN, USER } from 'app/auth/token-services/http.service';
import { PROJECT_RESOURCE } from 'app/endpoints';
import { IProject } from 'app/model/api';
import { AppReducers } from 'app/store';

import { MngProjectsReducers } from 'app/view/projects/store';

@Injectable()
export class ProjectsService {
    constructor(private http: HttpService, private store: Store<any>) { }

    public patchProject(project: string, propertyName: string, property: string): Observable<IProject> {
        return this.http.patch(
            PROJECT_RESOURCE + '/' + project,
            {[propertyName]: property},
            [ADMIN, USER],
        ).map((x) => x.json());
    }

    public findProjectByName(name: string): Observable<IProject> {
        return this.http.get(PROJECT_RESOURCE + '/' + name, [ADMIN, USER]).map((x) => x.json());
    }

    public findProjectsByName(name: string): Observable<IProject[]> {
        const params = new URLSearchParams();
        params.append('from', name);
        params.append('limit', 5 + '');
        return this.http.search(PROJECT_RESOURCE, params, [ADMIN, USER]).map((x) => x.json()['projects']);
    }

    public findProjectByNameCached(name: string): Observable<IProject> {
      return this.store.select(AppReducers.MngProjectsSelectors.getProject(name))
        .map((it) => (it || {value: null}).value)
        .flatMap((project) => {
            if (project == null) {
              this.store.dispatch(MngProjectsReducers.actions.reloadResource(name));
              return Observable.empty();
            }
            return Observable.of(project);
        });
    }
}
