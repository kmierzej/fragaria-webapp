import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { HttpService, USER, ADMIN } from 'app/auth/token-services/http.service';
import { TASK_RESOURCE } from 'app/endpoints';
import { ITask } from 'app/model/api';
import { AppReducers } from 'app/store';
import { MngTasksReducers } from 'app/view/projects/project-tasks-list/store';

@Injectable()
export class TasksService {
    constructor(private http: HttpService, private store: Store<any>) { }

    public patchTask(task: string, propertyName: string, property: string): Observable<ITask> {
        const url = TASK_RESOURCE(task);
        return this.http.patch(url, {[propertyName]: property}, [ADMIN, USER]).map((x) => x.json());
    }

    public findTaskByName(name: string): Observable<ITask> {
        const url = TASK_RESOURCE(name);
        return this.http.get(url, [ADMIN, USER]).map((x) => x.json());
    }

    public findTaskByNameCached(name: string): Observable<ITask> {
      return this.store.select(AppReducers.MngTasksSelectors.getProject(name))
        .map((it) => (it || {value: null}).value)
        .flatMap((project) => {
            if (project == null) {
              this.store.dispatch(MngTasksReducers.actions.reloadResource(name));
              return Observable.empty();
            }
            return Observable.of(project);
        });
    }
}
