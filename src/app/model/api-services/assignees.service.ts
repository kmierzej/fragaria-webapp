import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpService, ADMIN, USER } from 'app/auth/token-services/http.service';
import { ASSIGNEE_RESOURCE, MANAGER_RESOURCE } from 'app/endpoints';
import { IProject } from 'app/model/api';
import { IUser } from 'app/model/api';

@Injectable()
export class AssigneesService {
    constructor(private http: HttpService) { }

    public getAssignees(project: string): Observable<IUser[]> {
        return this.http.get(ASSIGNEE_RESOURCE(project), [ADMIN]).map((x) => x.json());
    }

    public addAssignees(project: string, assignees: Array<{display: string, value: IUser}>): Observable<IUser[]> {
        return this.http.post(
            ASSIGNEE_RESOURCE(project),
            assignees.map((it) => it.value.login),
            [ADMIN, USER],
        ).map((x) => x.json());
    }

    public addManager(project: string, manager: {display: string, value: IUser}): Observable<IProject> {
        return this.http.post(
            MANAGER_RESOURCE(project),
            manager.value.login,
            [ADMIN, USER],
        ).map((x) => x.json());
    }

    public removeManager(project: string): Observable<IProject> {
        return this.http.delete(
            MANAGER_RESOURCE(project),
            [ADMIN, USER],
        ).map((x) => x.json());
    }

    public removeAssignees(project: string, assignees: Array<{display: string, value: IUser}>): Observable<IUser[]> {
        return this.http.deleteWithBody(
            ASSIGNEE_RESOURCE(project),
            assignees.map((it) => it.value.login),
            [ADMIN, USER],
        ).map((x) => x.json());
    }
}
