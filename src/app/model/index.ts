export interface IAdminState {
    isAdmin: boolean;
    canBeAdmin: boolean;
}
