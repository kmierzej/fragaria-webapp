export interface IUser {
    login?: string;
    email?: string;
    fullname?: string;
    password?: string;
    permissions?: string[];
}

export const adminOption = { value: 'ADMIN', display: 'Admin'};
export const userOption = { value: 'USER', display: 'User'};
export const allUserStates = [ adminOption, userOption ];
export const userStatesMap = { [adminOption.value]: adminOption, [userOption.value]: userOption};
