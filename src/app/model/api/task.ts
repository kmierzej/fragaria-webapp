export interface ITask {
    id: string;

    title: string;
    status: string;

    created: string;
    updated: string;

    estimatedTime: number;
    totalLogged: number;
}
