export interface ITimeEntry {
    user: string;
    task: string;
    date: string;
    time: number;
}
