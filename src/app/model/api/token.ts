export interface IToken {
    expirationTime: number;
    token: string;
}

export interface ITokenPair {
    accessToken: IToken;
    refreshToken: IToken;
    allPerm: string[];
    grantedPerm: string[];
    login: number;
}
