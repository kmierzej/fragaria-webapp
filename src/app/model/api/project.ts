export interface IProject {
    id: string;
    title: string;
    status: string;
    manager: string;
    visibilityStrategy: string;
    loggingWhoStrategy: string;
    createTaskStrategy: string;
    estimatedTime: number;
    totalLogged: number;
}

export interface IProjectStatus {
    visible: boolean;
    loggable: boolean;
    editable: boolean;
    manager: boolean;
}

export const openStatus = {display: 'Otwarty', value: 'OPEN'};
export const pendingStatus = {display: 'Oczekujący', value: 'PENDING'};
export const closedStatus = {display: 'Zamknięty', value: 'CLOSED'};

export const allStatus = [ openStatus, pendingStatus, closedStatus ];
export const statusMap = {
    [openStatus.value] : openStatus,
    [pendingStatus.value] : pendingStatus,
    [closedStatus.value] : closedStatus,
};

export const managerSetting = {display: 'Manager', value: 'MANAGER'};
export const allSetting = {display: 'Wszyscy', value: 'ALL'};
export const assignedSetting = {display: 'Pracownicy', value: 'ASSIGNED'};

export const projectVisibilityOptions = [ allSetting, assignedSetting ];
export const projectCreateTaskOptions = [ ...projectVisibilityOptions, managerSetting ];
export const projectLoggingWhoOptions = [ ...projectVisibilityOptions ];
