import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
    selector: 'frg-filter-users-dialog',
    template: `
    <form role="form" (submit)="close($event, filterValue.value)">
      <input #filterValue id="filter-by-input"/>
      <button type="submit" id="filter-by-button">Szukaj</button>
    </form>
    `,
})
export class FilterResourceDialogComponent {
  constructor(public dialogRef: MdDialogRef<FilterResourceDialogComponent>) { }

  public close(event: Event, value: string) {
    event.preventDefault();
    this.dialogRef.close(value);
  }
}
