import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MdDialogRef } from '@angular/material';

import { IFormOption } from '../model';

@Component({
    selector: 'frg-filter-options-dialog',
    template: `
    <form role="form" (ngSubmit)="onSubmit()" [formGroup]="form">
      <div *ngFor="let option of _options">
        <md-checkbox [id]="option.value" [formControlName]="option.value">
        {{option.display}}
        </md-checkbox>
      </div>
      <button md-button type="submit" id="filter-by-button">Szukaj</button>
    </form>
    `,
})
export class FilterResourceOptionDialogComponent {
  public _options: IFormOption[]; //tslint:disable-line
  set options(options: IFormOption[]) {
    this._options = options;
    this.form = this.getFormGroup(options);
  }

  public form: FormGroup;

  constructor(public dialogRef: MdDialogRef<FilterResourceOptionDialogComponent>) { }

  public onSubmit() {
    const result = this._options.filter((x) => this.form.value[x.value]).map((x) => x.value.toUpperCase());
    this.dialogRef.close(result);
  }

  private getFormGroup(options: IFormOption[]): FormGroup {
    const group: any = {};
    options.forEach((option) => {
      group[option.value] = new FormControl(option.checked);
    });
    return new FormGroup(group);
  }
}
