import { Component, ChangeDetectionStrategy } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { IDeleteResourceForm } from '../model';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-delete-users-dialog',
    template: `
    <h2 md-dialog-title>Czy usunąć {{login}}?</h2>
    <form role="form" (ngSubmit)="abort()" #form="ngForm">
      <div class="form-group" style="margin: 10px; text-align: center;">
      <button md-button type="submit">Nie</button>
      <button md-button type="button" (click)="confirm()">
        Tak, usuń permanentnie.
      </button>
      </div>
      <div class="form-group" style="margin: 10px;">
        <small>
          <md-checkbox type="checkbox" [(ngModel)]="dontprompt" name="dontprompt">
            Nie pytaj ponownie.
          </md-checkbox>
        </small>
      </div>
    </form>
    `,
})
export class ConfirmDeleteDialogComponent {
  public login: string;
  public dontprompt = false;

  constructor(public dialogRef: MdDialogRef<ConfirmDeleteDialogComponent>) { }

  public abort() {
    const result: IDeleteResourceForm = { confirmDelete: !this.dontprompt, id: undefined };
    this.dialogRef.close(result);
  }

  public confirm() {
    const result: IDeleteResourceForm = { confirmDelete: !this.dontprompt, id: this.login };
    this.dialogRef.close(result);
  }
}
