import { AbstractControl } from '@angular/forms';

export interface IDictionaryOfDictionaries {[_: string]: {[_: string]: string}; }

export interface IDictionary {[_: string]: string; }

export function getErrorMessage(formGroup: AbstractControl, dict: IDictionary): string {
      return getErrorMessage2(formGroup.errors, dict);
}

export function getErrorMessage2(errors: IDictionary, dict: IDictionary): string {
      if (errors == null) {
        return null;
      } else if ( dict == null) {
        return 'Brak opisu błędu walidacji';
      }
      // guarantined to be at least one here
      const key = Object.keys(errors)[0];
      return dict[key] || 'Nieznany błąd walidacji';
}
