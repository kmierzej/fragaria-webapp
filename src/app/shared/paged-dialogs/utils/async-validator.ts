import { AbstractControl, Validator } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

export class AsyncValidator implements Validator {
  private validatorInput: Subject<string>;
  private validatorChain: Observable<any>;
  constructor(service: (_: string) => Observable<boolean>) {
    this.validatorInput = new Subject();
    this.validatorChain = this.validatorInput
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap((value) => service(value).mapTo({unique: true}).catch(() => Observable.of(null)))
      .share()
      .take(1);
  }

  public validate = (control: AbstractControl) => {
    // An immediate timeout is set because the next has to occur after the
    // validator chain is subscribed to.
    setTimeout(() => this.validatorInput.next(control.value), 0);
    return this.validatorChain;
  }
}
