import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from 'app/shared/material';

import {
    ConfirmDeleteDialogComponent,
    FilterResourceDialogComponent,
    FilterResourceOptionDialogComponent,
} from './components';

import { PagedResourceDialogsService } from './services';

const DIALOGS = [
    ConfirmDeleteDialogComponent,
    FilterResourceDialogComponent,
    FilterResourceOptionDialogComponent,
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
    ],
    declarations: [ ...DIALOGS ],
    entryComponents: [ ...DIALOGS ],
    exports: [ ...DIALOGS ],
    providers: [ PagedResourceDialogsService ],
})
export class PagedResourceDialogsModule {}
