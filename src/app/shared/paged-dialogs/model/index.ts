export interface IDeleteResourceForm {
  confirmDelete: boolean;
  id: string;
}

export interface IFormOption {
  display: string;
  value: string;
  checked: boolean;
}
