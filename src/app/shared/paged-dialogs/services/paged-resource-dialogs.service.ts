import { Injectable } from '@angular/core';
import { MdDialog, ComponentType } from '@angular/material';
import { Observable } from 'rxjs/Observable';

import { DialogController } from 'app/utils/dialogs';

import {
  ConfirmDeleteDialogComponent,
  FilterResourceDialogComponent,
  FilterResourceOptionDialogComponent,
} from '../components';

import { IDeleteResourceForm, IFormOption } from '../model';

@Injectable()
export class PagedResourceDialogsService {
  private controller: DialogController;
  constructor(dialog: MdDialog) {
    this.controller = new DialogController(dialog);
  }

  public openSelectOptionDialog(options: IFormOption[]): Observable<string[]> {
      const initializer = (instance: FilterResourceOptionDialogComponent) => instance.options = options;
      return this.controller.createDialog(FilterResourceOptionDialogComponent, initializer);
  }

  public openDeleteDialog(confirmDelete: boolean, id: string): Observable<IDeleteResourceForm> {
      if (!confirmDelete) {
          return Observable.of({confirmDelete, id});
      }

      const initializer = (instance: ConfirmDeleteDialogComponent) => instance.login = id;
      return this.controller.createDialog(ConfirmDeleteDialogComponent, initializer).filter((x) => x.id !== undefined);
  }

  public openFilterDialog(): Observable<string> {
    return this.controller.createDialog(FilterResourceDialogComponent);
  }

  public createDialog<T>(
    componentRef: ComponentType<T>,
    initializer: ((instance: T) => void) | null = null,
  ): Observable<any> {
    return this.controller.createDialog(componentRef, initializer);
  }

  public cleanup() {
    this.controller.cleanup();
  }
}
