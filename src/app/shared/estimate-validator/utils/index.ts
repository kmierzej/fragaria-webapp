export const datePattern = new RegExp(
  /^\s*(?:([0-9]+)\s*[mM][oO])?\s*(?:([0-9]+)\s*[wW])?\s*(?:([0-9]+)\s*[dD])?\s*(?:([0-9]{1,2})\s*[Hh])?\s*(?:([0-9]{1,2})\s*[Mm])?\s*$/ //tslint:disable-line
);

export const MINUTES_IN_HOUR = 60;
export const HOURS_IN_DAY = 8;
export const DAYS_IN_WEEK = 5;
export const WEEKS_IN_MONTH = 4;

export const MINUTES_IN_DAY = MINUTES_IN_HOUR * HOURS_IN_DAY;
export const MINUTES_IN_WEEK = MINUTES_IN_DAY * DAYS_IN_WEEK;
export const MINUTES_IN_MONTH = MINUTES_IN_WEEK * WEEKS_IN_MONTH;

export interface IEstimate { months: number; weeks: number; days: number; hours: number; minutes: number; }

export function extractEstimate(value: string): IEstimate {
    const result = datePattern.exec(value);
    if ( result == null ) {
        return { months: 0, weeks: 0, days: 0, hours: 0, minutes: 0};
    }

    const months = +result[1] || 0;
    const weeks = +result[2] || 0;
    const days = +result[3] || 0;
    const hours = +result[4] || 0;
    const minutes = +result[5] || 0;

    return { months, weeks, days, hours, minutes };
}

export function estimateToString(estimate: IEstimate): string {
    const months = estimate.months ? estimate.months + 'MO ' : '';
    const weeks = estimate.weeks ? estimate.weeks + 'W ' : '';
    const days = estimate.days ? estimate.days + 'D ' : '';
    const hours = estimate.hours ? estimate.hours + 'H ' : '';
    const minutes = estimate.minutes ? estimate.minutes + 'M' : '';

    return (months + weeks + days + hours + minutes).trim();
}

export function estimateStringToMinutes(value: string): number {
    return estimateToMinutes(extractEstimate(value));
}

export function estimateToMinutes(estimate: IEstimate): number {
  const totalWeeks = estimate.months * WEEKS_IN_MONTH + estimate.weeks;
  const totalDays = totalWeeks * DAYS_IN_WEEK + estimate.days;
  const totalHours = totalDays * HOURS_IN_DAY + estimate.hours;
  const totalMinutes = totalHours * MINUTES_IN_HOUR + estimate.minutes;
  return totalMinutes;
}

export function minutesToEstimate(value: number): IEstimate {
  const months = Math.floor(value / MINUTES_IN_MONTH);
  const weeks = Math.floor(value / MINUTES_IN_WEEK) % WEEKS_IN_MONTH;
  const days = Math.floor(value / MINUTES_IN_DAY) % DAYS_IN_WEEK;
  const hours = Math.floor(value / MINUTES_IN_HOUR) % HOURS_IN_DAY;
  const minutes = value % MINUTES_IN_HOUR;

  return { months, weeks, days, hours, minutes };
}

export function minutesToEstimateString(value: number): string {
  return estimateToString(minutesToEstimate(value));
}
