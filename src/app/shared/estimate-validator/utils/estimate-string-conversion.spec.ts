import { extractEstimate, estimateToString } from './';

describe(`extractEstimateFromString`, () => {
  it(`extract estimate from string`, () => {
    const result = extractEstimate('1mo 2w 3d 4H 5M');
    const expected = { months: 1, weeks: 2, days: 3, hours: 4, minutes: 5 };

    expect(result).toEqual(expected);
  });

  it(`extract empty estimate from string`, () => {
    const result = extractEstimate('');
    const expected = { months: 0, weeks: 0, days: 0, hours: 0, minutes: 0 };

    expect(result).toEqual(expected);
  });

  it(`extract empty estimate from null`, () => {
    const result = extractEstimate(null);
    const expected = { months: 0, weeks: 0, days: 0, hours: 0, minutes: 0 };

    expect(result).toEqual(expected);
  });

  it(`empty estimate to string`, () => {
    const estimate = { months: 0, weeks: 0, days: 0, hours: 0, minutes: 0 };
    const result = estimateToString(estimate);

    expect(result).toEqual('');
  });

  it(`estimate to string`, () => {
    const estimate = { months: 1, weeks: 2, days: 3, hours: 4, minutes: 5 };
    const result = estimateToString(estimate);

    expect(result).toEqual('1MO 2W 3D 4H 5M');
  });
});
