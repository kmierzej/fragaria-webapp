import { estimateToMinutes, minutesToEstimate } from './';

describe(`Estimate and minutes conversion `, () => {
  it(`minutes from day`, () => {
    const estimate = { months: 0, weeks: 0, days: 1, hours: 0, minutes: 0 };
    const result = estimateToMinutes(estimate);
    expect(result).toEqual(8 * 60);
  });

  it(`estimate from day`, () => {
    const estimate = { months: 0, weeks: 0, days: 1, hours: 0, minutes: 0 };
    const result = minutesToEstimate(8 * 60);
    expect(result).toEqual(estimate);
  });

  it(`minutes from week`, () => {
    const estimate = { months: 0, weeks: 1, days: 0, hours: 0, minutes: 0 };
    const result = estimateToMinutes(estimate);
    expect(result).toEqual(5 * 8 * 60 );
  });

  it(`estimate from week`, () => {
    const estimate = { months: 0, weeks: 1, days: 0, hours: 0, minutes: 0 };
    const result = minutesToEstimate(5 * 8 * 60);
    expect(result).toEqual(estimate);
  });

  it(`minutes from month`, () => {
    const estimate = { months: 1, weeks: 0, days: 0, hours: 0, minutes: 0 };
    const result = estimateToMinutes(estimate);
    expect(result).toEqual(4 * 5 * 8 * 60);
  });

  it(`estimate from month`, () => {
    const estimate = { months: 1, weeks: 0, days: 0, hours: 0, minutes: 0 };
    const result = minutesToEstimate(4 * 5 * 8 * 60);
    expect(result).toEqual(estimate);
  });

  it(`minutes from hours and minutes simple`, () => {
    const estimate = { months: 0, weeks: 0, days: 0, hours: 1, minutes: 1 };
    const result = estimateToMinutes(estimate);
    expect(result).toEqual(61);
  });

  it(`estimate from hours and minutes simple`, () => {
    const estimate = { months: 0, weeks: 0, days: 0, hours: 1, minutes: 1 };
    const result = minutesToEstimate(61);
    expect(result).toEqual(estimate);
  });

  it(`simplify check`, () => {
    const estimate = { months: 0, weeks: 8, days: 0, hours: 0, minutes: 0 };
    const minutes = estimateToMinutes(estimate);

    const simplified = { months: 2, weeks: 0, days: 0, hours: 0, minutes: 0 };
    const result = minutesToEstimate(minutes);

    expect(result).toEqual(simplified);
  });
});
