import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EstimateValidatorDirective } from './estimate-validator.directive';

@NgModule({
    declarations: [
        EstimateValidatorDirective,
    ],
    exports: [
        EstimateValidatorDirective,
    ],
    imports: [
        CommonModule,
        FormsModule,
    ],
})
export class EstimateValidatorModule {}
