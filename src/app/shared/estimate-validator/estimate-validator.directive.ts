import { Directive, OnInit } from '@angular/core';
import { FormControl, Validator, ValidatorFn, NG_VALIDATORS } from '@angular/forms';
import { estimateValidatorFactory } from './estimate-validator.factory';

@Directive({
  selector: '[frgEstimateValidator][ngModel]',
  providers: [ { provide: NG_VALIDATORS, useExisting: EstimateValidatorDirective, multi: true } ],
})
export class EstimateValidatorDirective implements Validator, OnInit {
  private validator: ValidatorFn;

  public ngOnInit() {
    this.validator = estimateValidatorFactory();
  }

  public validate(control: FormControl) {
    return this.validator(control);
  }
}
