import { FormControl, ValidatorFn } from '@angular/forms';
import { datePattern } from './utils';

export const REQUIRED = { required: true };
export const PARSE_BAD_FORMAT = { parseBadFormat: true };
export const PARSE_EMPTY = { parseEmpty: true };
export const PARSE_OVERFLOW = { parseOverflow: true };

export function estimateValidatorFactory(): ValidatorFn {
  return (control: FormControl) => {
    if (control.value == null ) {
      return null;
    }
    const result = datePattern.exec(control.value);
    if (result == null) {
      return PARSE_BAD_FORMAT;
    }

    return null;
  };
}
