import { FormControl, ValidatorFn } from '@angular/forms';
import { timePattern } from 'app/utils/time';

export const REQUIRED = { required: true };
export const PARSE_BAD_FORMAT = { parseBadFormat: true };
export const PARSE_EMPTY = { parseEmpty: true };
export const PARSE_OVERFLOW = { parseOverflow: true };

export function timeValidatorFactory(): ValidatorFn {
  return (control: FormControl) => {
    if (control.value === null || control.value === '') {
      return REQUIRED;
    }
    const result = timePattern.exec(control.value);
    if (result == null) {
      return PARSE_BAD_FORMAT;
    }

    let hours: number = +result[1];
    let minutes: number = +result[2];

    if (isNaN(hours) && isNaN(minutes)) {
      return PARSE_BAD_FORMAT;
    }
    hours = hours || 0;
    minutes = minutes || 0;

    if (hours === 0 && minutes === 0) {
      return PARSE_EMPTY;
    } else if (hours === 24 && minutes !== 0) {
      return PARSE_OVERFLOW;
    } else if (hours > 24 || minutes > 59) {
      return PARSE_OVERFLOW;
    }

    return null;
  };
}
