import { Directive, OnInit } from '@angular/core';
import { FormControl, Validator, ValidatorFn, NG_VALIDATORS } from '@angular/forms';
import { timeValidatorFactory } from './time-validator.factory';

@Directive({
  selector: '[frgTimeValidator][ngModel]',
  providers: [ { provide: NG_VALIDATORS, useExisting: TimeValidatorDirective, multi: true } ],
})
export class TimeValidatorDirective implements Validator, OnInit {
  private validator: ValidatorFn;

  public ngOnInit() {
    this.validator = timeValidatorFactory();
  }

  public validate(control: FormControl) {
    return this.validator(control);
  }
}
