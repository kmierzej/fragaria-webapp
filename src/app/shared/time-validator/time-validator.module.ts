import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TimeValidatorDirective } from './time-validator.directive';

@NgModule({
    declarations: [
        TimeValidatorDirective,
    ],
    exports: [
        TimeValidatorDirective,
    ],
    imports: [
        CommonModule,
        FormsModule,
    ],
})
export class TimeValidatorModule {}
