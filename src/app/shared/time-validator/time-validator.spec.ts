import { AbstractControl } from '@angular/forms';
import {
    timeValidatorFactory, PARSE_BAD_FORMAT,
    PARSE_EMPTY, PARSE_OVERFLOW, REQUIRED,
} from './time-validator.factory';

const validator = timeValidatorFactory();

describe(`TimeValidator`, () => {
  it(`check invalid input`, () => {
      const result = validate('ddd');
      expect(result).toEqual(PARSE_BAD_FORMAT);
  });

  it(`check empty`, () => {
      const result = validate('0h 0m');
      expect(result).toEqual(PARSE_EMPTY);
  });

  it(`check overflow hours`, () => {
      const result = validate('25h 0m');
      expect(result).toEqual(PARSE_OVERFLOW);
  });

  it(`check overflow minutes`, () => {
      const result = validate('23h 60m');
      expect(result).toEqual(PARSE_OVERFLOW);
  });

  it(`check null`, () => {
      const result = validate(null);
      expect(result).toEqual(REQUIRED);
  });

  it(`parse with spaces`, () => {
      const result = validate('  2      h 0  m  ');
      expect(result).toEqual(null);
  });

  it(`proper input`, () => {
      const result = validate('4h 4m');
      expect(result).toEqual(null);
  });

});

function validate(value: string): {[key: string]: any} | null {
    const control = {value} as AbstractControl;
    return validator(control);
}
