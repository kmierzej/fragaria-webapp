import * as MngResourceActions from './managed-resource.actions';
import * as MngResourceReducers from './managed-resource.reducers';

export { MngResourceActions, MngResourceReducers };
