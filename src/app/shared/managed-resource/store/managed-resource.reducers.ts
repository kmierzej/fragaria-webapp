import { Action } from '@ngrx/store';

import { IPagedResourceState, IPage, IResourceEntry } from '../model';
import * as manageResource from './managed-resource.actions';
import { Permission } from 'app/auth/token-services/http.service';

export interface IPermissionSet {
    modify: Permission[];
    remove: Permission[];
    view: Permission[];
}

export const initialStateFactory:
 (url: string, keySelector: string, permission: IPermissionSet, responseSelector: string) => IPagedResourceState<any> =
  (url: string, keySelector: string, permission: IPermissionSet, responseSelector: string) => ({
    confirmDelete: true,
    pageState: 'INITIAL',
    resources: [],
    selected: null,

    filters: {},
    url, keySelector, responseSelector, ...permission,
});

const clean = (obj: any) => {
    Object.getOwnPropertyNames(obj).forEach((name) => {
        const isEmptyArray = Array.isArray(obj[name]) && obj[name].length === 0;
        const isEmpty = obj[name] === null || obj[name] === undefined;
        if (isEmptyArray || isEmpty) { delete obj[name]; }
    });
    return obj;
};

export const reducerFactory = <T>(
    prefix: string,
    actionTypes: any,
    initialState: IPagedResourceState<T>,
    ) => (state = initialState, action: Action): IPagedResourceState<T> => {
    const getAction = (x: string) => prefix + x;
    switch (action.type) {
        case getAction(actionTypes.SET_PAGE_STATE): {
            const pageState = (action as manageResource.SetPageStateAction).payload.state;
            if (pageState === 'INITIAL') { return state; }

            return {
                ... state,
                pageState,
            };
        } case getAction(actionTypes.SET_DELETE_CONFIRMATION): {
            const payload = (action as manageResource.SetDeleteConfirmationAction).payload.deleteConfirmation;
            return {
                ... state,
                confirmDelete: payload,
            };
        } case getAction(actionTypes.SET_PAGE_META): {
            const payload = (action as manageResource.SetPageMetadataAction).payload;
            const newFilters = clean(Object.assign({}, payload));
            return {
                ... state,
                filters: Object.assign({}, newFilters),
            };
        } case getAction(actionTypes.DELETE_RESOURCE): {
            const payload = (action as manageResource.DeleteResourceAction).payload.id;
            return {
                ... state,
                resources: state.resources.filter((x) => x.key !== payload),
                selected: state.selected && state.selected.key === payload ? null : state.selected,
            };
        } case getAction(actionTypes.NEXT): {
            if (state.pageState === 'INITIAL') { return state; }
            return {
                ... state,
                pageState: 'LOADING_NEXT',
            };
        } case getAction(actionTypes.PREVIOUS): {
            if (state.pageState === 'INITIAL') { return state; }
            return {
                ... state,
                pageState: 'LOADING_PREVIOUS',
            };
        } case getAction(actionTypes.RELOAD): {
            if (state.pageState === 'INITIAL') { return state; }
            return {
                ... state,
                pageState: 'LOADING_ALL',
            };
        } case getAction(actionTypes.SET_RESOURCES): {
            const payload = (action as manageResource.SetResourcesAction<T>).payload;

            return {
                ... state,
                pageState: 'READY',
                resources: payload.slice(),
            };
        } case getAction(actionTypes.SET_RESOURCE): {
            const payload = (action as manageResource.SetResourceAction<T>).payload;

            let array = state.resources;
            const index = state.resources.findIndex((it) => it.key === payload.key);
            if (index > -1 ) {
                array = state.resources.slice();
                array[index] = payload;
            }

            return {
                ... state,
                resources: array,
                selected: payload,
            };
        } case getAction(actionTypes.SET_URL): {
            const url = (action as manageResource.SetUrlAction).payload.url;

            return {
                ... state,
                resources: (url === state.url) ? state.resources : [],
                url,
            };
        } default: {
            return state;
        }
    }
};

export const managedResourceSelectorsFactory = <T>() => ({
    getDeleteConfirmation: (state: IPagedResourceState<T>) => state.confirmDelete,
    getPageMeta: (state: IPagedResourceState<T>) => state.filters,
    getPage: (state: IPagedResourceState<T>): IPage => ({
        view: state.view,
        remove: state.remove,
        modify: state.modify,
        filters: state.filters,
        first: state.resources[0] ? state.resources[0].key : null,
        last: state.resources[0] ? state.resources[state.resources.length - 1].key : null,
        isFull: state.resources.length >= (+state.filters.limit || 10),
        url: state.url,
        responseSelector: state.responseSelector,
        keySelector: state.keySelector,
    }),
    getPageState: (state: IPagedResourceState<T>) => state.pageState,
    getResource: (key: string) => (state: IPagedResourceState<T>): IResourceEntry<T> => {
        if (state.selected && state.selected.key === key) {
            return state.selected;
        }
        return state.resources.find((x) => x.key === key);
    },
    getResources: (state: IPagedResourceState<T>) => state.resources,
});
