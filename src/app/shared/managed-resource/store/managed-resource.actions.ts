/* tslint:disable:max-classes-per-file */
import { PageLoadState, IFilter, IResourceEntry } from '../model';
import { Action, Store } from '@ngrx/store';

export const baseTags = {
    DELETE_RESOURCE: ('[ManagedResource] Delete resource'),
    CREATE_RESOURCE: ('[ManagedResource] Create resource'),

    SET_DELETE_CONFIRMATION: ('[ManagedResource] Set delete confirmation'),
    SET_PAGE_STATE: ('[ManagedResource] Set page state'),
    SET_PAGE_META: ('[ManagedResource] Set page meta'),
    SET_URL: ('[ManagedResource] Set url'),

    SET_RESOURCE: ('[ManagedResource] Set resource'),
    SET_RESOURCES: ('[ManagedResource] Set resources list'),

    NEXT: ('[ManagedResource] Next resources'),
    PREVIOUS: ('[ManagedResource] Previous resources'),
    RELOAD: ('[ManagedResource] Reload resources'),
    RELOAD_RESOURCE: ('[ManagedResource] Reload resource'),
};

export class ResourceAction implements Action {
    public type: string;
    constructor(public prefix: string, type: string) {
        this.type = prefix + type;
    }

    public dispatch(store: Store<any>) {
        store.dispatch(this);
    }
}

export class SetPageStateAction extends ResourceAction {
    constructor(prefix: string, public payload: {state: PageLoadState}) {
        super(prefix, baseTags.SET_PAGE_STATE);
    }
}

export class SetResourceAction<T> extends ResourceAction {
    constructor(prefix: string, public payload: IResourceEntry<T>) {
        super(prefix, baseTags.SET_RESOURCE);
    }
}

export class SetResourcesAction<T> extends ResourceAction {
    constructor(prefix: string, public payload: Array<IResourceEntry<T>>) {
        super(prefix, baseTags.SET_RESOURCES);
    }
}

export class SetDeleteConfirmationAction extends ResourceAction {
    constructor(prefix: string, public payload: {deleteConfirmation: boolean}) {
        super(prefix, baseTags.SET_DELETE_CONFIRMATION);
    }
}

export class SetPageMetadataAction extends ResourceAction {
    constructor(prefix: string, public payload: IFilter) {
        super(prefix, baseTags.SET_PAGE_META);
    }
}

export class SetUrlAction extends ResourceAction {
    constructor(prefix: string, public payload: {url: string}) {
        super(prefix, baseTags.SET_URL);
    }
}

export class ReloadResoucesAction extends ResourceAction {
     constructor(prefix: string) {
        super(prefix, baseTags.RELOAD);
    }
}

export class DeleteResourceAction extends ResourceAction {
     constructor(prefix: string, public payload: {id: string}) {
        super(prefix, baseTags.DELETE_RESOURCE);
    }
}

export class CreateResourceAction<T> extends ResourceAction {
     constructor(prefix: string, public payload: IResourceEntry<T>) {
        super(prefix, baseTags.CREATE_RESOURCE);
    }
}

export class ReloadResourceAction extends ResourceAction {
     constructor(prefix: string, public payload: string) {
        super(prefix, baseTags.RELOAD_RESOURCE);
    }
}
export class EventFactory<T> {
    constructor(private prefix: string) {}
    public create = (value: T, key: string) => new CreateResourceAction(this.prefix, {value, key});
    public delete = (id: string) => new DeleteResourceAction(this.prefix, {id});
    public reload = () => new ReloadResoucesAction(this.prefix);
    public reloadResource = (id: string) => new ReloadResourceAction(this.prefix, id);
    public setMeta = (metadata: IFilter) => new SetPageMetadataAction(this.prefix, metadata);
    public setUrl = (url: string) => new SetUrlAction(this.prefix, {url});
    public setDeleteConfirmation =
        (deleteConfirmation: boolean) => new SetDeleteConfirmationAction(this.prefix, {deleteConfirmation})
    public select = (value: T, key: string) => new SetResourceAction(this.prefix, {value, key});
}

export type Actions<T> =
  | DeleteResourceAction
  | SetPageStateAction
  | SetResourceAction<T>
  | SetResourcesAction<T>
  | SetDeleteConfirmationAction
  | SetUrlAction
  | CreateResourceAction<T>
  ;
