import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { ManagedResourceService } from '../services';

import { LoggerActions } from 'app/prompt/logger/store';
import { AppReducers } from 'app/store';

import { baseTags } from './managed-resource.actions';
import * as MngActions from './managed-resource.actions';

import { IPage } from '../model';

const message = 'Brak dostępu do zasobu, spróbuj zalogować się ponownie jako administrator';

const helperMap: { [property: string]: (state: any) => IPage} = {
    '[USER]': AppReducers.MngUsersSelectors.getPage,
    '[PROJECTS]': AppReducers.MngProjectsSelectors.getPage,
    '[TASKS]': AppReducers.MngTasksSelectors.getPage,
};

@Injectable()
export class ManagedResourceEffects {
    @Effect()
    public deleteResource: Observable<Action> = this.actions
        .filter((action) => action.type.endsWith(baseTags.DELETE_RESOURCE))
        .map( (x) => (x as MngActions.DeleteResourceAction))
        .filter((x) => !(x.payload.id == null))
        .flatMap((x) =>
            this.getStateSnapshot(x.prefix).flatMap((state) =>
                this.resourceService.deleteByName(state.remove, state.url, x.payload.id)
                    .mapTo(({ type: 'empty'}) )
                    .catch( () => Observable.of(
                        new MngActions.ReloadResoucesAction(x.prefix) as Action,
                        new LoggerActions.LogMessageAction('Nie udało się usunąć: ' + x.payload.id),
                    ))),
        );

    @Effect()
    public createResource: Observable<Action> = this.actions
        .filter((action) => action.type.endsWith(baseTags.CREATE_RESOURCE))
        .map( (x) => (x as MngActions.CreateResourceAction<any>))
        .flatMap( (x) =>
            this.getStateSnapshot(x.prefix).flatMap((state) =>
            this.resourceService.create(state.modify, state.url, x.payload.value)
                .mapTo( new MngActions.ReloadResoucesAction(x.prefix))
                .catch( () => Observable.of(
                    new MngActions.ReloadResoucesAction(x.prefix) as Action,
                    new LoggerActions.LogMessageAction('Nie udało się utworzyć zasobu'),
                ))),
        );

    @Effect()
    public setMetadata: Observable<Action> = this.actions
        .filter((action) => action.type.endsWith(baseTags.SET_PAGE_META))
        .map((x) => (x as MngActions.SetPageMetadataAction))
        .map((action) => new MngActions.ReloadResoucesAction(action.prefix));

    @Effect()
    public reloadResources: Observable<Action> = this.actions
        .filter((action) => action.type.endsWith(baseTags.RELOAD))
        .debounceTime(200) // Ideally this should be done as "if different than in store, or not url update"
        .map((action) => (action as MngActions.ReloadResoucesAction))
        .switchMap((action) =>
             this.getStateSnapshot(action.prefix)
                .flatMap((state) =>
                    this.resourceService.findResources(
                        state.view,
                        state.url,
                        state.filters,
                        state.responseSelector,
                        state.keySelector,
                    ))
                .map((resources) => new MngActions.SetResourcesAction(action.prefix, resources))
                .catch(() => {
                    return Observable.of(new LoggerActions.LogMessageAction(message));
                }),
        );

    @Effect()
    public reloadResource: Observable<Action> = this.actions
        .filter((action) => action.type.endsWith(baseTags.RELOAD_RESOURCE))
        .map((action) => (action as MngActions.ReloadResourceAction))
        .switchMap((action) =>
             this.getStateSnapshot(action.prefix)
                .flatMap((state) =>
                    this.resourceService.findResource(
                        state.view,
                        state.url,
                        action.payload,
                        state.keySelector,
                    ))
                .map((resources) => new MngActions.SetResourceAction(action.prefix, resources))
                .catch(() => Observable.of(new LoggerActions.LogMessageAction(message))),
        );

    constructor(
      private actions: Actions,
      private resourceService: ManagedResourceService,
      private store: Store<AppReducers.IState>,
    ) { }

    private getStateSnapshot(prefix: string): Observable<IPage> {
        return this.store.select(helperMap[prefix]).take(1);
    }
}
