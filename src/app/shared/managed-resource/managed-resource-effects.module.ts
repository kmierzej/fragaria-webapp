import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';

import { ManagedResourceService } from './services';
import { ManagedResourceEffects } from './store/managed-resource.effects';

@NgModule({
    imports: [
        EffectsModule.run(ManagedResourceEffects),
    ] ,
    providers: [
        ManagedResourceService,
    ],
})
export class ManagedResourceEffectsModule { }
