import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { HttpService, Permission } from 'app/auth/token-services/http.service';
import { IFilter, IResourceEntry } from '../model';

@Injectable()
export class ManagedResourceService {
    constructor(
        private http: HttpService,
    ) {}

    public deleteByName(permission: Permission[], url: string, name: string): Observable<{}> {
        return this.http.delete(url + '/' + name, permission);
    }

    public create(permission: Permission[], url: string, user: any): Observable<{}> {
        return this.http.post(url, user, permission);
    }

    public findResource(permission: Permission[], url: string, id: string, keySel: string):
      Observable<IResourceEntry<any>> {
        return this.http.get(url + '/' + id, permission).map((x) => x.json())
            .map((it: any) => ({key: it[keySel], value: it}));
    }

    public findResources(permission: Permission[], url: string, filters: IFilter, responseSel: string, keySel: string):
      Observable<Array<IResourceEntry<any>>> {
        const params = this.getUrlSearchParams(filters);
        return this.http.search(url, params, permission).map((x) => x.json())
            .map((resp) => resp[responseSel].map((it: any) => ({key: it[keySel], value: it})));
    }

    private getUrlSearchParams(filters: IFilter): URLSearchParams {
        const params = new URLSearchParams();
        Object.getOwnPropertyNames(filters)
          .forEach((key) => {
              const value = filters[key];
              if (Array.isArray(value)) {
                  value.forEach((y) => params.append(key, y));
              } else {
                  params.set(key, value);
              }
        });
        return params;
    }
}
