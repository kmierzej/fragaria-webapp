import { Injectable } from '@angular/core';
import { Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { AppReducers } from 'app/store';
import { EventFactory } from '../store/managed-resource.actions';

import { IPage } from '../model';

@Injectable()
export class QueryParamsService {
    public getSubscription(
        params: Observable<Params>,
        single: string[],
        list: string[],
    ): Observable<Params> {
        return params.debounceTime(200).map((x) => {
            const toAssign: Params[] = [];
            toAssign.push(this.getExclusiveParams(x, ['from', 'after', 'before']));

            const extractedLimit = this.getSingle(x['limit']);
            if (this.isPositiveInteger(extractedLimit)) { toAssign.push({limit: extractedLimit}); }

            list.forEach((entry) => {
                const extracted = this.getList(x[entry]);
                if (extracted) { toAssign.push({[entry]: extracted}); }
            });

            single.forEach((entry) => {
                const extracted = this.getSingle(x[entry]);
                if (extracted) { toAssign.push({[entry]: extracted}); }
            });

            const result = Object.assign({}, ...toAssign);

            return result;
        });
    }

    public next(
        store: Store<any>,
        selector: (_: AppReducers.IState) => IPage,
        router: Router,
        actions: EventFactory<any>,
    ) {
        store.select(selector).first().subscribe((page) => {
            const alternative = page.filters.before || page.filters.from || page.filters.after;
            if (page.last || alternative) {
                const after = page.last ? page.last : alternative;
                const newFilters = Object.assign({}, page.filters, {after});
                delete newFilters.from;
                delete newFilters.before;
                actions.setMeta(newFilters).dispatch(store);
                router.navigate([], {
                    queryParams: newFilters,
                });
            } else {
                actions.reload().dispatch(store);
            }
        });
    }

    public previous(
        store: Store<any>,
        selector: (_: AppReducers.IState) => IPage,
        router: Router,
        actions: EventFactory<any>,
    ) {
        store.select(selector).first().subscribe((page) => {
            const alternative = page.filters.before || page.filters.from || page.filters.after;

            let before = page.first ? page.first : alternative;
            let newFilters = Object.assign({}, page.filters, { before });
            if (!page.first && page.filters.after) {
                const pageAfter = page.filters.after as string;

                const test = +pageAfter.substring(pageAfter.lastIndexOf('-') + 1);
                // Does end with a number?
                if (test) {
                    before = pageAfter.substring(0, pageAfter.lastIndexOf('-') + 1) + (test + 1);
                } else {
                    before = page.filters.after + ' ';
                }
                newFilters = Object.assign({}, page.filters, {before});
            }

            if (page.first || alternative) {
                delete newFilters.from;
                delete newFilters.after;
                actions.setMeta(newFilters).dispatch(store);
                router.navigate([], {
                    queryParams: newFilters,
                });
            } else {
                actions.reload().dispatch(store);
            }
        });
    }

    public from(
        store: Store<any>,
        selector: (_: AppReducers.IState) => IPage,
        router: Router,
        actions: EventFactory<any>,
        from: string,
    ) {
        store.select(selector).first()
        .subscribe((page) => {
            const newFilters = Object.assign({}, page.filters, { from });
            delete newFilters.before;
            delete newFilters.after;
            actions.setMeta(newFilters).dispatch(store);
            router.navigate([], {
                queryParams: newFilters,
            });
        });
    }

    public setQueryParam(
        store: Store<any>,
        selector: (_: AppReducers.IState) => IPage,
        router: Router,
        actions: EventFactory<any>,
        key: string,
        value: string | string[],
    ) {
        store.select(selector).first()
        .subscribe((page) => {
            const newFilters = Object.assign({}, page.filters, { [key]: value });
            actions.setMeta(newFilters).dispatch(store);
            router.navigate([], {
                queryParams: newFilters,
            });
        });
    }

    private isPositiveInteger(str: string) {
        const n = Math.floor(Number(str));
        return String(n) === str && n > 0;
    }

    private getExclusiveParams(params: Params, exclusives: string[]): Params | null {
        return exclusives.map((it) => {
            const mapped = params[it];
            return (mapped) ? {[it]: this.getSingle(mapped)} : null;
        }).find((it) => it != null);
    }

    private getSingle(param: string | string[]): string {
        return Array.isArray(param) ? param[0] : param;
    }

    private getList(param: string | string[]): string[] {
        if ( param === undefined || param === null ) { return null; }
        if ( Array.isArray(param)) {
            return param;
        }
        return [param];
    }
}
