import { Permission } from 'app/auth/token-services/http.service';
export interface IResourceEntry<T> { key: string; value: T; }

export interface IPagedResourceState<T> {
    confirmDelete: boolean;
    pageState: PageLoadState;
    resources: Array<IResourceEntry<T>>;
    selected: IResourceEntry<T>;
    filters: IFilter;

    modify: Permission[];
    remove: Permission[];
    view: Permission[];

    url: string;
    keySelector: string;
    responseSelector: string;
}

export interface IPage {
    filters: IFilter;
    first: string;
    last: string;
    isFull: boolean;

    modify: Permission[];
    remove: Permission[];
    view: Permission[];

    url: string;
    keySelector: string;
    responseSelector: string;
}

export interface IFilter {
    [key: string]: string | string[];
}

export type PageLoadState =
    | 'LOADING_NEXT'
    | 'LOADING_PREVIOUS'
    | 'LOADING_ALL'
    | 'READY'
    | 'INITIAL'
    ;

export type PageAction = 'NEXT' | 'PREVIOUS' | 'RELOAD';
