import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MaterialModule } from 'app/shared/material';

import { DataButtonComponent, PagedHeaderComponent } from './components';

@NgModule({
    declarations: [
        PagedHeaderComponent,
        DataButtonComponent,
    ],
    exports: [
        PagedHeaderComponent,
    ],
    imports: [
        MaterialModule,
        CommonModule,
    ],
})
export class PagedHeaderModule {}
