import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { PageLoadState } from 'app/shared/managed-resource/model';

@Component ({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-paged-header',
    template: `
        <frg-data-button class="previous-button"
            (clicked)="previous.emit($event)"
            [loading]="state === previousState"
            [icon]="'navigate_before'"
        ></frg-data-button>

        <button md-button class="th-button filter-button"
            (click)="find.emit($event)"
        >{{title}}</button>

        <frg-data-button class="next-button"
            (clicked)="next.emit($event)"
            [loading]="state === nextState"
            [icon]="'navigate_next'"
        ></frg-data-button>
    `,
    styles: [`
      .th-button {
          font-weight: bold;
      }
    `],
})
export class PagedHeaderComponent {
    @Input()
    public state: PageLoadState;

    @Input()
    public title: PageLoadState;

    @Output()
    public find = new EventEmitter();

    @Output()
    public next = new EventEmitter();

    @Output()
    public previous = new EventEmitter();

    public previousState: PageLoadState = 'LOADING_PREVIOUS';
    public nextState: PageLoadState = 'LOADING_NEXT';
}
