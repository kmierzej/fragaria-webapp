import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component ({
    selector: 'frg-data-button',
    template: `
    <button md-icon-button *ngIf="!(loading)"
                            type="button"
                            (click)="clicked.emit($event)">
        <md-icon class="md-18">{{icon}}</md-icon>
    </button>
    <md-spinner *ngIf="loading"
                class="small-spinner"
                color="accent">
    </md-spinner>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataButtonComponent {
    @Input()
    public loading: boolean;

    @Input()
    public icon: string;

    @Output()
    public clicked = new EventEmitter();
}
