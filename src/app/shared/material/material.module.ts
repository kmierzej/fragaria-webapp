import { NgModule } from '@angular/core';

import {
    MdCardModule,
    MdButtonModule,
    MdIconModule,
    MdInputModule,
    MdMenuModule,
    MdToolbarModule,
    MdSelectModule,
    MdListModule,
    MdCheckboxModule,
    MdDialogModule,
    MdProgressSpinnerModule,
    MdOptionModule,
} from '@angular/material';

const MATERIAL_MODULE = [
    MdCardModule,
    MdButtonModule,
    MdIconModule,
    MdInputModule,
    MdMenuModule,
    MdToolbarModule,
    MdSelectModule,
    MdListModule,
    MdCheckboxModule,
    MdDialogModule,
    MdProgressSpinnerModule,
    MdOptionModule,
];

@NgModule({
    imports: MATERIAL_MODULE,
    exports: MATERIAL_MODULE,
})
export class MaterialModule {}
