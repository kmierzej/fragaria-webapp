import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from 'app/shared/material';

import { EditPropertyComponent } from './edit-property.component';
import { SelectPropertyComponent } from './select-property.component';
import { EditPropertyService } from './edit-property.service';

@NgModule({
    declarations: [
        EditPropertyComponent,
        SelectPropertyComponent,
    ],
    exports: [
        EditPropertyComponent,
        SelectPropertyComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        MaterialModule,
    ],
    providers: [ EditPropertyService ],
})
export class EditPropertyModule {}
