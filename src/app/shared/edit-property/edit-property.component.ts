import { Component, ChangeDetectionStrategy, Input,
         Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { IEditProperty } from './edit-property';

@Component({
    selector: 'frg-edit-property',
    template: `
    <div *ngIf="!isEdit()">
      {{label}}:
      <button md-button (click)="edit = true">
        {{property}}
        <md-spinner *ngIf="property === null" class="small-spinner" mode="indeterminate">
        </md-spinner>
      </button>
    </div>
    <div *ngIf="isEdit()">
      <md-input-container>
        <input mdInput type="{{type}}"
                  placeholder= "{{placeholder()}}"
                  autocomplete="new-password"
                  #propertyInput>
      </md-input-container>
      <button md-button [disabled]="progress" (click)="changeProperty(propertyInput.value)">
        Aktualizuj
        <md-spinner *ngIf="progress" class="small-spinner" mode="indeterminate"></md-spinner>
      </button>
    </div>
    `,
    styles: [`
      .small-spinner {
          height: 25px;
          width: 25px;
          display: inline-block;
          vertical-align: middle;
    }`],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditPropertyComponent implements IEditProperty {
    @Output()
    public propertyChange = new EventEmitter<string>();

    @Input()
    public label: string;

    @Input()
    public property: string = '';

    @Input()
    public edit: boolean = false;

    @Input()
    public type: string = 'text';

    public progress = false;

    constructor( private cdr: ChangeDetectorRef) {}

    public placeholder(): string {
      if (this.property == null || this.property.trim() === '') {
        return this.label;
      }
      return this.property;
    }

    public isEdit(): boolean {
      return (this.property === undefined || (this.property != null && this.property.trim() === ''))
        || this.edit;
    }

    public enable() {
      this.progress = false;
      this.edit = false;
      this.cdr.markForCheck();
    }

    public changeProperty(property: string) {
      if (property === null || property === '') { return; }
      this.progress = true;
      this.propertyChange.emit(property);
    }
}
