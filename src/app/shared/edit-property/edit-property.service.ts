import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpService, USER, ADMIN } from 'app/auth/token-services/http.service';
import { ACCOUNT_RESOURCE, USER_RESOURCE } from 'app/endpoints';
import { IUser } from 'app/model/api';

@Injectable()
export class EditPropertyService {
    constructor(private http: HttpService) { }

    public editUserProperty(login: string, key: string, value: any): Observable<IUser> {
        const body: any = {};
        body[key] = value;
        return this.http.patch(USER_RESOURCE + '/' + login, body, [ADMIN, USER] ).map((it) => it.json());
    }

    public editAccountProperty(key: string, value: any): Observable<IUser> {
        const body: any = {};
        body[key] = value;
        return this.http.patch(ACCOUNT_RESOURCE, body, [USER]).map((it) => it.json());
    }
}
