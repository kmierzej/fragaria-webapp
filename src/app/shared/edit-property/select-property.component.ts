import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { IEditProperty } from './edit-property';

@Component({
    selector: 'frg-select-property',
    template: `
    <div *ngIf="!full" style="margin-bottom:10px;">
        <md-select placeholder="{{placeholder}}" [(ngModel)]="selectedValue">
        <md-option *ngFor="let state of remainingPermissions" [value]="state">
            {{state}}
        </md-option>
        </md-select>
        <button md-button
            (click)="addPrivilege(selectedValue)">
          {{buttonLabel}}
        </button>
    </div>

    <div *ngIf="!empty || progress">
      <span>{{label}}</span>
      <md-spinner *ngIf="progress" class="small-spinner" mode="indeterminate"></md-spinner>
    </div>
    <div *ngIf="!empty">
      <md-list dense>
        <md-list-item *ngFor="let permission of currentPermissions">
          {{permission}}
          <button md-button (click)="removePrivilege(permission)">
            <md-icon>close</md-icon>
          </button>
        </md-list-item>
      </md-list>
    </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectPropertyComponent implements IEditProperty {
    @Output() public change = new EventEmitter<string[]>();
    @Input() public placeholder: string = '';
    @Input() public label: string = '';
    @Input() public buttonLabel: string = '';
    @Input() public allProperties: string[] = [];

    public currentPermissions: string[] = [];
    public remainingPermissions: string[] = this.allProperties;

    public empty: boolean = true;
    public full: boolean = false;
    public selectedValue: string;
    public progress = false;

    @Input()
    set selectedProperties(permissions: string[]) {
      if (permissions == null) {
        this.currentPermissions = [];
        this.remainingPermissions = this.allProperties;
        this.empty = true;
        this.full = false;
      }
      this.currentPermissions = permissions;
      this.remainingPermissions = this.allProperties.filter(
        (elem) => this.currentPermissions.indexOf(elem) === -1,
      );

      this.empty = this.currentPermissions.length === 0;
      this.full = this.currentPermissions.length === this.allProperties.length;
    }

    constructor( private cdr: ChangeDetectorRef) {}

    public enable() {
      this.progress = false;
      this.cdr.markForCheck();
    }

    public removePrivilege(permission: string) {
      const result = this.currentPermissions.filter((x) => x !== permission);
      this.progress = true;
      this.change.emit(result);
    }

    public addPrivilege(permission: string) {
      if (permission === undefined) {
        return;
      }

      const result = this.currentPermissions.concat(permission);
      this.progress = true;
      this.change.emit(result);
    }
}
