import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavigateBeforeButtonComponent } from './navigate-before-button.component';

import { MaterialModule } from 'app/shared/material';

@NgModule({
    declarations: [
        NavigateBeforeButtonComponent,
    ],
    imports: [
        MaterialModule,
        CommonModule,
    ],
    exports: [
        NavigateBeforeButtonComponent,
    ],
})
export class NavigateBeforeButtonModule { }
