import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'frg-navigate-before-button',
    template: `
    <button md-button (click)="goBack()">
      <md-icon>navigate_before</md-icon>
    </button>
    `,
})
export class NavigateBeforeButtonComponent {
    @Input()
    public back: string = '..';

    constructor(
        private router: Router,
        private route: ActivatedRoute,
    ) {}

    public goBack() {
      this.router.navigate([this.back], {relativeTo: this.route});
    }
}
