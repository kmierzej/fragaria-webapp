import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TimeValidatorModule } from 'app/shared/time-validator';
import { LogFormControllerModule } from '../log-form-controller';

import { LogFormComponent } from './log-form.component';
import { ConditionalDivComponent } from './cond-div.component';
import { UserLogService } from './services';

import { MaterialModule } from 'app/shared/material';

@NgModule({
    declarations: [
        ConditionalDivComponent,
        LogFormComponent,
    ],
    exports: [
        LogFormComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,

        TimeValidatorModule,
        LogFormControllerModule,
        MaterialModule,
    ],
    providers: [
        UserLogService,
    ],
})
export class LogFormModule {}
