import {
  Component, Input, Output, ChangeDetectionStrategy,
  ChangeDetectorRef, EventEmitter, OnInit, OnDestroy, ViewChild,
} from '@angular/core';

import { FormGroup } from '@angular/forms';

import { Subscription } from 'rxjs';

import { getErrorMessage2, IDictionaryOfDictionaries } from 'app/shared/paged-dialogs/utils';
import { getTimeString, extractTimeFromString } from 'app/utils/time';
import { LogFormControllerService } from '../log-form-controller';

@Component({
    selector: 'frg-log-form',
    template: `
    <form role="form" #form="ngForm" (ngSubmit)="logTime()">
      <frg-div-wrapper [wrap]="wrap">
        <md-input-container *ngIf="enabledModel.user">
          <input mdInput #user="ngModel" name="user" [(ngModel)]="model.user"
              placeholder="Login" required minlength="3" maxlength="30" >
          <md-error> {{getError(user.errors, 'user')}} </md-error>
        </md-input-container>
      </frg-div-wrapper>

      <frg-div-wrapper [wrap]="wrap">
        <md-input-container *ngIf="enabledModel.date">
          <input mdInput #date="ngModel" name="date" [(ngModel)]="model.date"
           placeholder="Data" pattern="\\d{4}-\\d{2}-\\d{2}" required>
          <md-error> {{getError(date.errors, 'date')}} </md-error>
        </md-input-container>
      </frg-div-wrapper>

      <frg-div-wrapper [wrap]="wrap">
        <md-input-container *ngIf="enabledModel.task">
          <input mdInput #task="ngModel" name="task" [(ngModel)]="model.task"
              placeholder="Zadanie" pattern="\\w+-\\d+" required>
          <md-error> {{getError(task.errors, 'task')}} </md-error>
        </md-input-container>
      </frg-div-wrapper>

      <frg-div-wrapper [wrap]="wrap">
        <md-input-container>
          <input mdInput #time="ngModel" name="time" [(ngModel)]="model.time"
              placeholder="Czas" frgTimeValidator required>
          <md-error> {{getError(time.errors, 'time')}} </md-error>
        </md-input-container>
      </frg-div-wrapper>

      <frg-div-wrapper [wrap]="wrap">
        <button md-button [disabled]="progress || !form.valid" type="submit">Zaloguj czas</button>
      </frg-div-wrapper>
    </form>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogFormComponent implements OnInit, OnDestroy {
    public model = { date: getTimeString(new Date()), task: '', user: '', time: '' };
    public enabledModel = { date: true, task: true, user: true };

    @Input()
    public wrap = false;

    @Input()
    public progress = false;

    @Input()
    set enabled(enabled: any) {
      this.enabledModel = Object.assign(this.enabledModel, enabled);
    }

    @Input()
    set defaults(defaults: any) {
      this.model = Object.assign(this.model, defaults);

      // https://github.com/angular/material2/issues/3005
      setTimeout(() => { this.cdr.markForCheck(); }, 100);
    }

    @Output()
    public newTimeEntry = new EventEmitter();

    @ViewChild('form')
    public form: FormGroup;

    public errorDict = this.createErrorDictionary();

    private subscriptions: Subscription[] = [];

    public constructor(
      private logFormController: LogFormControllerService,
      private cdr: ChangeDetectorRef,
    ) {}

    public logTime() {
      const time = extractTimeFromString(this.model.time);
      const result = Object.assign({}, this.model, {time});

      this.newTimeEntry.emit(result);
    }

    public ngOnInit() {
      this.subscribeToSelectLogService();
    }

    public ngOnDestroy() {
      this.subscriptions.forEach((it) => it.unsubscribe());
    }

    public getError(errors: any, name: string) {
        return getErrorMessage2(errors, this.errorDict[name]);
    }

    private subscribeToSelectLogService() {
      const dateSub = this.logFormController.date.map((date) => date ? getTimeString(date) : null)
      .subscribe((date) => {
        const dateControl = this.form.controls['date'];
        if (date == null) {
          this.model.date = null;
          dateControl.reset();
        } else {
          this.model.date = date;
          dateControl.setValue(date);
        }
        this.cdr.markForCheck();
      });

      const taskSub = this.logFormController.task.subscribe((task) => {
        const control = this.form.controls['task'];
        if (task == null) {
          this.model.task = null;
          control.reset();
        } else {
          this.model.task = task;
          control.setValue(task);
        }
        this.cdr.markForCheck();
      });

      this.subscriptions.push(dateSub, taskSub);
    }

    private createErrorDictionary(): IDictionaryOfDictionaries {
      const date = {
        required: 'Data nie może być pusta',
        pattern: 'YYYY-MM-DD',
       };
      const task = {
        pattern: 'Nieprawidłowe id zadania',
        required: 'Zadanie nie może być puste',
      };
      const time = {
        required: 'Czas nie może być pusty',
        parseBadFormat: 'Niewłaściwy format. Przykładowa poprawna wartość to: 3h 10m',
        parseEmpty: 'Czas nie może być pusty',
        parseOverflow: 'Niewłaściwe wartości minut lub godzin',
      };
      const user = {
        unique: 'Użytkownik o tym loginie już istnieje.',
        minlength: 'Ten login jest za krótki.',
        maxlength: 'Ten login jest za długi.',
      };
      return { user, date, task, time };
    }
}
