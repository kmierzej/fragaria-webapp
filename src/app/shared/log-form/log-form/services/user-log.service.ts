import { Injectable } from '@angular/core';
import { HttpService, USER, ADMIN } from 'app/auth/token-services/http.service';
import { URLSearchParams, Response } from '@angular/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { USER_LOG_RESOURCE, TASK_LOG_RESOURCE } from 'app/endpoints';
import { ITimeEntry } from 'app/model/api';

import { getTimeString } from 'app/utils/time';

import { groupByCompositeArray } from 'app/utils/collections';
import { getTimeArray } from 'app/utils/time';
import { LoggerActions } from 'app/prompt/logger/store';

export interface ICompositeKey {task: string; project: string; user: string; }
export interface ILogResource {
  range: Date[];
  summary: number[];
  values: Array<{key: ICompositeKey, values: number[] }>;
}

@Injectable()
export class UserLogService {
    constructor(private http: HttpService, private store: Store<any>) { }

    public getUserLogs(login: string, from: Date, to: Date): Observable<ILogResource> {
        const params = new URLSearchParams();
        params.append('from', getTimeString(from));
        params.append('to', getTimeString(to));
        const result = this.http.search(USER_LOG_RESOURCE(login), params, [ADMIN, USER])
          .map((it) => it.json() as ITimeEntry[]);
        return result.map((it) => this.transformEntries(it, from, to));
    }

    public createUserLog(entry: ITimeEntry): Observable<Response | void> {
        const body = { user: entry.user, time: entry.time, date: entry.date };
        return this.http.post(TASK_LOG_RESOURCE(entry.task), body, [ADMIN, USER]).catch((error: Response) => {
          // receiving 403 - forbidden and 400 - unknown
          return Observable.of(error.json() as any)
            .map((it) => it.description as string)
            .map((it) => {
               const message = 'Logowanie czasu nieudane - ';
               const action = new LoggerActions.LogMessageAction(message + it);
               this.store.dispatch(action);
            });
       });
    }

    public deleteUserLog(entry: ITimeEntry): Observable<Response> {
        const body = { user: entry.user, date: entry.date };
        return this.http.deleteWithBody(TASK_LOG_RESOURCE(entry.task), body, [ADMIN, USER]);
    }

    public transformEntries(entries: ITimeEntry[], from: Date, to: Date) {
      const range = getTimeArray(from, to);
      const rangeString = range.map((it) => getTimeString(it));

      // 1. group by task + user: so we have dates together!
      const byCompositeKey = groupByCompositeArray(entries, (entry: ITimeEntry) => {
        const last = entry.task.lastIndexOf('-');
        const project = entry.task.substring(0, last);
        return {task: entry.task, user: entry.user, project };
      }, (a: ICompositeKey, b: ICompositeKey) => a.task === b.task && a.user === b.user );

      // 2. purify dates in given range
      const purifiedByComposite = byCompositeKey.map((composite) => {
        const key = composite.key;
        const values = rangeString.map((date) => {
          const result = composite.values.find((it) => it && it.date === date);
          if (result == null) {
            return 0;
          }
          return result.time;
        });
        return {key, values};
      });

      // 3. count summary
      const summary: number[] = range.map(() => 0);
      purifiedByComposite.forEach((entry) => {
        for (let i = 0; i < entry.values.length; i++) {
          summary[i] += entry.values[i];
        }
      });

      return {range, values: purifiedByComposite, summary };
    }
}
