import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LogFormControllerService {
  private dateSource = new Subject<Date>();
  private taskSource = new Subject<string>();

  public date = this.dateSource.asObservable(); // tslint:disable-line
  public task = this.taskSource.asObservable(); // tslint:disable-line

  public selectTask( task: string) {
    this.taskSource.next(task);
  }

  public selectDate( date: Date) {
    this.dateSource.next(date);
  }

  public cleanDate() {
    this.dateSource.next(null);
  }
}
