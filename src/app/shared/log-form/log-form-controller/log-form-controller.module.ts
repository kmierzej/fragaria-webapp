import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogFormControllerService } from './log-form-controller.service';

@NgModule({
    imports: [
        CommonModule,
    ],
    providers: [
        LogFormControllerService,
    ],
})
export class LogFormControllerModule {}
