import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'app/shared/material';

import {
    AddAssigneesComponent,
    UserListComponent,
    DelayedInputComponent,
    ExpandWrapperComponent,
    SelectManagerComponent,

    AssigneeListComponent,
    AssigneeDetailsComponent,
 } from './components';

import { PagedResourceDialogsModule } from 'app/shared/paged-dialogs';

@NgModule({
    declarations: [
        DelayedInputComponent,
        ExpandWrapperComponent,
        UserListComponent,
        AssigneeListComponent,
        AssigneeDetailsComponent,

        SelectManagerComponent,
        AddAssigneesComponent,
    ],
    exports: [
        AssigneeDetailsComponent,
        AssigneeListComponent,
        AddAssigneesComponent,
        ExpandWrapperComponent,
        SelectManagerComponent,
        UserListComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule,
        PagedResourceDialogsModule,
        FormsModule,
    ],
})
export class SelectAssigneesModule {}
