import { Component, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';

import { IProject, IUser } from 'app/model/api';
import { AssigneesService, SearchUsersService, ProjectsService } from 'app/model/api-services';

@Component({
    selector: 'frg-add-assignees',
    template: `
    <div>
      <frg-expand-wrapper [label]="'Dodaj Pracownika'" [expand]="expandAddUser" (expandEvent)="toggleAddUser()">
        <frg-delayed-input [progress]="querriedAssigneesProgress" [label]="'Szukaj pracownika:'"
          (typed)="queryAssigned($event)" (toggle)="toggleAddUser()">
        </frg-delayed-input>

        <frg-user-list [users]="users" (addUser)="addUser($event)">
        </frg-user-list>
      </frg-expand-wrapper>
    </div>

    <frg-expand-wrapper [label]="'Dodaj Pracowników z Projektu'"
        [expand]="expandAddUsers" (expandEvent)="toggleAddUsers()">
      <frg-delayed-input [progress]="querriedProjectProgress" [label]="'Szukaj projektu:'"
        (typed)="queryProject($event)" (toggle)="toggleAddUsers()">
      </frg-delayed-input>

      <frg-user-list [users]="projects" [icon]="'folder'" (addUser)="addProject($event)">
      </frg-user-list>
    </frg-expand-wrapper>
    `,
})
export class AddAssigneesComponent {
  @Input()
  public users: IUser[] = [];

  @Output()
  public qUsers: EventEmitter<Array<{display: string, value: IUser}>> = new EventEmitter();

  @Output()
  public aUsers: EventEmitter<Array<{display: string, value: IUser}>> = new EventEmitter();

  public projects: Array<{display: string, value: IProject}> = [];

  public querriedAssigneesProgress: string = 'hidden';
  public querriedProjectProgress: string = 'hidden';

  public expandAddUser: boolean = false;
  public expandAddUsers: boolean = false;

  constructor(
    private searchUsersService: SearchUsersService,
    private projectsService: ProjectsService,
    private assigneeService: AssigneesService,
    private cdr: ChangeDetectorRef,
  ) {}

  public toggleAddUser() {
    this.expandAddUser = !this.expandAddUser;
  }

  public toggleAddUsers() {
    this.expandAddUsers = !this.expandAddUsers;
  }

  public addUser(user: {display: string, value: any}) {
    this.aUsers.emit([user]);
  }

  public addProject(project: {display: string, value: IProject}) {
    this.assigneeService.getAssignees(project.value.id).subscribe((users) => {
      this.aUsers.emit(users.map((it) => ({display: it.login, value: it})));
      this.cdr.markForCheck();
    });
  }

  public queryAssigned(event: string) {
    this.querriedAssigneesProgress = 'visible';
    return this.searchUsersService.findUserByName(event).subscribe((users) => {
      this.querriedAssigneesProgress = 'hidden';
      const assignees = users.map((it) => ({display: it.login, value: it}));
      this.qUsers.emit(assignees);
      this.cdr.markForCheck();
    }, () => { this.querriedAssigneesProgress = 'hidden'; });
  }

  public queryProject(projectName: string) {
    this.querriedProjectProgress = 'visible';
    this.projectsService.findProjectsByName(projectName).subscribe((it) => {
        this.querriedProjectProgress = 'hidden';
        this.projects = it.map((project) => ({display: project.id + ' - ' + project.title, value: project}));
        this.cdr.markForCheck();
    });
  }
}
