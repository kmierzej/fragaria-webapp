import { Component, Input, Output, ChangeDetectorRef, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { SearchUsersService } from 'app/model/api-services';

@Component({
    selector: 'frg-select-manager',
    template: `
    <frg-expand-wrapper [buttonLabel]="'Manager:'" [label]="manager"
                        [expand]="expandEditManager || !manager"
                        (expandEvent)="showAddManager()">
      <frg-delayed-input
          [progress]="querriedManagerProgress"
          [label]="'Wybierz managera:'"
          [hideHide]="hideHide()"
          (typed)="queryManager($event)"
          (toggle)="hideAddManager()">
      </frg-delayed-input>

      <frg-user-list *ngIf="expandEditManager"
          [users]="querriedManager"
          (addUser)="emitSelectManager($event)">
      </frg-user-list>
    </frg-expand-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectManagerComponent {
  @Input()
  public projectId: string;

  @Input()
  public manager: string;

  @Output()
  public selectManager = new EventEmitter();

  public querriedManager: Array<{display: string, value: any}> = [];
  public querriedManagerProgress: string = 'hidden';

  public expandEditManager: boolean = false;

  constructor(
    private searchUsersService: SearchUsersService,
    private cdr: ChangeDetectorRef,
  ) {}

  public hideHide() {
    return this.manager == null && (this.querriedManager.length === 0 || !this.expandEditManager );
  }

  public queryManager(event: string) {
    this.expandEditManager = true;
    this.querriedManagerProgress = 'visible';
    return this.searchUsersService.findUserByName(event).subscribe((users) => {
      this.querriedManagerProgress = 'hidden';
      this.querriedManager = users.map((it) => ({display: it.login, value: it}));
      this.cdr.markForCheck();
    }, () => { this.querriedManagerProgress = 'hidden'; });
  }

  public showAddManager() {
    this.expandEditManager = true;
  }

  public hideAddManager() {
    this.expandEditManager = false;
  }

  public emitSelectManager(manager: {display: string, value: any}) {
    this.expandEditManager = false;
    this.selectManager.emit(manager);
  }
}
