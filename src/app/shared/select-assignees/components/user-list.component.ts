import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'frg-user-list',
    template: `
    <div *ngIf="isEmpty()">
      <md-divider></md-divider>
      <md-list>
        <md-list-item *ngFor="let user of users">
          <md-icon md-list-icon>{{icon}}</md-icon>
          <button md-button type="button" style="text-align:left" (click)="addUserFn(user)">
           {{user.display}}
          </button>
        </md-list-item>
      </md-list>
      <md-divider></md-divider>
    </div>
    `,
})
export class UserListComponent {
    @Input()
    public users: Array<{display: string, value: any}>;

    @Input()
    public icon = 'account_circle';

    @Output()
    public addUser = new EventEmitter<string>();

    public addUserFn(user: string) {
        this.addUser.emit(user);
    }

    public isEmpty() {
        return this.users.length > 0;
    }
}
