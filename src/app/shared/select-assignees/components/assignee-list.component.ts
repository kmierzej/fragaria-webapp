import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component ({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-assignee-list',
    template: `
      <frg-assignee-details *ngFor="let assignee of assignees"
                            [assignee]="assignee"
                            [canModify]="canModify"
                            (delete)="deleteAssignee($event)" >
      </frg-assignee-details>
    `,
})
export class AssigneeListComponent {
  @Input()
  public assignees: Array<{display: string, value: any}> = [];

  @Output()
  public delete: EventEmitter<string> = new EventEmitter();
  public canModify: boolean = true;

  public deleteAssignee(assignee: string) {
    this.delete.emit(assignee);
  }
}
