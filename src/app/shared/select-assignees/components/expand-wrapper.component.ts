import { Component, Output, Input, EventEmitter } from '@angular/core';

@Component({
    selector: 'frg-expand-wrapper',
    template: `
    <ng-container *ngIf="expand; else addUserButtonTemplate">
      <ng-content>
      </ng-content>
    </ng-container>
    <ng-template #addUserButtonTemplate>
      {{buttonLabel}}
      <button md-button (click)="expandEventFn()">
        {{label}}
      </button>
    </ng-template>
    `,
})
export class ExpandWrapperComponent {
  @Input()
  public expand = false;

  @Input()
  public label = '';

  @Input()
  public buttonLabel = '';

  @Output()
  public expandEvent = new EventEmitter();

  public expandEventFn() {
    this.expandEvent.emit();
  }
}
