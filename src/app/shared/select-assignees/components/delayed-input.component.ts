import { Component, Input, Output, OnInit, OnDestroy, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
    selector: 'frg-delayed-input',
    template: `
    {{label}}
    <form #f="ngForm" class="nowrap">
      <md-input-container>
        <input type="text" mdInput (ngModelChange)="modelChange($event)"
          name="model" [ngModel]="intModel" #model minlength="3">
        <md-error *ngIf="!model.valid">Nazwa jest za krótka - minimum 3 znaki </md-error>
      </md-input-container>
      <md-spinner [style.visibility]="progress"
                  class="small-spinner"
                  mode="indeterminate">
      </md-spinner>
      <button md-button *ngIf="!hideHide" type="button" (click)="toggleAddUser()">
        <md-icon>expand_less</md-icon>
      </button>
    </form>
    `,
    styles: [`
      .small-spinner {
          height: 25px;
          width: 25px;
          display: inline-block;
          vertical-align: middle;
      }
    `],
})
export class DelayedInputComponent implements OnInit, OnDestroy {
  public intModel = '';

  @Output()
  public typed = new EventEmitter<string>();

  @Output()
  public toggle = new EventEmitter();

  @Input()
  public progress = 'hidden';

  @Input()
  public hideHide: boolean = false;

  @Input()
  public label = 'label';

  private subject: BehaviorSubject<string>;
  private sub: Subscription[] = [];

  constructor( private cdr: ChangeDetectorRef) { }

  public ngOnInit() {
      this.subject = new BehaviorSubject<string>('');
      const observable =  this.subject.asObservable()
        .debounceTime(400)
        .filter((it) => it.length > 2)
        .distinctUntilChanged();

      const sub = observable.subscribe((it) => {
          this.typed.emit(it);
          this.cdr.markForCheck();
      });

      this.sub.push(sub);
  }

  public ngOnDestroy() {
      this.sub.forEach((it) => it.unsubscribe());
  }

  public toggleAddUser() {
      this.toggle.emit(null);
  }

  public modelChange(event: string) {
    this.subject.next(event);
  }
}
