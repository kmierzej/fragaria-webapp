import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { IUser } from 'app/model/api';

@Component ({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-assignee-details',
    template: `
    <button md-button type="button" (click)="buttonClick()">{{assignee.value.login}}
      <md-icon *ngIf="canModify">close</md-icon>
    </button>
    `,
})
export class AssigneeDetailsComponent {
  @Input()
  public assignee: {display: string, value: IUser};

  @Input()
  public canModify: boolean;

  @Output()
  public delete = new EventEmitter();

  public buttonClick() {
    if (this.canModify) {
      this.delete.emit(this.assignee);
    }
  }
}
