import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavigateBeforeComponent } from './components';

import { MaterialModule } from 'app/shared/material';
import { NavigateBeforeButtonModule } from 'app/shared/navigateBefore/navigate-before-button.module';

@NgModule({
    declarations: [
        NavigateBeforeComponent,
    ],
    imports: [
        NavigateBeforeButtonModule,
        MaterialModule,
        CommonModule,
    ],
    exports: [
        NavigateBeforeComponent,
    ],
})
export class ProjectsCommonModule { }
