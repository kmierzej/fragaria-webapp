import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'frg-navigate-before',
    template: `
    <frg-navigate-before-button [back]="back"></frg-navigate-before-button>
    {{label}}

    <ng-container *ngIf="editVisible">
      <button md-button *ngIf="editEnabled; else editTemplate" (click)="goToEdit()">
        <md-icon>edit</md-icon>
      </button>
      <ng-template #editTemplate>
        <span mdTooltip="Rozpocznij sesję administratora by edytować ustawienia.">
        <button md-button [disabled]="true" (click)="goToEdit()">
          <md-icon>edit</md-icon>
        </button>
        </span>
      </ng-template>
    </ng-container>
    `,
})
export class NavigateBeforeComponent {
    @Input()
    public label: string;

    @Input()
    public back: string = '..';

    @Input()
    public editVisible: boolean = true;

    @Input()
    public editEnabled: boolean = false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
    ) {}

    public goBack() {
      this.router.navigate([this.back], {relativeTo: this.route});
    }

    public goToEdit() {
      this.router.navigate(['edit'], {relativeTo: this.route});
    }
}
