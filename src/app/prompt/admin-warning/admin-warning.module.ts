import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'app/shared/material';

import { AdminWarningComponent } from './admin-warning.component';
import { DocumentClickDirective } from './document-click.directive';
import { TimerComponent } from './timer.component';
import { CountdownPipe } from './countdown.pipe';

@NgModule({
    declarations: [
        AdminWarningComponent,
        DocumentClickDirective,
        TimerComponent,
        CountdownPipe,
    ],
    exports: [
        AdminWarningComponent,
    ],
    imports: [
        MaterialModule,
        CommonModule,
    ],
})
export class AdminWarningModule {}
