import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'frg-timer',
    template: `
      {{ time | countdown }}
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimerComponent {
    public time: number;

    @Input()
    public set countdown(countdown: number) {
      if (countdown <= 0 ) {
          this.timeout.emit(null);
      } else {
          this.time = countdown;
      }
    }

    @Output()
    public timeout = new EventEmitter();
}
