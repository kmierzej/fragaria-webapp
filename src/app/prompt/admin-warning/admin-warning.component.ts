import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { TokenActions } from 'app/auth/token-services/store';

import { TimerService } from './timer-service';

@Component({
    selector: 'frg-admin-warning',
    template: `
    <md-card style="align-items:center; display:flex;">
        <span frgDocumentClick (documentClick)="documentClick()">
            Trwa sesja z uprawnieniami Administratora:&nbsp;
        </span>
        <frg-timer [countdown]="timeDiff | async"
               (timeout)="dropAdmin()">
        </frg-timer>
        <span style="flex:1 1 auto"></span>
        <button md-raised-button id="admin-warning-logout"
            (click)='dropAdmin()'
            color="accent">
            Zmniejsz poziom uprawnień
        </button>
    </md-card>
    `,
    providers: [ TimerService ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminWarningComponent implements OnInit {
    public timeDiff: Observable<number>;
    private expirationSubject = new BehaviorSubject(getMinutesInFuture(30));

    constructor(
        private store: Store<any>,
        private timerService: TimerService,
    ) {}

    public ngOnInit() {
        this.timeDiff = this.timerService.getTimeout(this.expirationSubject);
    }

    public dropAdmin() {
        this.store.dispatch(new TokenActions.LogoutAdminAction());
    }

    public documentClick() {
        this.expirationSubject.next(getMinutesInFuture(30));
    }
}

function getMinutesInFuture(minutes: number) {
    const date = new Date();
    return Math.floor(date.getTime() / 1000) + minutes * 60;
}
