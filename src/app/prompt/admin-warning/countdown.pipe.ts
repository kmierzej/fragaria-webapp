import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'countdown' })
export class CountdownPipe implements PipeTransform {
    public transform(countdown: number): string {
      const minutes = Math.floor(countdown / 60);
      const seconds = countdown % 60;
      const result =  minutes + ':' + ((seconds < 10) ? '0' : '') + seconds;
      return result;
    }
}
