import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TimerService {
    public getTimeout(countDownTo: Observable<number>) {
        return countDownTo.switchMap((countdown) => {
            return this.getTimer().map((now) => countdown - now);
        });
    }

    private getTimer(): Observable<number> {
        return Observable.interval(1000)
          .startWith(0)
          .map( () => Math.floor(new Date().getTime() / 1000));
    }
}
