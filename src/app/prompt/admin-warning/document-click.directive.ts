import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
    selector: '[frgDocumentClick]',
})
export class DocumentClickDirective {
    @Output()
    public documentClick = new EventEmitter();

    @HostListener('document:click', ['$event.target'])
    public onClick(_: any) {
        this.documentClick.emit(null);
    }
}
