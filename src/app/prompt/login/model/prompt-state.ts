export interface IPromptState {
    show: boolean;
    error: string;
}
