/* tslint:disable:max-classes-per-file */
import { Action } from '@ngrx/store';
import { type } from 'app/util';

import { UserCredentials } from '../model';

export const ActionTypes = { // tslint:disable-line:variable-name
    HIDE_ADMIN_LOGIN:   type('[LoginPrompt] Hide admin login'),
    HIDE_USER_LOGIN:    type('[LoginPrompt] Hide user login'),
    LOGIN_ADMIN:        type('[LoginPrompt] Login Admin'),
    LOGIN_ADMIN_FAILURE: type('[LoginPrompt] Admin Login Failure'),
    LOGIN_USER:         type('[LoginPrompt] Login User'),
    LOGIN_USER_FAILURE: type('[LoginPrompt] User Login Failure'),
    SHOW_ADMIN_LOGIN:   type('[LoginPrompt] Show admin login'),
    SHOW_USER_LOGIN:    type('[LoginPrompt] Show user login'),
};

export class HideUserLoginAction implements Action {
    public type = ActionTypes.HIDE_USER_LOGIN;
}

export class HideAdminLoginAction implements Action {
    public type = ActionTypes.HIDE_ADMIN_LOGIN;
}

export class LoginAdminAction implements Action {
    public type = ActionTypes.LOGIN_ADMIN;

    constructor(public payload: string) {}
}

export class LoginAdminFailureAction implements Action {
    public type = ActionTypes.LOGIN_ADMIN_FAILURE;

    constructor(public payload: string) {}
}

export class LoginUserAction implements Action {
    public type = ActionTypes.LOGIN_USER;

    constructor(public payload: UserCredentials) {}
}

export class LoginUserFailureAction implements Action {
    public type = ActionTypes.LOGIN_USER_FAILURE;

    constructor(public payload: string) {}
}

export class ShowAdminLoginAction implements Action {
    public type = ActionTypes.SHOW_ADMIN_LOGIN;
}

export class ShowUserLoginAction implements Action {
    public type = ActionTypes.SHOW_USER_LOGIN;
}

export type Actions =
    | HideAdminLoginAction
    | HideUserLoginAction
    | LoginAdminAction
    | LoginAdminFailureAction
    | LoginUserAction
    | LoginUserFailureAction
    | ShowAdminLoginAction
    | ShowUserLoginAction
    ;
