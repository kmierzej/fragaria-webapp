import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { TokenActions } from 'app/auth/token-services/store';
import { LoginActions } from './';

import { AuthStore } from 'app/auth/token-services/auth.store';
import { getLogin, getRT } from 'app/auth/token-services/auth.reducers';
import { TokenService } from 'app/auth/token-services/services/token.service';

@Injectable()
export class LoginEffects {
    @Effect()
    public loginAdmin: Observable<Action> = this.actions
        .ofType(LoginActions.ActionTypes.LOGIN_ADMIN)
        .debounceTime(300)
        .map((action: LoginActions.LoginAdminAction) => action.payload)
        .map((password) => ({login: this.authService.peek(getLogin), password}))
        .switchMap((credentials) =>
            this.loginService.login(credentials.login, credentials.password, ['ADMIN'])
                .do((token: any) => this.authService.registerTokens(token))
                .mapTo(new LoginActions.HideAdminLoginAction())
                .catch( (res: Response) => Observable.of(res)
                    .map((response) => this.getFailedLoginMessage(response))
                    .map((message) => new LoginActions.LoginAdminFailureAction(message))),
        );

    @Effect()
    public loginUser: Observable<Action> = this.actions
        .ofType(LoginActions.ActionTypes.LOGIN_USER)
        .debounceTime(300)
        .map((action: LoginActions.LoginUserAction) => action.payload)
        .switchMap((credentials) =>
            this.loginService.login(credentials.login, credentials.password)
                .do((token) => this.authService.registerTokens(token))
                .mapTo(new LoginActions.HideUserLoginAction())
                .catch( (res: Response) => Observable.of(res)
                    .map((response) => this.getFailedLoginMessage(response))
                    .map((message) => new LoginActions.LoginUserFailureAction(message))),
        );

    // TODO: Check all permissions with server on each app reload.
    // TODO: Also refresh token on page load would be nice (if old enough);
    // TODO: Logout with server should be created

    @Effect()
    public logoutUser: Observable<Action> = this.actions
        .ofType(TokenActions.ActionTypes.LOGOUT_USER)
        .debounceTime(300)
        .map(() => this.authService.peek(getRT('USER')))
        .do(() => this.authService.logout())
        .flatMap((token) => this.loginService.logout(token).catch(() => Observable.empty()))
        .flatMap(() => Observable.empty());

    @Effect()
    public logoutAdmin: Observable<Action> = this.actions
        .ofType(TokenActions.ActionTypes.LOGOUT_ADMIN)
        .debounceTime(300)
        .map(() => this.authService.peek(getRT('ADMIN')))
        .do(() => this.authService.logoutPermission('ADMIN'))
        .flatMap((token) => this.loginService.logout(token).catch(() => Observable.empty()))
        .flatMap(() => Observable.empty());

    constructor(
        private actions: Actions,
        private loginService: TokenService,
        private authService: AuthStore,
    ) {}

    private getFailedLoginMessage(res: Response): string {
        const payload = res.json().description
            || ('Niezrozumiała odpowiedź serwera. Powiadom administratora serwisu.');
        if (res.status === 400) {
            return payload;
        }
        return 'Błąd serwera. Spróbuj ponownie.';
    }
}
