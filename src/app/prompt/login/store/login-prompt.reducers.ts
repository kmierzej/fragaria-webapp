import { LoginActions } from './';

import { IPromptState } from '../model';

export interface IState {
    showAdmin: boolean;
    showUser: boolean;
    errorAdmin: string;
    errorUser: string;
}

const initialState: IState = {
    errorAdmin: '',
    errorUser: '',
    showAdmin: false,
    showUser: false,
};

export function reducer(state = initialState, action: LoginActions.Actions): IState {
    switch (action.type) {
        case LoginActions.ActionTypes.SHOW_USER_LOGIN: {
            return {
                ...state,
                showUser: true,
            };
        } case LoginActions.ActionTypes.HIDE_USER_LOGIN: {
            return {
                ...state,
                errorUser: '',
                showUser: false,
            };
        } case LoginActions.ActionTypes.LOGIN_USER_FAILURE: {
            const error = (action as LoginActions.LoginUserFailureAction).payload;
            return {
                ...state,
                errorUser: error,
            };
        } case LoginActions.ActionTypes.LOGIN_ADMIN_FAILURE: {
            const error = (action as LoginActions.LoginAdminFailureAction).payload;
            return {
                ...state,
                errorAdmin: error,
            };
        } case LoginActions.ActionTypes.SHOW_ADMIN_LOGIN: {
            return {
                ...state,
                showAdmin: true,
            };
        } case LoginActions.ActionTypes.HIDE_ADMIN_LOGIN: {
            return {
                ...state,
                errorAdmin: '',
                showAdmin: false,
            };
        } default: {
            return state;
        }
    }
}

export const getUserPromptState: (_: IState) => IPromptState =
    (state: IState) => ({ show: state.showUser, error: state.errorUser });
export const getAdminPromptState: (_: IState) => IPromptState =
    (state: IState) => ({ show: state.showAdmin, error: state.errorAdmin });
