import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';

import { MaterialModule } from 'app/shared/material';

import {
  LoginAdminPromptComponent,
  LoginComponent,
  LoginUserPromptComponent,
} from './components';

import { LoginService } from './services';
import { LoginEffects } from './store';

@NgModule({
    declarations: [
        LoginComponent,
        LoginAdminPromptComponent,
        LoginUserPromptComponent,
    ],
    exports: [
        LoginComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule,
        EffectsModule.runAfterBootstrap(LoginEffects),
    ],
    providers: [
        LoginService,
    ],
})
export class LoginModule {}
