import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { IPromptState, UserCredentials } from '../model';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-login-user-prompt',
    template: `
    <md-card *ngIf="state.show">
    <div style="align-items:center; display:flex;">
        <span style="flex:1 1 auto"></span>
    <form role="form" (submit)="emitLogin($event, username.value, password.value)">
        <md-input-container>
        <input mdInput type="text" #username class="form-control" id="username" placeholder="Login">
        <md-hint [ngStyle]="{'color': 'red'}" align="start">{{state.error}}</md-hint>
        </md-input-container>
        <md-input-container>
        <input mdInput type="password" #password class="form-control" id="password" placeholder="Hasło">
        </md-input-container>
        <button md-raised-button color="accent" type="submit">Zaloguj się</button>
    </form>
        <button md-button (click)="emitHide()">
        <md-icon style="vertical-align:middle">close</md-icon>
        </button>
    </div>
    </md-card>
    `,
})
export class LoginUserPromptComponent {
    @Input()
    public state: IPromptState;

    @Output()
    public hide = new EventEmitter();
    @Output()
    public login = new EventEmitter();

    public emitLogin(event: Event, username: string, password: string) {
        event.preventDefault();
        const credentials = new UserCredentials(username, password);
        this.login.emit(credentials);
    }

    public emitHide() {
        this.hide.emit();
    }
}
