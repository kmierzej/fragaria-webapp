import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { IPromptState } from '../model';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-login-admin-prompt',
    template: `
<md-card *ngIf="state.show">
  <div style="align-items:center; display:flex;">
    <span style="flex:1 1 auto"></span>
  <form role="form" (submit)="emitLogin($event, password.value)">
    <md-input-container>
      <input mdInput type="password" id="admin-password"
              #password class="form-control"
              placeholder="Hasło">
      <md-hint [ngStyle]="{'color': 'red'}" align="start">{{state.error}}</md-hint>
    </md-input-container>
    <button md-raised-button id="admin-login" color="warn" type="submit">
      Sesja Administratora
    </button>
  </form>
    <button md-button id="admin-hide" (click)="hide.emit()">
      <md-icon style="vertical-align:middle">close</md-icon>
    </button>
  </div>
</md-card>
    `,
})
export class LoginAdminPromptComponent {
    @Input()
    public state: IPromptState;

    @Output()
    public hide = new EventEmitter();
    @Output()
    public login = new EventEmitter();

    public emitLogin(event: Event, password: string) {
      event.preventDefault();
      this.login.emit(password);
    }
}
