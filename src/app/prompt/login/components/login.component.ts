import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { UserCredentials } from '../model';
import { IPromptState } from '../model';

import { LoginActions, LoginReducers } from 'app/prompt/login/store';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-login',
    template: `
    <frg-login-user-prompt
      [state]="userState"
      (hide)="hideUser()"
      (login)="loginUser($event)">
    </frg-login-user-prompt>
    <frg-login-admin-prompt
      [state]="adminState"
      (hide)="hideAdmin()"
      (login)="loginAdmin($event)">
    </frg-login-admin-prompt>
    `,
})
export class LoginComponent {

    public adminState: IPromptState;
    public userState: IPromptState;

    @Input()
    set state(state: LoginReducers.IState) {
        this.adminState = { show: state.showAdmin, error: state.errorAdmin };
        this.userState = {show: state.showUser, error: state.errorUser };
    }

    constructor(private store: Store<any>) {}

    public loginUser(credentials: UserCredentials) {
        this.store.dispatch(new LoginActions.LoginUserAction(credentials));
    }

    public loginAdmin(password: string) {
        this.store.dispatch(new LoginActions.LoginAdminAction(password));
    }

    public hideAdmin() {
        this.store.dispatch(new LoginActions.HideAdminLoginAction());
    }

    public hideUser() {
        this.store.dispatch(new LoginActions.HideUserLoginAction());
    }
}
