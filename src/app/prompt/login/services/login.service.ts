import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ADMIN_LOGIN, USER_LOGIN} from 'app/endpoints';
import { UserCredentials } from '../model';

@Injectable()
export class LoginService {
    constructor(private http: Http) {}

    public loginUser(credentials: UserCredentials): Observable<string> {
        return this.performLogin(USER_LOGIN, credentials);
    }

    public loginAdmin(credentials: UserCredentials): Observable<string> {
        return this.performLogin(ADMIN_LOGIN, credentials);
    }

    private performLogin(url: string, credentials: UserCredentials): Observable<string> {
        return this.http.post(url, credentials).map((res) => res.json());
    }
}
