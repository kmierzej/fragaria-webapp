import { NgModule } from '@angular/core';

import { AdminWarningComponent, AdminWarningModule } from './admin-warning';
import { LoggerComponent, LoggerModule } from './logger';
import { LoginComponent, LoginModule } from './login';
import { NavbarComponent, NavbarModule } from './navbar';

@NgModule({
    exports: [
        AdminWarningComponent,
        LoginComponent,
        LoggerComponent,
        NavbarComponent,
    ],
    imports: [
        AdminWarningModule,
        LoginModule,
        LoggerModule,
        NavbarModule,
    ],
})
export class PromptModule { }
