import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MaterialModule } from 'app/shared/material';

import { LoginButtonComponent, MenuButtonComponent, NavbarComponent } from './components';
import { TabService } from './services';

@NgModule({
    declarations: [
        MenuButtonComponent,
        NavbarComponent,
        LoginButtonComponent,
    ],
    exports: [
        NavbarComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule,
    ],
    providers: [
        TabService,
    ],
})
export class NavbarModule {}
