import { Component, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-login-button',
    template: `
    <button md-button id="login-button"
            (mouseleave) = 'lock()'
            (mouseenter) = 'unlock()'
            (click) ='loginEmit()'>
      <md-icon class="app-toolbar-menu" style="vertical-align:middle">{{lockIcon}}</md-icon>
      <span>Logowanie</span>
    </button>
    `,
})
export class LoginButtonComponent {
    @Output()
    public login = new EventEmitter();

    public lockIcon: string = 'lock';

    public lock() {
        this.lockIcon = 'lock';
    }

    public unlock() {
        this.lockIcon = 'lock_open';
    }

    public loginEmit() {
        this.login.emit();
    }
}
