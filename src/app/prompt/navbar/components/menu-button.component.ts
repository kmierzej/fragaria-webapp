import { Component, EventEmitter, Input, Output, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { NavTab } from '../model';
import { TabService } from '../services';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-menu-button',
    template: `
    <button md-button [md-menu-trigger-for]="menu" id="menu-button">
      <md-icon>account_circle</md-icon>
      <span id="menu-login">{{username}}</span>
    </button>
    <md-menu x-position="before" #menu="mdMenu">
      <button md-menu-item *ngFor="let tab of usertabs"
                           (click)="navigate.emit(tab.link)">{{tab.text}}</button>
      <div *ngIf="canBeAdmin">
        <button md-menu-item *ngIf="!isadmin"
                             (click)="loginadmin.emit()">Sesja Administratora</button>
        <button md-menu-item *ngFor="let tab of admintabs"
                             (click)="navigate.emit(tab.link)"
                             [disabled]="!isadmin">{{tab.text}}</button>
      </div>
      <button md-menu-item (click)="logout.emit()">Wyloguj się</button>
    </md-menu>
    `,
})
export class MenuButtonComponent implements OnInit {
    @Input()
    public username: string;

    @Input()
    public isadmin: boolean;
    @Input()
    public canBeAdmin: boolean;

    @Output()
    public logout = new EventEmitter();
    @Output()
    public loginadmin = new EventEmitter();
    @Output()
    public navigate = new EventEmitter();

    public admintabs: NavTab[];
    public usertabs: NavTab[];

    constructor(private tabService: TabService) {}

    public ngOnInit() {
      this.admintabs = this.tabService.getAdminTabs();
      this.usertabs = this.tabService.getUserTabs();
    }
}
