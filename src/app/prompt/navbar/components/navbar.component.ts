import { Component, Output, EventEmitter, ChangeDetectionStrategy, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { TokenActions } from 'app/auth/token-services/store';
import { LoginActions } from 'app/prompt/login/store';

interface INavbarState {
  login: string;
  isUser: boolean;
  isAdmin: boolean;
  canBeAdmin: boolean;
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-navbar',
    template: `
    <md-toolbar color="primary">
      <span id="app-title">Fragaria</span>

      <span class="app-toolbar-filler"></span>
      <frg-menu-button *ngIf="state.isUser; else content"
                   [username]="state.login"
                   [isadmin]="state.isAdmin"
                   [canBeAdmin]="state.canBeAdmin"
                   (logout)="logout()"
                   (loginadmin)="loginAdmin()"
                   (navigate)="navigate.emit($event)">
      </frg-menu-button>
      <ng-template #content>
        <frg-login-button (login)="login()">
        </frg-login-button>
      </ng-template>
    </md-toolbar>
    `,
    styles: [`
      .app-toolbar-filler {
          flex: 1 1 auto;
      }
      .app-sidenav {
        padding: 10px;
        min-width: 100px;
      }
      .app-toolbar-menu {
        padding: 0 14px 0 14px;
        color: white;
      }
      .app-icon-button {
        box-shadow: none;
        user-select: none;
        background: none;
        border: none;
        cursor: pointer;
        filter: none;
        font-weight: normal;
        height: auto;
        line-height: inherit;
        margin: 0;
        min-width: 0;
        padding: 0;
        text-align: left;
        text-decoration: none;
      }
    `],
})
export class NavbarComponent {
    @Input()
    public state: INavbarState;

    @Output()
    public navigate = new EventEmitter();

    constructor( private store: Store<any> ) {}

    public login() {
        this.store.dispatch(new LoginActions.ShowUserLoginAction());
    }

    public logout() {
        this.store.dispatch(new TokenActions.LogoutUserAction());
    }

    public loginAdmin() {
        this.store.dispatch(new LoginActions.ShowAdminLoginAction());
    }
}
