import { Injectable } from '@angular/core';
import { NavTab } from '../model';

export const USER_TABS = [
    new NavTab('Dashboard', '/dashboard'),
    new NavTab('Projekty', '/projects'),
    new NavTab('Ustawienia', '/user/settings'),
];

export const ADMIN_TABS = [
    new NavTab('Zarządzaj użytkownikami', '/admin/users?permission=ADMIN&permission=USER'),
    new NavTab('Raport', '/report'),
];

@Injectable()
export class TabService {
    public getAdminTabs() {
        return ADMIN_TABS;
    }

    public getUserTabs() {
        return USER_TABS;
    }
}
