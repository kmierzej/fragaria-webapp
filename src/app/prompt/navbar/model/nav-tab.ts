export class NavTab {
    constructor(public text: string,
                public link: string,
                public disabled: boolean = false) {}

    public toggle(): NavTab {
        return new NavTab(this.text, this.link, !this.disabled);
    }
}
