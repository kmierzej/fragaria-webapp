import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MaterialModule } from 'app/shared/material';

import { LoggerComponent, MessageEntryComponent } from './components';

@NgModule({
    declarations: [
        LoggerComponent,
        MessageEntryComponent,
    ],
    exports: [
        LoggerComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule,
    ],
})
export class LoggerModule {}
