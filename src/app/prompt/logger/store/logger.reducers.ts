import { Message } from '../model';
import { LoggerActions } from './';

export interface IState {
    counter: number;
    message: Message[];
}

const initialState: IState = {
    counter: 0,
    message: [],
};

export function reducer(state = initialState, action: LoggerActions.Actions): IState {
    switch (action.type) {
        case LoggerActions.ActionTypes.LOG_MESSAGE: {
            const message = (action as LoggerActions.LogMessageAction).payload;
            return {
                counter: state.counter + 1,
                message: state.message.concat(new Message(state.counter + 1, message, 'warn')),
            };
        } case LoggerActions.ActionTypes.LOG_NEUTRAL_MESSAGE: {
            const message = (action as LoggerActions.LogNeutralMessageAction).payload;
            return {
                counter: state.counter + 1,
                message: state.message.concat(new Message(state.counter + 1, message, '')),
            };
        } case LoggerActions.ActionTypes.CLOSE_MESSAGE: {
            const id = (action as LoggerActions.CloseMessageAction).payload;
            return {
                ...state,
                message: state.message.filter((x) => x.id !== id),
            };
        } default: {
            return state;
        }
    }
}

export const getMessages = (state: IState) => state.message;
