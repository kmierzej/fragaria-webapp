/* tslint:disable:max-classes-per-file */
import { Action } from '@ngrx/store';
import { type } from 'app/util';

export const ActionTypes = {
    CLOSE_MESSAGE:  type('[Logger] Close Message'),
    LOG_MESSAGE:    type('[Logger] Log Message'),
    LOG_NEUTRAL_MESSAGE:    type('[Logger] Log Neutral Message'),
};

export class LogMessageAction implements Action {
    public type = ActionTypes.LOG_MESSAGE;

    constructor(public payload: string) {}
}

export class LogNeutralMessageAction implements Action {
    public type = ActionTypes.LOG_NEUTRAL_MESSAGE;

    constructor(public payload: string) {}
}

export class CloseMessageAction implements Action {
    public type = ActionTypes.CLOSE_MESSAGE;

    constructor(public payload: number) {}
}

export type Actions =
  | CloseMessageAction
  | LogMessageAction
  | LogNeutralMessageAction
  ;
