import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { Message } from '../model';
import { LoggerActions } from '../store';

@Component({
    selector: 'frg-logger',
    template: `
    <md-card *ngIf="isEmpty()">
      <frg-message-entry *ngFor="let message of messages"
                            [message]="message"
                            (close)="closeMessage($event)">
      </frg-message-entry>
    </md-card>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoggerComponent {
    @Input()
    public messages: Message[];

    constructor(private store: Store<any>) {}

    public closeMessage(id: number) {
        this.store.dispatch(new LoggerActions.CloseMessageAction(id));
    }

    public isEmpty() {
        return this.messages.length > 0;
    }
}
