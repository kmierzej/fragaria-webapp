import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Message } from '../model';

@Component ({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'frg-message-entry',
    template: `
    <div style="text-align: right;">
      <button md-button color="{{message.type}}" (click)="emitHide()">
        {{message.message}}
        <md-icon style="vertical-align:middle; color: black !important;">close</md-icon>
      </button>
    </div>
    `,
})
export class MessageEntryComponent {
    @Input()
    public message: Message;

    @Output()
    public close = new EventEmitter();

    public emitHide() {
        this.close.emit(this.message.id);
    }
}
