import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserAuthGuard } from 'app/guards';
import { DashboardWrapperComponent } from './view/dashboard';
import { HomeComponent } from './view/home';
import { DefaultComponent } from './view/default';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    {
      path: 'home',
      component: HomeComponent,
    },
    {
      component: DashboardWrapperComponent,
      path: 'dashboard',
      canActivate: [UserAuthGuard],
    },
    {
      loadChildren: './view/report#ReportModule',
      path: 'report',
    },
    {
      loadChildren: './view/manage-users#ManageUsersModule',
      path: 'admin/users',
    },
    {
      loadChildren: './view/settings#SettingsModule',
      path: 'user/settings',
    },
    {
      loadChildren: './view/tokens#TokensModule',
      path: 'user/tokens',
    },
    {
      loadChildren: './view/projects#ProjectsRoutingModule',
      path: 'projects',
    },
    {
      path: '**',
      component: DefaultComponent,
    },
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    providers: [
      UserAuthGuard,
    ],
})
export class AppRoutingModule { }
