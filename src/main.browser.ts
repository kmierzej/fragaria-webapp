import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { bootloader } from '@angularclass/hmr';
import { decorateModuleRef } from './app/environment';
import './polyfills.browser';

import { AppModule } from './app';

export function main(): Promise<any> {
    return platformBrowserDynamic()
        .bootstrapModule(AppModule)
        .then(decorateModuleRef)
        .catch((err) => console.error(err)); //tslint:disable-line
}

bootloader(main);
