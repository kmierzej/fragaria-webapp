declare var ENV: string;
declare var BASE_URL: string;

declare var System: SystemJS;

interface SystemJS { //tslint:disable-line
    import: (path?: string) => Promise<any>;
}
