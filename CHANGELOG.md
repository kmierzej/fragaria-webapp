#Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.7.1]
### Changed
- small, finall cosmetic changes

## [0.7.0]
### Added
- timetable resource
- homepage and fallback page
- download report page
- token invalidation page

## [0.6.0]
### Added
- task resource
- assign workers and managers to projects

## [0.5.0]
### Added
- project resource
- filter paged resources by ... filters, for example status, or permissions
### Performance
- moved to angular4 for new features and smaller prod package (at least faster)

## [0.4.2]
### Performance
- created AOT build

## [0.4.1]
### Performance
- upgraded dependencies

## [0.4.0] - PHASE 2
### Added
- edit and modify other user properties, such as email, fullname.

## [0.3.0]
### Changed
- facelift of edit users page
- make use of new patch user interface
- show/hide password on create user
### Fixed
- fixed refresh token of app init
### Removed
- Removed sample resource

## [0.2.0]
### Added
- Added user settings page
- Added neutal colored messages in logger

## [0.1.0]
### Added
- initial work
